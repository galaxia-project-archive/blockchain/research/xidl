# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_EXTERN_ROCKSDB)
  return()
endif()
set(CMAKE_XI_EXTERN_ROCKSDB TRUE)

xi_include(Package/Lz4)
xi_include(Package/Zlib)
xi_include(Package/BZip2)

set(WITH_LZ4 ON CACHE INTERNAL "" FORCE)
set(WITH_ZLIB ON CACHE INTERNAL "" FORCE)
set(WITH_BZ2 ON CACHE INTERNAL "" FORCE)
set(FAIL_ON_WARNINGS OFF CACHE INTERNAL "" FORCE)
set(ROCKSDB_LITE OFF CACHE INTERNAL "" FORCE)
set(WITH_TESTS OFF CACHE INTERNAL "" FORCE)
set(WITH_TOOLS OFF CACHE INTERNAL "" FORCE)

add_subdirectory(${XI_ROOT_DIR}/Extern/RocksDb EXCLUDE_FROM_ALL)

add_library(rocksdb::rocksdb ALIAS rocksdb)

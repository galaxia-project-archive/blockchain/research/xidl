# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_EXTERN_URIPARSER)
  return()
endif()
set(CMAKE_XI_EXTERN_URIPARSER TRUE)

set(URIPARSER_BUILD_CHAR ON CACHE INTERNAL "" FORCE)
set(URIPARSER_BUILD_WCHAR_T OFF CACHE INTERNAL "" FORCE)
set(URIPARSER_BUILD_DOCS OFF CACHE INTERNAL "" FORCE)
set(URIPARSER_BUILD_TESTS OFF CACHE INTERNAL "" FORCE)
set(URIPARSER_BUILD_TOOLS OFF CACHE INTERNAL "" FORCE)
set(URIPARSER_MSVC_RUNTIME "" CACHE INTERNAL "" FORCE)

add_subdirectory(${XI_ROOT_DIR}/Extern/UriParser EXCLUDE_FROM_ALL)
add_library(uriparser::uriparser ALIAS uriparser)

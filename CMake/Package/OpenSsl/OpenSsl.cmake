# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_PACKAGE_OPENSSL)
  return()
endif()
set(CMAKE_XI_PACKAGE_OPENSSL TRUE)

xi_include(Xi/Log)
xi_include(Xi/Compiler/Classify/Linkage)

if(WIN32)
  find_package(Perl QUIET)
  if(NOT PERL_FOUND)
    xi_fatal("The package manager requires perl to build openssl on windows. Please intall a recent version.")
  endif()
endif()

hunter_add_package(OpenSSL)
find_package(OpenSSL REQUIRED)

if(WIN32 AND XI_COMPILER_STATIC)
  foreach(ssl_lib OpenSSL::Crypto OpenSSL::SSL)
    target_link_libraries(
      ${ssl_lib}

      INTERFACE
        WS2_32.LIB
        GDI32.LIB
        ADVAPI32.LIB
        CRYPT32.LIB
        USER32.LIB
    )
  endforeach()
endif()

mark_as_advanced(
  LIB_EAY_DEBUG
  LIB_EAY_RELEASE

  SSL_EAY_DEBUG
  SSL_EAY_RELEASE
)

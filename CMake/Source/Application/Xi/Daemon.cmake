# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_APPLICATION_XI_DAEMON)
  return()
endif()
set(CMAKE_XI_SOURCE_APPLICATION_XI_DAEMON TRUE)

xi_include(Xi/Make/Application)
xi_include(Source/Library/Legacy/P2p)
xi_include(Source/Library/Legacy/Serialization)
xi_include(Source/Library/Legacy/CryptoNoteProtocol)
xi_include(Source/Library/Legacy/BlockchainExplorer)
xi_include(Source/Library/Xi/App)
xi_include(Source/Library/Xi/VersionInfo)
xi_include(Source/Library/Xi/Blockchain/Explorer/Core)
xi_include(Source/Library/Xi/Blockchain/Service/BlockExplorer)
xi_include(Source/Library/Xi/Serialization/Console)

xi_make_application(
  Xi.Daemon

  LIBRARIES
    Xi::App
    Xi::VersionInfo
    Xi::Blockchain::Explorer::Core
    Xi::Blockchain::Service::BlockExplorer
    Xi::Serialization::Console

    Legacy::P2p
    Legacy::Serialization
    Legacy::CryptoNoteProtocol
    Legacy::BlockchainExplorer
)


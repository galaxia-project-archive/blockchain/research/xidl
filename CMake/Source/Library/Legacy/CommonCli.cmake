# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LEGACY_COMMON_CLI)
  return()
endif()
set(CMAKE_XI_LEGACY_COMMON_CLI TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/Rang)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/VersionInfo)
xi_include(Source/Library/Xi/CrashHandler)
xi_include(Source/Library/Xi/Resource)
xi_include(Source/Library/Xi/Serialization/Console)
xi_include(Resource/License)

xi_make_library(
  CommonCLI

  LEGACY

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::VersionInfo
    Xi::CrashHandler

  PRIVATE_LIBRARIES
    Xi::Log
    Xi::Resource
    Xi::Serialization::Console

    Resource::License
    rang::rang
)

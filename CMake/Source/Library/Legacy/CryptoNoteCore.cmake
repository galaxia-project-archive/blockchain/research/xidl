# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LEGACY_CRYPTONOTE_CORE)
  return()
endif()
set(CMAKE_XI_LEGACY_CRYPTONOTE_CORE TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/AsyncPp)

# Currently not supported due to c++17 issues
# xi_include(Package/RocksDb)
xi_include(Extern/RocksDb)

xi_include(Source/Library/Legacy/Common)
xi_include(Source/Library/Legacy/Serialization)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Crypto)
xi_include(Source/Library/Xi/Config)
xi_include(Source/Library/Xi/Endianess)
xi_include(Source/Library/Xi/Resource)
xi_include(Source/Library/Xi/Blockchain)
xi_include(Source/Library/Xi/Blockchain/Explorer)
xi_include(Source/Library/Xi/ProofOfWork)
xi_include(Resource/Chain)
xi_include(Resource/Checkpoint)

xi_make_library(
  CryptoNoteCore

  LEGACY

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Crypto
    Xi::Endianess
    Xi::Config
    Xi::Resource
    Xi::Blockchain
    Xi::Blockchain::Explorer
    Xi::ProofOfWork

    Legacy::Common
    Legacy::Serialization

    Resource::Chain
    Resource::Checkpoint

    rocksdb::rocksdb
    Async++::Async++
)

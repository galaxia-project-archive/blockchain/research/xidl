# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_LEGACY_WALLET)
  return()
endif()
set(CMAKE_XI_LEGACY_WALLET TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Legacy/CryptoNoteCore)
xi_include(Source/Library/Legacy/Crypto)
xi_include(Source/Library/Legacy/Transfers)
xi_include(Source/Library/Legacy/NodeRpcProxy)
xi_include(Source/Library/Legacy/NodeInProcess)
xi_include(Source/Library/Legacy/BlockchainExplorer)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Mnemonic)

xi_make_library(
  Wallet

  LEGACY

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Mnemonic

    Legacy::Transfers
    Legacy::NodeRpcProxy
    Legacy::NodeInProcess
    Legacy::BlockchainExplorer
    Legacy::Crypto
    Legacy::CryptoNoteCore
)

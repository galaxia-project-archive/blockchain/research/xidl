# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_TOOL_XI_IDL_BUILD)
  return()
endif()
set(CMAKE_XI_SOURCE_TOOL_XI_IDL_BUILD TRUE)

xi_include(Xi/Make/Tool)
xi_include(Package/Boost)
xi_include(Package/CxxOpts)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Crypto)
xi_include(Source/Library/Xi/Resource)
xi_include(Source/Library/Xi/FileSystem)
xi_include(Source/Library/Xi/Idl)
xi_include(Source/Library/Xi/Idl/Generator/Cpp)

xi_make_tool(
  Xi.Idl.Builder

  LIBRARIES
    Xi::Core
    Xi::FileSystem
    Xi::Crypto
    Xi::Resource
    Xi::Idl
    Xi::Idl::Generator::Cpp
    Xi::Log

    Boost::boost
    cxxopts::cxxopts
)



# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_SOURCE_TOO_XI_RESOURCE)
  return()
endif()
set(CMAKE_XI_SOURCE_TOO_XI_RESOURCE TRUE)

xi_include(Xi/Make/Tool)
xi_include(Package/Boost)
xi_include(Package/CxxOpts)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Crypto)
xi_include(Source/Library/Xi/FileSystem)
xi_include(Source/Library/Xi/Resource)
xi_include(Source/Library/Xi/Serialization)
xi_include(Source/Library/Xi/Serialization/Yaml)
xi_include(Source/Library/Xi/Serialization/Binary)

xi_make_tool(
  Xi.Resource.Builder

  LIBRARIES
    Xi::Core
    Xi::Log
    Xi::FileSystem
    Xi::Crypto
    Xi::Resource
    Xi::Serialization
    Xi::Serialization::Yaml
    Xi::Serialization::Binary

    Boost::boost
    cxxopts::cxxopts
)



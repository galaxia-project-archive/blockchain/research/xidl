# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_CHECK)
  return()
endif()
set(CMAKE_XI_COMPILER_CHECK TRUE)

include(CMakeParseArguments)

xi_include(Xi/Log)

# xi_compiler_check(
#   REQUIRED <version>                < Version required at least, if not fullfilled an fatal error is reported.
#   UPGRADE <version>                 < Lower versions are marked deprecated and new releases may drop support.
#   RECOMMENDED <version>             < This version is testet at most and recommended to use
# )
function(xi_compiler_check)
  cmake_parse_arguments(
    XI_COMPILER_CHECK
      ""
      "REQUIRED;UPGRADE;RECOMMENDED"
      ""

    ${ARGN}
  )

  if(NOT XI_COMPILER_CHECK_REQUIRED)
    xi_fatal("REQUIRED is not an optional parameter for xi_compiler_check")
  endif()

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_REQUIRED})
    xi_fatal(
      "Your c compiler is outdated and not supported."
        "Compiler           ${CMAKE_C_COMPILER_ID}"
        "Current Version    ${CMAKE_C_COMPILER_VERSION}"
        "Required Version   ${XI_COMPILER_CHECK_REQUIRED}"
    )
  endif()

  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_REQUIRED})
    xi_fatal(
      "Your cxx compiler is outdated and not supported."
      "Compiler           ${CMAKE_CXX_COMPILER_ID}"
      "Current Version    ${CMAKE_CXX_COMPILER_VERSION}"
      "Required Version   ${XI_COMPILER_CHECK_REQUIRED}"
    )
  endif()

  if(XI_COMPILER_CHECK_UPGRADE)
    if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_UPGRADE})
      xi_warning(
        "Your c compiler is outdated and will not be supported in upcoming versions."
        "Compiler           ${CMAKE_C_COMPILER_ID}"
        "Current Version    ${CMAKE_C_COMPILER_VERSION}"
        "Upgrade Version    ${XI_COMPILER_CHECK_UPGRADE}"
      )
    endif()

    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_UPGRADE})
      xi_warning(
        "Your cxx compiler is outdated and will not be supported in upcoming versions."
        "Compiler           ${CMAKE_CXX_COMPILER_ID}"
        "Current Version    ${CMAKE_CXX_COMPILER_VERSION}"
        "Upgrade Version    ${XI_COMPILER_CHECK_UPGRADE}"
      )
    endif()
  endif()

  if(XI_COMPILER_CHECK_RECOMMENDED)
    if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_RECOMMENDED})
      xi_warning(
        "Your c compiler is outdated, a newer version is recommended."
        "Compiler              ${CMAKE_C_COMPILER_ID}"
        "Current Version       ${CMAKE_C_COMPILER_VERSION}"
        "Recommended Version   ${XI_COMPILER_CHECK_RECOMMENDED}"
      )
    endif()

    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_RECOMMENDED})
      xi_warning(
        "Your cxx compiler is outdated, a newer version is recommended"
        "Compiler              ${CMAKE_CXX_COMPILER_ID}"
        "Current Version       ${CMAKE_CXX_COMPILER_VERSION}"
        "Recommended Version   ${XI_COMPILER_CHECK_RECOMMENDED}"
      )
    endif()
  endif()
endfunction()

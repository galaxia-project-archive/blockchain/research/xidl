# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_CLASSIFY_PLATFORM)
  return()
endif()
set(CMAKE_XI_COMPILER_CLASSIFY_PLATFORM TRUE)

xi_include(Xi/Log)

set(
    XI_COMPILER_CLASSIFY_SUPPORTED_PLATFORMS
        LINUX
        WINDOWS
        DARWIN
        POSIX

    CACHE INTERNAL "" FORCE
)

set(XI_COMPILER_PLATFORM_LINUX_NAME "Linux" CACHE INTERNAL "" FORCE)
set(XI_COMPILER_PLATFORM_WINDOWS_NAME "Windows" CACHE INTERNAL "" FORCE)
set(XI_COMPILER_PLATFORM_DARWIN_NAME "Darwin" CACHE INTERNAL "" FORCE)
set(XI_COMPILER_PLATFORM_POSIX_NAME "Posix" CACHE INTERNAL "" FORCE)

foreach(platform ${XI_COMPILER_CLASSIFY_SUPPORTED_PLATFORMS})
    set(XI_COMPILER_PLATFORM_${platform} OFF CACHE INTERNAL "" FORCE)
endforeach()

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
  set(XI_COMPILER_PLATFORM_LINUX ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_PLATFORM_POSIX ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_PLATFORM_ID "LINUX" CACHE INTERNAL "" FORCE)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
  set(XI_COMPILER_PLATFORM_WINDOWS ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_PLATFORM_ID "WINDOWS" CACHE INTERNAL "" FORCE)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  set(XI_COMPILER_PLATFORM_DARWIN ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_PLATFORM_POSIX ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_PLATFORM_ID "DARWIN" CACHE INTERNAL "" FORCE)
else()
  xi_fatal("Unknown platform for system name '${CMAKE_SYSTEM_NAME}'")
endif()


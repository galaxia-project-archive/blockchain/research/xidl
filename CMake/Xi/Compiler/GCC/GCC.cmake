# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_GCC)
    return()
endif()
set(CMAKE_XI_COMPILER_GCC TRUE)

xi_include(Xi/Option/TreatWarningAsError)
xi_include(Xi/Option/Breakpad)
xi_include(Xi/Compiler/Flag/Add)
xi_include(Xi/Compiler/Check)

xi_compiler_check(
  REQUIRED 7.3 
  UPGRADE 8.1
  RECOMMENDED 8.3
)

xi_compiler_flag_add("-Wall -Wextra")

if(XI_TREAT_WARNING_AS_ERROR)
  xi_compiler_flag_add("-Werror")
endif()

if(XI_BUILD_BREAKPAD)
  if(CMAKE_BUILD_TYPE MATCHES RELEASE OR CMAKE_BUILD_TYPE MATCHES MINSIZEREL)
    xi_compiler_flag_add("-g -fno-omit-frame-pointer")
  endif()
endif()

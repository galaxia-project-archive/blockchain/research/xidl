# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_COMPILER_MSVC_PARALLEL)
    return()
endif()
set(CMAKE_XI_COMPILER_MSVC_PARALLEL TRUE)

option(XI_COMPILER_PARALLEL_BUILD OFF "enables parallel compiler execution")
set(XI_COMPILER_PARALLEL_BUILD_THREADS "-1" CACHE STRING
        "maximum number of threads to use for parallel compilation (<=0 -> max)")

xi_include(Xi/Log)
xi_include(Xi/Compiler/Flag/Add)

if(XI_COMPILER_PARALLEL_BUILD)
  if(XI_COMPILER_PARALLEL_BUILD_THREADS LESS_EQUAL "0")
    xi_status("Enabling parallel compilation with maximum threads.")
    xi_compiler_flag_add("/MP")
  else()
    xi_status("Enabling parallel compilation with ${XI_PARALLEL_BUILD_THREADS} threads.")
    xi_compiler_flag_add("/MP${XI_PARALLEL_BUILD_THREADS}")
  endif()
endif()

# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_ESCAPE)
  return()
endif()
set(CMAKE_XI_ESCAPE TRUE)

# xi_escape(
#   <out_var>                   < Output variable name to store the escaped string
#   <value>                     < Value to escape
# )
#
# Escape replaces common characters to escape within c++ string.
function(xi_escape out_var value)
  set(result ${value})
  string(REPLACE "\\" "\\\\" value ${value})
  string(REPLACE "\r" "" value ${value})
  string(REPLACE "\t" "\\t" value ${value})
  string(REPLACE "\n" "\\n" value ${value})
  set(${out_var} "${result}" PARENT_SCOPE)
endfunction()

# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_SET_COMPILER_FLAGS)
  return()
endif()
set(CMAKE_XI_MAKE_SET_COMPILER_FLAGS TRUE)

xi_include(Xi/Compiler)

# xi_make_set_compiler_flags(
#   [<source_file> ...]                 < source files to embedd additional xi compiler flags
# )
#
# This function will determine the source file type and embedd additional xi compiler flags.
function(xi_make_set_compiler_flags)
  foreach(sourceFile ${ARGN})
    get_filename_component(sourceFileExt ${sourceFile} LAST_EXT)
    if(sourceFileExt STREQUAL ".c")
      set_source_files_properties(
        ${sourceFile}

        COMPILE_FLAGS
          ${XI_C_FLAGS}
      )
    elseif(sourceFileExt STREQUAL ".cpp")
      set_source_files_properties(
        ${sourceFile}

        COMPILE_FLAGS
          ${XI_CXX_FLAGS}
        )
    endif()
  endforeach()
endfunction()

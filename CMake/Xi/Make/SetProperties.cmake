# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_SETPROPERTIES)
    return()
endif()
set(CMAKE_XI_MAKE_SETPROPERTIES TRUE)

xi_include(Xi/Version)
xi_include(Xi/Compiler/Classify/Identifier)

# xi_make_set_properties(
#   <target> [<target>...]              << Targets to be configured.
# )
# Sets default properties on targets used by all xi applications.
function(xi_make_set_properties)
  set_target_properties(
    ${ARGN}

    PROPERTIES
      VERSION "${XI_VERSION}"
      ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib/"
      LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib/"
      RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin/"
  )

  if(XI_COMPILER_MSVC)
    set_target_properties(
      ${ARGN}

      PROPERTIES
        LINK_FLAGS "/IGNORE:4099"
    )
  endif()
endfunction()

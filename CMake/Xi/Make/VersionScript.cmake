# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_MAKE_VERSIONSCRIPT)
    return()
endif()
set(CMAKE_XI_MAKE_VERSIONSCRIPT TRUE)

# Input Variables:
#   PROJECT_DIR: Project source directory
#   INPUT_FILE: VERSION input filepath
#   HEADER_DIR: Version.hh output directory
#   TEMPLATE_FILE: Version.hh.in template file
#   NAMESPACE: Library namespace (ie. Xi::...)

set(OUTPUT_FILE "${HEADER_DIR}/Version.hh")
set(TEMP_OUTPUT_FILE "${HEADER_DIR}/Version.hh.temp")

message(STATUS "Building version info for '${PROJECT_DIR}'")

if(XI_SKIP_VERSION_UPDATE AND EXISTS "${OUTPUT_FILE}")
  message(STATUS "Version skip is enabled (XI_SKIP_VERSION_UPDATE=ON), skipping...")
  return()
endif()

find_package(Git QUIET)
if(Git_FOUND)
  macro(git_log out_var format)
    execute_process(
      COMMAND
        ${GIT_EXECUTABLE} log -1 "--pretty=format:${format}"

      OUTPUT_VARIABLE
        ${out_var}

      WORKING_DIRECTORY
        ${PROJECT_DIR}
    )
  endmacro()
  execute_process(
    COMMAND
      ${GIT_EXECUTABLE} name-rev --name-only HEAD

    OUTPUT_VARIABLE
      GIT_BRANCH

    WORKING_DIRECTORY
      ${PROJECT_DIR}
  )
  string(REPLACE "\n" "" GIT_BRANCH ${GIT_BRANCH})
  string(REPLACE "\r" "" GIT_BRANCH ${GIT_BRANCH})
  set(GIT_ENABLED "1")
else()
  set(GIT_ENABLED "0")
endif()

git_log(GIT_COMMIT_HASH "%H")
git_log(GIT_COMMIT_HASH_SHORT "%h")
git_log(GIT_COMMITER_NAME "%cN")
git_log(GIT_COMMITER_EMAIL "%cE")
git_log(GIT_COMMITER_DATE "%cD")
git_log(GIT_COMMITER_TIMESTAMP "%ct")
git_log(GIT_TREE_HASH "%T")
git_log(GIT_TREE_HASH_SHORT "%t")
git_log(GIT_AUTHOR_NAME "%aN")
git_log(GIT_AUTHOR_EMAIL "%aE")
git_log(GIT_AUTHOR_DATE "%aD")
git_log(GIT_AUTHOR_TIMESTAMP "%at")

file(STRINGS ${INPUT_FILE} input_content)
list(LENGTH input_content intput_content_length)

if(input_content_length LESS 1)
    message(FATAL_ERROR "Empty version content for '${INPUT_FILE}'.")
endif()

string(REPLACE "::" "_" VERSION_MACRO_PREFIX ${NAMESPACE})
string(TOUPPER ${VERSION_MACRO_PREFIX} VERSION_MACRO_PREFIX)

set(VERSION_NAMESPACE_BEGIN)
set(VERSION_NAMESPACE_END)
string(REPLACE "::" ";" namespace_list ${NAMESPACE})
foreach(namespace ${namespace_list})
    set(VERSION_NAMESPACE_BEGIN "${VERSION_NAMESPACE_BEGIN}namespace ${namespace} {\n")
    set(VERSION_NAMESPACE_END "${VERSION_NAMESPACE_END}\n}")
endforeach()

list(GET input_content 0 version_line)
string(REPLACE "." ";" version ${input_content})
list(LENGTH version version_size)

macro(version_size_macro index name)
    if(${version_size} GREATER ${index})
        set(VERSION_HAS_${name} 1)
        list(GET version ${index} index_version)
        set(VERSION_${name} ${index_version})
    else()
        set(VERSION_HAS_${name} 0)
    endif()
endmacro()

version_size_macro(0 "MAJOR")
version_size_macro(1 "MINOR")
version_size_macro(2 "PATCH")
version_size_macro(3 "BUILD")
set(VERSION ${version})

if(EXISTS "${TEMP_OUTPUT_FILE}")
  file(REMOVE "${TEMP_OUTPUT_FILE}")
endif()

configure_file(
    "${TEMPLATE_FILE}"
    "${TEMP_OUTPUT_FILE}"
    @ONLY
)

if(EXISTS ${OUTPUT_FILE})
  file(SHA256 "${TEMP_OUTPUT_FILE}" temp_hash)
  file(SHA256 "${OUTPUT_FILE}" current_hash)
  if(NOT TEMP_OUTPUT_FILE STREQUAL OUTPUT_FILE)
    file(RENAME ${TEMP_OUTPUT_FILE} ${OUTPUT_FILE})
    message(STATUS "Version file '${OUTPUT_FILE}' updated.")
  else()
    file(REMOVE "${TEMP_OUTPUT_FILE}")
    message(STATUS "Version file '${OUTPUT_FILE}' already up to date.")
  endif()
else()
  file(RENAME ${TEMP_OUTPUT_FILE} ${OUTPUT_FILE})
  message(STATUS "Version file '${OUTPUT_FILE}' updated.")
endif()

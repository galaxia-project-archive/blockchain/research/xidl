# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(DEFINED CMAKE_XI_XI)
    return()
endif()
set(CMAKE_XI_XI TRUE)

set(XI_ROOT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
set(XI_CMAKE_SOURCE_DIR "${XI_ROOT_DIR}/CMake" CACHE INTERNAL "" FORCE)
include(${XI_CMAKE_SOURCE_DIR}/Xi/Include.cmake)

xi_include(Hunter)
xi_include(Xi/Log)

xi_debug(
  "CMake initialized."
  "XI_ROOT_DIR=${XI_ROOT_DIR}"
  "XI_CMAKE_SOURCE_DIR=${XI_CMAKE_SOURCE_DIR}"
)

set(XI_PROJECT_NAME "Xi")
set(XI_PROJECT_COMPANY "Xi Project Developers")
set(XI_PROJECT_DESCRIPTION "An evolving framework for blockchain-based applications.")
set(XI_PROJECT_HOMEPAGE "https://xiproject.io/")
set(XI_PROJECT_COPYRIGHT "Copyright 2018-2019 Xi Project Developers <support.xiproject.io>")

file(READ "${CMAKE_SOURCE_DIR}/VERSION" version_content)
string(REPLACE "\n" ";" version_content "${version_content}")
list(GET version_content 0 XI_PROJECT_VERSION)

xi_status(
  "Project Info"
    "Name             ${XI_PROJECT_NAME}"
    "Company          ${XI_PROJECT_COMPANY}"
    "Description      ${XI_PROJECT_DESCRIPTION}"
    "Homepage         ${XI_PROJECT_HOMEPAGE}"
    "Copyright        ${XI_PROJECT_COPYRIGHT}"
    "Version          ${XI_PROJECT_VERSION}"
)

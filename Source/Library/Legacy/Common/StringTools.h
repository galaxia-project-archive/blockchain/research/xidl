﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <string>

namespace Common {

std::string asString(const void* data, size_t size);     // Does not throw
std::string asString(const std::vector<uint8_t>& data);  // Does not throw
std::vector<uint8_t> asBinaryArray(const std::string& data);

std::string ipAddressToString(uint32_t ip);
bool parseIpAddress(uint32_t& ip, const std::string& addr);
bool parseIpAddressAndPort(uint32_t& ip, uint16_t& port, const std::string& addr);

std::string timeIntervalToString(uint64_t intervalInSeconds);

}  // namespace Common

﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/String/String.hpp>

#include <Serialization/EnumSerialization.hpp>

namespace Logging {

XI_ERROR_CODE_BEGIN(Level)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(Level, "Logging::LevelError")

enum struct Level {
  None = 1,
  Fatal = 2,
  Error = 3,
  Warning = 4,
  Info = 5,
  Debugging = 6,
  Trace = 7,
};

CRYPTONOTE_SERIALIZATION_ENUM(Level)

std::string stringify(Level level);
Xi::Result<void> parse(const std::string&, Level& out);
}  // namespace Logging

XI_ERROR_CODE_OVERLOADS(Logging, Level)

CRYPTONOTE_SERIALIZATION_ENUM_RANGE(Logging::Level, None, Trace)
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, None, "none")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Fatal, "fatal")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Error, "error")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Warning, "warning")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Info, "info")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Debugging, "debugging")
CRYPTONOTE_SERIALIZATION_ENUM_TAG(Logging::Level, Trace, "trace")

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/App/BreakpadOptions.h"

#include <Xi/Log/Log.hpp>
#include <Xi/FileSystem/Directory.hpp>
#include <Xi/CrashHandler/CrashHandler.hpp>
#include <Xi/CrashHandler/Config/Config.hh>

XI_LOGGER("Breakpad")

namespace Xi {
namespace App {

void BreakpadOptions::loadEnvironment(Environment &env) {
  // clang-format off
  env
    (enabled(), "BREAKPAD")
    (outputPath(), "BREAKPAD_OUT")
    (upload(), "BREAKPAD_UPLOAD")
    (server(), "BREAKPAD_SERVER")
  ;
  // clang-format on
}

void BreakpadOptions::emplaceOptions(cxxopts::Options &options) {
  if constexpr (CrashHandler::is_enabled) {
    // clang-format off
    options.add_options("breakpad")

      ("breakpad-enable", "Enables creation of crash dumps to help developers reconstruct bugs occuring in release builds.",
        cxxopts::value<bool>(enabled())->implicit_value("true")->default_value(enabled() ? "true" : "false"))

      ("breakpad-out", "Output directory for storing crash dumps",
        cxxopts::value<std::string>(outputPath())->default_value(outputPath()))

      ("breakpad-server", "breakpad server for uploading crash dumps",
        cxxopts::value<std::string>(server())->default_value(server()))

      ("breakpad-upload", "Enables auto upload of crash dumps to the galaxia project breakpad server.",
        cxxopts::value<bool>(upload())->implicit_value("true")->default_value(upload() ? "true" : "false"))
    ;
    // clang-format on
  }
}

bool BreakpadOptions::evaluateParsedOptions(const cxxopts::Options &options, const cxxopts::ParseResult &result) {
  XI_UNUSED(options, result);
  if constexpr (CrashHandler::is_enabled) {
    if (enabled()) {
      auto dir = FileSystem::makeDirectory(outputPath());
      if (dir.isError()) {
        XI_FATAL("Invalid breakpad directory '{}' (Error: {}).", outputPath(), dir.error())
        XI_RETURN_EC(true);
      }
      auto ec = dir->createRegularIfNotExists();
      if (ec.isError()) {
        XI_FATAL("Unable to create breakpad directory '{}' (Error: {}).", outputPath(), ec.error())
        XI_RETURN_EC(true);
      }
    }
  } else if (enabled()) {
    XI_WARN("Breakpad was explicitly enabled but this release was built without breakpad support.")
  }
  XI_RETURN_SC(false);
}

CrashHandler::Config BreakpadOptions::config(const std::string &app) const {
  CrashHandler::Config reval{};
  reval.IsEnabled = enabled();
  reval.OutputPath = outputPath();
  reval.IsUploadEnabled = upload();
  reval.Server = server();
  reval.Application = app;
  return reval;
}

}  // namespace App
}  // namespace Xi

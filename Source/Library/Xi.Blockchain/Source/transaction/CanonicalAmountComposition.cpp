/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Transaction/CanonicalAmountComposition.hpp"

namespace Xi {
namespace Blockchain {
namespace Transaction {

CanonicalAmountVector decomposeAmount(const Amount amount) {
  CanonicalAmountVector reval{};
  auto remainder = amount;
  decltype(remainder) order = 1;
  while (remainder > 0) {
    if (const auto iAmount = (remainder % 10) * order; iAmount > 0) {
      reval.push_back(CanonicalAmount{iAmount});
    }

    order *= 10;
    remainder /= 10;
  }
  return reval;
}

}  // namespace Transaction
}  // namespace Blockchain
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Transaction/CanonicalAmountDescriptor.hpp"

#include <unordered_map>

#include <Xi/Algorithm/Math.h>

namespace Xi {
namespace Blockchain {
namespace Transaction {

class CanonicalAmountDescriptorCollection {
 private:
  std::unordered_map<Amount, CanonicalAmountDescriptor> m_units;

 private:
  CanonicalAmountDescriptorCollection() {
    for (uint8_t decade = 0; decade < 16; ++decade) {
      uint64_t iDecadeUnit = Xi::pow64(10, decade);
      for (uint8_t digit = 1; digit < 10; ++digit) {
        uint64_t iAmount = iDecadeUnit * digit;
        m_units.emplace(std::piecewise_construct, std::forward_as_tuple(iAmount),
                        std::forward_as_tuple(CanonicalAmountDescriptor{decade, digit}));
      }
    }
  }

 public:
  static const std::unordered_map<Amount, CanonicalAmountDescriptor>& units() {
    static const CanonicalAmountDescriptorCollection __Singleton{};
    return __Singleton.m_units;
  }
};

std::optional<CanonicalAmountDescriptor> CanonicalAmountDescriptor::find(const Amount amount) {
  const auto search = CanonicalAmountDescriptorCollection::units().find(amount);
  if (search == CanonicalAmountDescriptorCollection::units().end()) {
    return std::nullopt;
  } else {
    return search->second;
  }
}

CanonicalAmountDescriptor::CanonicalAmountDescriptor(uint8_t decade_, uint8_t digit_)
    : m_decade{decade_}, m_digit{digit_}, m_amount{digit_ * pow64(10, decade_)} {
  /* */
}

uint8_t CanonicalAmountDescriptor::decade() const {
  return m_decade;
}

uint8_t CanonicalAmountDescriptor::digit() const {
  return m_digit;
}

CanonicalAmount CanonicalAmountDescriptor::amount() const {
  return m_amount;
}

CanonicalAmountDescriptor::operator CanonicalAmount() const {
  return m_amount;
}

}  // namespace Transaction
}  // namespace Blockchain
}  // namespace Xi

﻿#include "Xi/Config/NetworkType.h"

#include <stdexcept>

#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Config::Network, Type)
XI_ERROR_CODE_DESC(Unknown, "unknown network type")
XI_ERROR_CODE_CATEGORY_END()

static_assert(
    static_cast<uint8_t>(Xi::Config::Network::Type::MainNet) == 1,
    "Their values are used as offset for fundamental configurations, changing those will break the entire chain.");
static_assert(
    static_cast<uint8_t>(Xi::Config::Network::Type::StageNet) == 2,
    "Their values are used as offset for fundamental configurations, changing those will break the entire chain.");
static_assert(
    static_cast<uint8_t>(Xi::Config::Network::Type::TestNet) == 3,
    "Their values are used as offset for fundamental configurations, changing those will break the entire chain.");
static_assert(
    static_cast<uint8_t>(Xi::Config::Network::Type::LocalTestNet) == 4,
    "Their values are used as offset for fundamental configurations, changing those will break the entire chain.");

namespace Xi {
namespace Config {
namespace Network {

std::string stringify(Type type) {
  switch (type) {
    case Type::MainNet:
      return "MainNet";
    case Type::StageNet:
      return "StageNet";
    case Type::TestNet:
      return "TestNet";
    case Type::LocalTestNet:
      return "LocalTestNet";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string &str, Config::Network::Type &out) {
  auto lower = toLower(str);
  if (lower == toLower(toString(Type::MainNet)))
    out = Type::MainNet;
  else if (lower == toLower(toString(Type::StageNet)))
    out = Type::StageNet;
  else if (lower == toLower(toString(Type::TestNet)))
    out = Type::TestNet;
  else if (lower == toLower(toString(Type::LocalTestNet)))
    out = Type::LocalTestNet;
  else
    XI_FAIL(TypeError::Unknown)
  XI_SUCCEED()
}

}  // namespace Network
}  // namespace Config
}  // namespace Xi

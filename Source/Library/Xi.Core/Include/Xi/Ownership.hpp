/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <utility>

#include <Xi/Global.hh>
#include <Xi/Exceptions.hpp>

namespace Xi {

template <typename _ValueT>
class Ownership final {
 private:
  struct _Concept {
    virtual ~_Concept() = default;

    virtual const _ValueT& get() const = 0;
    virtual _ValueT& get() = 0;
  };

  template <typename _CovariantT>
  struct _UniqueOwnership final : _Concept {
    std::unique_ptr<_CovariantT> m_data;

    _UniqueOwnership(std::unique_ptr<_CovariantT> data) : m_data{std::move(data)} {
      XI_EXCEPTIONAL_IF_NOT(NullArgumentError, m_data)
    }
    ~_UniqueOwnership() = default;

    virtual const _ValueT& get() const {
      return *this->m_data;
    }
    virtual _ValueT& get() {
      return *this->m_data;
    }
  };

  template <typename _CovariantT>
  struct _DelegatedOwnership final : _Concept {
    _CovariantT& m_data;

    _DelegatedOwnership(_CovariantT& data) : m_data{data} {
      /* */
    }
    ~_DelegatedOwnership() = default;

    virtual const _ValueT& get() const {
      return this->m_data;
    }
    virtual _ValueT& get() {
      return this->m_data;
    }
  };

  template <typename _CovariantT>
  struct _ValueOwnership final : _Concept {
    _CovariantT m_data;

    _ValueOwnership(_CovariantT data) : m_data{std::move(data)} {
      /* */
    }
    ~_ValueOwnership() = default;

    virtual const _ValueT& get() const {
      return this->m_data;
    }
    virtual _ValueT& get() {
      return this->m_data;
    }
  };

 private:
  std::unique_ptr<_Concept> m_impl;

 private:
  explicit Ownership(std::unique_ptr<_Concept> impl) : m_impl{std::move(impl)} {
    /* */
  }

  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeUniqueOwnership(std::unique_ptr<_CovariantT> data);
  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeDelegatedOwnership(_CovariantT& data);
  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeValueOwnership(_CovariantT&& data);

 public:
  XI_DELETE_COPY(Ownership);
  XI_DEFAULT_MOVE(Ownership);
  virtual ~Ownership() = default;

  const _ValueT& get() const {
    return this->m_impl->get();
  }

  _ValueT& get() {
    return this->m_impl->get();
  }
};

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeUniqueOwnership(std::unique_ptr<_CovariantT> data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _UniqueOwnership<_CovariantT>{std::move(data)}}};
}

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeDelegatedOwnership(_CovariantT& data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _DelegatedOwnership<_CovariantT>{data}}};
}

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeValueOwnership(_CovariantT&& data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _ValueOwnership<_CovariantT>{std::move(data)}}};
}

}  // namespace Xi

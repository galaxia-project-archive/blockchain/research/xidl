/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <memory>

namespace Xi {

template <typename _ValueT>
struct is_unique_pointer : std::false_type {};
template <typename _ValueT>
struct is_unique_pointer<std::unique_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_unique_pointer<std::unique_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_unique_pointer_v = is_unique_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_shared_pointer : std::false_type {};
template <typename _ValueT>
struct is_shared_pointer<std::shared_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_shared_pointer<std::shared_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_shared_pointer_v = is_shared_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_weak_pointer : std::false_type {};
template <typename _ValueT>
struct is_weak_pointer<std::weak_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_weak_pointer<std::weak_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_weak_pointer_v = is_weak_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_smart_pointer
    : std::conditional_t<is_unique_pointer_v<_ValueT> || is_shared_pointer_v<_ValueT> || is_weak_pointer_v<_ValueT>,
                         std::true_type, std::false_type> {};
template <typename _ValueT>
inline constexpr bool is_smart_pointer_v = is_smart_pointer<_ValueT>::value;

static_assert(is_smart_pointer_v<std::unique_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::unique_ptr<const int>>, "");
static_assert(is_smart_pointer_v<std::shared_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::shared_ptr<const int>>, "");
static_assert(is_smart_pointer_v<std::weak_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::weak_ptr<const int>>, "");

}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <comdef.h>

#include "Xi/Windows/ComError.hpp"

namespace Xi {
namespace Windows {
ComErrorCategory ComErrorCategory::Instance{/* */};

const char *ComErrorCategory::name() const noexcept {
  return "ComError";
}

std::string ComErrorCategory::message(int hresult) const {
  return _com_error{hresult}.ErrorMessage();
}

std::error_condition ComErrorCategory::default_error_condition(int hresult) const noexcept {
  if (HRESULT_CODE(hresult) || hresult == 0) {
    return std::system_category().default_error_condition(HRESULT_CODE(hresult));
  } else {
    return std::error_condition{hresult, ComErrorCategory::Instance};
  }
}

ComError toComError(const HRESULT hresult) noexcept {
  return static_cast<ComError>(hresult);
}

}  // namespace Windows
}  // namespace Xi

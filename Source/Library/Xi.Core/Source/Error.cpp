﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Error.hpp"

#include <stdexcept>
#include <cassert>

#include "Xi/GlitchError.hpp"

namespace Xi {

Error::Error() : m_error{make_error_code(GlitchError::NotInitialized)} {
}
Error::Error(std::exception_ptr e) : m_error{e} {
}
Error::Error(std::error_code ec) : m_error{ec} {
}

std::string Error::message() const {
  if (isException()) {
    try {
      std::rethrow_exception(exception());
    } catch (const std::exception& e) {
      return e.what();
    } catch (...) {
      return "Unknown type has been thrown.";
    }
  } else {
    assert(isErrorCode());
    return errorCode().message();
  }
}

bool Error::isException() const {
  return std::holds_alternative<std::exception_ptr>(m_error);
}
std::exception_ptr Error::exception() const {
  return std::get<std::exception_ptr>(m_error);
}

bool Error::isErrorCode() const {
  return std::holds_alternative<std::error_code>(m_error);
}
std::error_code Error::errorCode() const {
  if (isException()) {
    return make_error_code(GlitchError::Exception);
  } else {
    assert(isErrorCode());
    return std::get<std::error_code>(m_error);
  }
}

void Error::throwException() const {
  if (isException()) {
    std::rethrow_exception(exception());
  } else {
    assert(isErrorCode());
    throw std::system_error(errorCode());
  }
}

std::string Error::stringify() const {
  return message();
}

}  // namespace Xi

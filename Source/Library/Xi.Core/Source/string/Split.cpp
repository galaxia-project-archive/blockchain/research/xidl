/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Split.hpp"

namespace Xi {

void split(std::string_view str, std::string_view tokens, std::function<void(std::string_view)> cb) {
  size_t begin = 0;
  size_t end = begin;

  while (end < str.size()) {
    const bool isToken = tokens.find(str[end]) != std::string_view::npos;
    if (isToken) {
      if (end > begin) {
        cb(str.substr(begin, end - begin));
      }
      begin = end + 1;
    }
    end += 1;
  }

  if (end > begin) {
    cb(str.substr(begin, end - begin));
  }
}

std::vector<std::string_view> split(std::string_view str, std::string_view tokens) {
  std::vector<std::string_view> reval{};
  split(str, tokens, [&reval](auto istr) { reval.emplace_back(istr); });
  return reval;
}

std::vector<std::string> split(std::string_view str, std::string_view tokens, make_copy_t) {
  std::vector<std::string> reval{};
  split(str, tokens, [&reval](auto istr) { reval.emplace_back(istr.data(), istr.size()); });
  return reval;
}

}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/ToUpper.hpp"

#include <memory>
#include <locale>

namespace Xi {

std::string toUpper(std::string_view str) {
  const std::locale locale{/* */};
  std::string reval{};
  reval.resize(str.size());
  for (size_t i = 0; i < str.size(); ++i) {
    reval[i] = static_cast<std::string::value_type>(std::toupper(str[i], locale));
  }
  return reval;
}

std::string::value_type toUpper(const std::string::value_type ch) {
  const std::locale locale{/* */};
  return static_cast<std::string::value_type>(std::toupper(ch, locale));
}

void toUpper(std::string &str, in_place_t) {
  const std::locale locale{/* */};
  for (size_t i = 0; i < str.size(); ++i) {
    str[i] = static_cast<std::string::value_type>(std::toupper(str[i], locale));
  }
}

}  // namespace Xi

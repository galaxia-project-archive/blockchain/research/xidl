/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Parse/Number.hh"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi, ParseNumber)
XI_ERROR_CODE_DESC(IllFormed, "ill formed number")
XI_ERROR_CODE_DESC(Overflow, "number is too small")
XI_ERROR_CODE_DESC(Underflow, "number is too large")
XI_ERROR_CODE_DESC(LeadingSpace, "number has leading spaces")
XI_ERROR_CODE_DESC(TrailingCharacter, "number has traling characters")
XI_ERROR_CODE_DESC(InvalidCharacter, "number contains invalid characters")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {

Result<void> parse(const std::string &str, uint8_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint8(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint16_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint16(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint32_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint32(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint64_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint64(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int8_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int8(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int16_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int16(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int32_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int32(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int64_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int64(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, float &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_float(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, double &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_double(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, long double &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_long_double(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/String/Stringify/Hexadecimal.hpp"

#include <type_traits>
#include <cstdio>
#include <utility>

namespace Xi {

std::string stringify(const uint8_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint8_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint16_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint16_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint32_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint32_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint64_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint64_t) * 2 + 2);
  sprintf(reval.data(), "0x%02llX", value);
  return reval;
}

std::string stringify(const int8_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint8_t>(-value), Stringify::hexadecimal);
  } else {
    return stringify(static_cast<uint8_t>(value), Stringify::hexadecimal);
  }
}

std::string stringify(const int16_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint16_t>(-value), Stringify::hexadecimal);
  } else {
    return stringify(static_cast<uint16_t>(value), Stringify::hexadecimal);
  }
}

std::string stringify(const int32_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint32_t>(-value), Stringify::hexadecimal);
  } else {
    return stringify(static_cast<uint32_t>(value), Stringify::hexadecimal);
  }
}

std::string stringify(const int64_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint64_t>(-value), Stringify::hexadecimal);
  } else {
    return stringify(static_cast<uint64_t>(value), Stringify::hexadecimal);
  }
}

}  // namespace Xi

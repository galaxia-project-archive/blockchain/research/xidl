/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/String/FromString.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_FromString

TEST(XI_TEST_SUITE, Number) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Testing;

  {
    static_assert(has_parse_expression_v<uint8_t>, "");
    auto value = fromString<uint8_t>("1");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(1));
  }

  {
    auto value = fromString<uint8_t>("0");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(0));
  }

  {
    auto value = fromString<uint8_t>("0xFF");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(0xFF));
  }

  {
    auto value = fromString<int8_t>("0xFF");
    ASSERT_THAT(value, IsFailure());
  }

  {
    auto value = fromString<int8_t>("-0xFF");
    ASSERT_THAT(value, IsFailure());
  }

  {
    auto value = fromString<int8_t>("-0x0F");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(-0x0F));
  }
}

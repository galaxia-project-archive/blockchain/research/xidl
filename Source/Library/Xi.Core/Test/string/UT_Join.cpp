/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/Join.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Join

TEST(XI_TEST_SUITE, EmptyCase) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{};
    EXPECT_THAT(join(value, ","), Eq(""));
  }
  {
    std::vector<std::string> value{};
    EXPECT_THAT(join(value, ""), Eq(""));
  }
}

TEST(XI_TEST_SUITE, SingleValue) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{""};
    EXPECT_THAT(join(value, ","), Eq(""));
  }
  {
    std::vector<std::string> value{"_"};
    EXPECT_THAT(join(value, ","), Eq("_"));
  }
}

TEST(XI_TEST_SUITE, MultiValue) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{"A", "B"};
    EXPECT_THAT(join(value, ","), Eq("A,B"));
  }
  {
    std::vector<std::string> value{",", "^", ","};
    EXPECT_THAT(join(value, ", "), Eq(",, ^, ,"));
  }
}

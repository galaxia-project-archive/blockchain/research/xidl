/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/ToLower.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToLower

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toLower(""), Eq(""));
}

TEST(XI_TEST_SUITE, InPlace) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::string value{"Ab-_-bA"};
    toLower(value, in_place_v);
    EXPECT_THAT(value, Eq("ab-_-ba"));
  }
}

TEST(XI_TEST_SUITE, Copy) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toLower("Ab-_-bA"), Eq("ab-_-ba"));
}

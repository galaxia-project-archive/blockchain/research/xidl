/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/String/Trim.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Trim

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(trimLeft(""), Eq(""));
  EXPECT_THAT(trimRight(""), Eq(""));
  EXPECT_THAT(trim(""), Eq(""));
}

TEST(XI_TEST_SUITE, Common) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(trimLeft("    "), Eq(""));
  EXPECT_THAT(trimRight("    "), Eq(""));
  EXPECT_THAT(trimLeft("    _   "), Eq("_   "));
  EXPECT_THAT(trimRight("    _   "), Eq("    _"));
  EXPECT_THAT(trim("       "), Eq(""));
  EXPECT_THAT(trim("   -  -    "), Eq("-  -"));
  EXPECT_THAT(trim("   \0  \0    "), Eq("\0  \0"));
}

TEST(XI_TEST_SUITE, CommonInPlace) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::string value{"    "};
    trimLeft(value, in_place_v);
    EXPECT_THAT(value, Eq(""));
  }
  {
    std::string value{"    "};
    trimRight(value, in_place_v);
    EXPECT_THAT(value, Eq(""));
  }
  {
    std::string value{"    "};
    trim(value, in_place_v);
    EXPECT_THAT(value, Eq(""));
  }

  {
    std::string value{"   -   "};
    trimLeft(value, in_place_v);
    EXPECT_THAT(value, Eq("-   "));
  }
  {
    std::string value{"   -   "};
    trimRight(value, in_place_v);
    EXPECT_THAT(value, Eq("   -"));
  }
  {
    std::string value{"   -   "};
    trim(value, in_place_v);
    EXPECT_THAT(value, Eq("-"));
  }

  {
    std::string value{"X X"};
    trim(value, in_place_v);
    EXPECT_THAT(value, Eq("X X"));
  }
  {
    std::string value{"\0"};
    trim(value, in_place_v);
    EXPECT_THAT(value, Eq("\0"));
  }
  {
    std::string value{" \0  \0 "};
    trim(value, in_place_v);
    EXPECT_THAT(value, Eq("\0  \0"));
  }
}

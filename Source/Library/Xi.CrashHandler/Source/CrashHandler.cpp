﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#if defined(__GNUC__)
#define __STDC_FORMAT_MACROS
#endif

#include "Xi/CrashHandler/CrashHandler.hpp"

#include "Xi/CrashHandler/Config/Config.hh"

#if !defined(XI_CRASH_HANDLER_ENABLED)
namespace Xi {
namespace CrashHandler {
struct CrashHandler::_Impl {
  /* */
};

CrashHandler::CrashHandler(const Config&) : m_impl{nullptr} {
  /* */
}
}  // namespace CrashHandler
}  // namespace Xi

#else

#include <stdexcept>
#include <utility>
#include <functional>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <iostream>
#include <codecvt>

#include <Xi/Extern/Push.hh>
#include <boost/predef/os.h>
#include <handler/exception_handler.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/String/String.hpp>
#include <Xi/FileSystem.h>
#include <Xi/Http/MultipartFormDataBuilder.h>
#include <Xi/Http/Client.h>
#include <Xi/VersionInfo/VersionInfo.hpp>

#if BOOST_OS_WINDOWS
#include <stringapiset.h>
#endif  // BOOST_OS_WINDOWS

#include "Xi/CrashHandler/Config.hpp"

#include "Uploader.hpp"

namespace Xi {
namespace CrashHandler {

struct CrashHandler::_Impl : std::enable_shared_from_this<CrashHandler::_Impl> {
  std::unique_ptr<google_breakpad::ExceptionHandler> handle;
  Config config;

#if BOOST_OS_WINDOWS
  std::map<std::wstring, std::wstring> reportInfo;
  google_breakpad::CustomClientInfo nfo;
#endif  // BOOST_OS_WINDOWS
};

namespace {
static void upload(const std::string& file, const std::string& app, const std::string& server) {
  std::cout << "Uploading..." << std::endl;
  Uploader uploader{server};
  auto result = uploader.upload(file, app);
  if (result) {
    std::cout << "Upload succeeded. Thanks for supporting the project." << std::endl;
  } else {
    std::cout << "Upload failed. Thanks for supporting the project though." << std::endl;
  }
}

#if BOOST_OS_WINDOWS
static std::wstring to_wstring(const std::string str) {
  std::wstring wstr;
  wstr.resize(str.length() * 2 + 2, L'\0');
  size_t length = 0;
  auto err = mbstowcs_s(&length, &wstr[0], wstr.size(), str.data(), str.size());
  wstr.resize(length);
  if (err != 0)
    throw std::runtime_error{std::string{"Unable to convert '"} + str + "' to wide string."};
  return wstr;
}

static std::string to_string(const std::wstring wstr) {
  if (wstr.empty())
    return std::string();
  int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
  if (size_needed < 0)
    return std::string{};
  std::string strTo(static_cast<size_t>(size_needed), 0);
  WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
  return strTo;
}

static bool dumpCallback(const wchar_t* dump_path, const wchar_t* minidump_id, void* context, EXCEPTION_POINTERS*,
                         MDRawAssertionInfo*, bool succeeded) {
  std::string file;
  {
    std::wstring wfile;
    wfile += dump_path;
    wfile += minidump_id;
    wfile += L".dmp";
    file = to_string(wfile);
  }

  if (succeeded) {
    std::cout << "Dump file created: " << file << std::endl;
    CrashHandler::_Impl* impl = static_cast<CrashHandler::_Impl*>(context);
    if (impl->config.IsUploadEnabled)
      upload(file, impl->config.Application, impl->config.Server);
  } else
    std::cout << "Dump file creation failed.";
  return succeeded;
}
#endif  // BOOST_OS_WINDOWS

#if BOOST_OS_LINUX
static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor, void* context, bool succeeded) {
  if (succeeded) {
    std::cout << "Dump file created: " << descriptor.path() << std::endl;
    const auto impl = static_cast<CrashHandler::_Impl*>(context);
    if (impl->config.IsUploadEnabled)
      upload(descriptor.path(), impl->config.Application, impl->config.Server);
  } else {
    std::cout << "Dump file creation failed.";
  }
  return succeeded;
}
#endif  // BOOST_OS_LINUX

#if BOOST_OS_MACOS
static bool dumpCallback(const char* dump_dir, const char* minidump_id, void* context, bool succeeded) {
  const std::string file = std::string{dump_dir} + std::string{minidump_id} + std::string{".dmp"};
  if (succeeded) {
    std::cout << "Dump file created: " << file << std::endl;
    const auto impl = static_cast<CrashHandler::_Impl*>(context);
    if (impl->config.IsUploadEnabled)
      upload(file, impl->config.Application, impl->config.Server);
  } else {
    std::cout << "Dump file creation failed.";
  }
  return succeeded;
}
#endif  // BOOST_OS_MACOS
}  // namespace

CrashHandler::CrashHandler(const Config& config) {
  using namespace std::placeholders;

  FileSystem::ensureDirectoryExists(config.OutputPath).throwOnError();
  m_impl = std::make_shared<_Impl>();
  m_impl->config = config;

#if BOOST_OS_WINDOWS
  const auto& version = VersionInfo::VersionInfo::project();
  m_impl->reportInfo[L"ver"] = to_wstring(toString(version.version()));
  m_impl->reportInfo[L"prod"] = to_wstring(config.Application);
  m_impl->handle = std::make_unique<google_breakpad::ExceptionHandler>(
      to_wstring(config.OutputPath), nullptr, dumpCallback, m_impl.get(),
      google_breakpad::ExceptionHandler::HANDLER_ALL, MiniDumpNormal, L"", &m_impl->nfo);
#endif  // BOOST_OS_WINDOWS

#if BOOST_OS_LINUX
  const google_breakpad::MinidumpDescriptor desc{config.OutputPath};
  m_impl->handle =
      std::make_unique<google_breakpad::ExceptionHandler>(desc, nullptr, dumpCallback, m_impl.get(), true, -1);
#endif  // BOOST_OS_LINUX

#if BOOST_OS_MACOS
  m_impl->handle = std::make_unique<google_breakpad::ExceptionHandler>(config.OutputPath, nullptr, dumpCallback,
                                                                       m_impl.get(), true, "");
#endif  // BOOST_OS_MACOS
}

}  // namespace CrashHandler
}  // namespace Xi

#endif

﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Uploader.hpp"

#include "Xi/CrashHandler/Config/Config.hh"

#if defined(XI_CRASH_HANDLER_ENABLED)

#include <Xi/VersionInfo/VersionInfo.hpp>
#include <Xi/String/ToString.hpp>
#include <Xi/Http/Client.h>
#include <Xi/Http/MultipartFormDataBuilder.h>

namespace Xi {
namespace CrashHandler {

Uploader::Uploader(const std::string& breapkpadHost) : m_host{breapkpadHost} {
  /* */
}

Uploader::~Uploader() {
  /* */
}

std::optional<std::string> Uploader::upload(const std::string& file, const std::string& application) {
  try {
    Xi::Http::Client client{m_host, Http::SSLConfiguration::RootStoreClient};
    Xi::Http::MultipartFormDataBuilder builder;
    builder.addField("prod", application);
    builder.addField("ver", toString(VersionInfo::VersionInfo::project().version()));
    builder.addFile("upload_file_minidump", file);
    auto request = builder.request("/crashreports");
    const auto response = client.send(std::move(request)).get();
    return response.body();
  } catch (...) {
    return std::nullopt;
  }
}
}  // namespace CrashHandler
}  // namespace Xi

#endif

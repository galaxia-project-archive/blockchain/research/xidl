﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/CrashHandler/Config/Config.hh"

#if defined(XI_CRASH_HANDLER_ENABLED)

#include <string>
#include <optional>

#include <Xi/Extern/Push.hh>
#include <boost/optional.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>

namespace Xi {
namespace CrashHandler {

/*!
 * \brief The CrashUploader class encapsulates the process of uploading a crash dump to the breakpad server
 */
class Uploader {
 public:
  /*!
   * \brief CrashUploader constructs a new object to upload crash dumps
   * \param breapkpadHost The server hosting the breakpad server
   * \param port The server post for reaching the breakpad server
   */
  explicit Uploader(const std::string &breapkpadHost);
  XI_DELETE_COPY(Uploader);
  XI_DEFAULT_MOVE(Uploader);
  ~Uploader();

  /*!
   * \brief upload sends a crash dump to the breakpad server
   * \param file The crash dump file path
   * \param application The application that produced the crash dump
   * \return A string if the uploaded succeeded, otherwise no value
   */
  std::optional<std::string> upload(const std::string &file, const std::string &application);

 private:
  std::string m_host;
};

}  // namespace CrashHandler
}  // namespace Xi

#endif

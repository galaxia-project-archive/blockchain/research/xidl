﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <stdexcept>
#include <utility>

#include <Xi/Encoding/Base16.hh>
#include <Xi/Crypto/Hash/FastHash.hh>
#include <Xi/Crypto/Hash/TreeHash.hh>

#include "Xi/Crypto/FastHash.hpp"

namespace Xi {
namespace Crypto {

const FastHash FastHash::Null{};

Result<FastHash> FastHash::parse(const std::string &hex) {
  XI_ERROR_TRY
  FastHash reval;

  const auto ec = Encoding::Base16::decodeStrict(hex, reval.span());
  XI_ERROR_PROPAGATE(ec)

  return success(std::move(reval));
  XI_ERROR_CATCH
}

Result<FastHash> FastHash::compute(Xi::ConstByteSpan data) {
  XI_ERROR_TRY
  FastHash reval{};
  compute(data, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH
}

Result<void> FastHash::compute(Xi::ConstByteSpan data, FastHash &out) {
  XI_ERROR_TRY
  const auto ec = Hash::fastHash(data, out.span());
  XI_FAIL_IF_NOT(ec == Hash::HashError::Success, ec)
  return Xi::success();
  XI_ERROR_CATCH
}

Result<void> FastHash::compute(Xi::ConstByteSpan data, Xi::ByteSpan out) {
  XI_ERROR_TRY
  const auto ec = Hash::fastHash(data, out);
  XI_FAIL_IF_NOT(ec == Hash::HashError::Success, ec)
  return Xi::success();
  XI_ERROR_CATCH
}

Xi::Result<FastHash> FastHash::compute(Stream::InputStream &stream) {
  FastHash reval{};
  XI_ERROR_PROPAGATE_CATCH(compute(stream, reval))
  XI_SUCCEED(std::move(reval));
}

Xi::Result<void> FastHash::compute(Stream::InputStream &stream, FastHash &out) {
  xi_crypto_hash_fast_hash_state state;
  int ec = xi_crypto_hash_fast_hash_init(std::addressof(state));
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, Crypto::Hash::HashError::Internal)
  auto isEos = stream.isEndOfStream();
  XI_ERROR_PROPAGATE(isEos);

  while (!*isEos) {
    ByteArray<1024> buffer{};
    const auto read = stream.read(buffer);
    XI_ERROR_PROPAGATE(read)
    if (*read > 0) {
      ec = xi_crypto_hash_fast_hash_update(std::addressof(state), buffer.data(), *read);
      XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, Crypto::Hash::HashError::Internal)
    }
    isEos = stream.isEndOfStream();
    XI_ERROR_PROPAGATE(isEos);
  }

  ec = xi_crypto_hash_fast_hash_finalize(std::addressof(state), out.data());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, Crypto::Hash::HashError::Internal)
  XI_SUCCEED()
}

Result<FastHash> FastHash::computeMerkleTree(Xi::ConstByteSpan data, size_t count) {
  XI_ERROR_TRY
  FastHash reval{};
  computeMerkleTree(data, count, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH
}

Result<void> FastHash::computeMerkleTree(Xi::ConstByteSpan data, size_t count, FastHash &out) {
  XI_ERROR_TRY
  const auto ec = Hash::treeHash(data, count, out.span());
  XI_FAIL_IF_NOT(ec == Hash::HashError::Success, ec)
  return success();
  XI_ERROR_CATCH
}

Result<FastHash> FastHash::computeMerkleTree(ConstFastHashSpan data) {
  XI_ERROR_TRY
  FastHash reval{};
  computeMerkleTree(data, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH
}

Result<void> FastHash::computeMerkleTree(ConstFastHashSpan data, FastHash &out) {
  XI_ERROR_TRY
  const auto ec = Hash::treeHash(
      Xi::ConstByteSpan{reinterpret_cast<const Xi::Byte *>(data.data()), data.size() * FastHash::bytes()}, data.size(),
      out.span());
  XI_FAIL_IF_NOT(ec == Hash::HashError::Success, ec)
  return success();
  XI_ERROR_CATCH
}

FastHash::FastHash() {
  nullify();
}

FastHash::FastHash(FastHash::array_type raw) : array_type(std::move(raw)) {
}

FastHash::~FastHash() {
}

bool FastHash::isNull() const {
  return *this == FastHash::Null;
}

std::string FastHash::stringify() const {
  return Encoding::Base16::encode(span());
}

std::string FastHash::toShortString() const {
  return Encoding::Base16::encode(span().range(0, 4)) + "...";
}

Xi::ConstByteSpan FastHash::span() const {
  return Xi::ConstByteSpan{data(), bytes()};
}

Xi::ByteSpan FastHash::span() {
  return Xi::ByteSpan{data(), bytes()};
}

void FastHash::nullify() {
  fill(0);
}

}  // namespace Crypto
}  // namespace Xi

bool Xi::Crypto::serialize(FastHash &hash, Common::StringView name, CryptoNote::ISerializer &serializer) {
  return serializer.binary(hash.data(), FastHash::bytes(), name);
}

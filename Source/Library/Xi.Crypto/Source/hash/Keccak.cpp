﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Hash/Keccak.hh"

#include <utility>

#include <Xi/GlitchError.hpp>

XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Keccak::Hash256, 256);
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Keccak::Hash1600, 1600);

namespace Xi {
namespace Crypto {
namespace Hash {
namespace Keccak {

HashError compute(ConstByteSpan data, Hash256 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_keccak(data.data(), data.size_bytes(), out.data(), Hash256::bytes()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  XI_RETURN_SC(HashError::Success);
}

HashError compute(ConstByteSpan data, Hash1600 &out) {
  XI_RETURN_EC_IF_NOT(xi_crypto_hash_keccak_1600(data.data(), data.size_bytes(), out.data()) == XI_RETURN_CODE_SUCCESS,
                      HashError::Internal);
  XI_RETURN_SC(HashError::Success);
}

Hash256Stream::Hash256Stream() : m_error{std::nullopt} {
  const auto ec = xi_crypto_hash_keccak_init(std::addressof(m_state));
  if (ec != XI_RETURN_CODE_SUCCESS) {
    m_error = makeError(HashError::Internal);
  }
}

Hash256Stream &Hash256Stream::operator<<(ConstByteSpan data) {
  if (!m_error) {
    const auto ec = xi_crypto_hash_keccak_update(std::addressof(m_state), data.data(), data.size());
    if (ec != XI_RETURN_CODE_SUCCESS) {
      m_error = makeError(HashError::Internal);
    }
  }
  return *this;
}

Result<Hash256> Hash256Stream::take() {
  if (m_error) {
    Result<Hash256> reval{*m_error};
    m_error = makeError(GlitchError::Moved);
    return reval;
  }
  m_error = makeError(GlitchError::Moved);

  Hash256 reval{};
  const auto ec = xi_crypto_hash_keccak_finish(std::addressof(m_state), reval.data());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, HashError::Internal);
  XI_SUCCEED(std::move(reval))
}

Result<Hash256> Hash256Stream::get() const {
  if (m_error) {
    return *m_error;
  }

  xi_crypto_hash_keccak_state stateCopy{};
  auto ec = xi_crypto_hash_keccak_copy(std::addressof(stateCopy), std::addressof(m_state));
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, HashError::Internal);

  Hash256 reval{};
  ec = xi_crypto_hash_keccak_finish(std::addressof(stateCopy), reval.data());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, HashError::Internal);
  XI_SUCCEED(std::move(reval))
}

}  // namespace Keccak
}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Encoding/VarInt.hh"

#include <cassert>

#include "Xi/Global.hh"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::VarInt, Decode)
XI_ERROR_CODE_DESC(Overflow, "decoding resulted in an overflow")
XI_ERROR_CODE_DESC(OutOfMemory, "decoding expected an successing byte, but reached end of stream")
XI_ERROR_CODE_DESC(NoneCanonical, "a trailing zero byte was encoded")
XI_ERROR_CODE_CATEGORY_END()

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::VarInt, Encode)
XI_ERROR_CODE_DESC(OutOfMemory, "end of stream reached writing encoded bytes")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Encoding {
namespace VarInt {

Result<size_t> decode(ConstByteSpan source, int8_t &out) {
  auto reval = xi_encoding_varint_decode_int8(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, uint8_t &out) {
  auto reval = xi_encoding_varint_decode_uint8(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, int16_t &out) {
  auto reval = xi_encoding_varint_decode_int16(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, uint16_t &out) {
  auto reval = xi_encoding_varint_decode_uint16(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, int32_t &out) {
  auto reval = xi_encoding_varint_decode_int32(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, uint32_t &out) {
  auto reval = xi_encoding_varint_decode_uint32(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, int64_t &out) {
  auto reval = xi_encoding_varint_decode_int64(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> decode(ConstByteSpan source, uint64_t &out) {
  auto reval = xi_encoding_varint_decode_uint64(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, makeError(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, makeError(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, makeError(DecodeError::OutOfMemory));
  return success(reval);
}

Result<void> decodeStrict(ConstByteSpan source, int8_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, uint8_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, int16_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, uint16_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, int32_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, uint32_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, int64_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<void> decodeStrict(ConstByteSpan source, uint64_t &out) {
  auto read = decode(source, out);
  XI_ERROR_PROPAGATE(read);
  XI_FAIL_IF(*read != source.size(), DecodeError::NoneCanonical);
  XI_SUCCEED();
}

Result<size_t> encode(int8_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_int8(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(uint8_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_uint8(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(int16_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_int16(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(uint16_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_uint16(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(int32_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_int32(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(uint32_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_uint32(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(int64_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_int64(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

Result<size_t> encode(uint64_t value, ByteSpan dest) {
  auto reval = xi_encoding_varint_encode_uint64(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, makeError(EncodeError::OutOfMemory));
  return success(reval);
}

bool hasSuccessor(Byte current) {
  return xi_encoding_varint_has_successor(current);
}

ByteVector encode(int8_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<int8_t>());
  auto actualSize = xi_encoding_varint_encode_int8(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(uint8_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<uint8_t>());
  auto actualSize = xi_encoding_varint_encode_uint8(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(int16_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<int16_t>());
  auto actualSize = xi_encoding_varint_encode_int16(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(uint16_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<uint16_t>());
  auto actualSize = xi_encoding_varint_encode_uint16(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(int32_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<int32_t>());
  auto actualSize = xi_encoding_varint_encode_int32(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(uint32_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<uint32_t>());
  auto actualSize = xi_encoding_varint_encode_uint32(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(int64_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<int64_t>());
  auto actualSize = xi_encoding_varint_encode_int64(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

ByteVector encode(uint64_t value) {
  ByteVector reval{};
  reval.resize(maximumEncodingSize<uint64_t>());
  auto actualSize = xi_encoding_varint_encode_uint64(value, reval.data(), reval.size());
  assert(actualSize != XI_VARINT_ENCODE_OUT_OF_MEMORY);
  reval.resize(actualSize);
  return reval;
}

}  // namespace VarInt
}  // namespace Encoding
}  // namespace Xi

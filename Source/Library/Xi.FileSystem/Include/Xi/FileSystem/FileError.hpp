/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace FileSystem {

/// Error occured on a file operation.
XI_ERROR_CODE_BEGIN(File)

/// Internal OS error.
XI_ERROR_CODE_VALUE(Internal, 0x0001)
/// File operation actually encountered a directory.
XI_ERROR_CODE_VALUE(IsDirectory, 0x0002)
/// File operation expected the file does not exist, actually does.
XI_ERROR_CODE_VALUE(AlreadyExists, 0x0003)
/// File operation expected the file exist, actually does not.
XI_ERROR_CODE_VALUE(NotFound, 0x0004)

XI_ERROR_CODE_END(File, "FileSystem::FileError")

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, File)

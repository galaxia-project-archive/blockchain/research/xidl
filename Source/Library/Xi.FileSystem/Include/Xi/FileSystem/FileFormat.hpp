/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <set>
#include <string>

#include <Xi/Global.hh>

namespace Xi {
namespace FileSystem {

/*!
 * \brief The FileFormat class wraps common extensions being used for a file format.
 */
class FileFormat {
 public:
  /*!
   * \brief The Kind enum describes a kind of format, ie. image, binary or text
   */
  enum struct Kind {
    /// Indicates a text based format.
    Text,
    /// Indicates a binary based format, normally interpretation is application specific.
    Binary,
    /// Indicates the content of the file is compressed.
    Archive,
  };

 public:
  static const FileFormat Csv;
  static const FileFormat Xml;
  static const FileFormat Yaml;
  static const FileFormat Json;

  static const FileFormat Bin;

  static const FileFormat Zip;

 public:
  /*!
   * \brief FileFormat Constructs a new file format description for given extensions.
   * \param kind Format kind, ie. text based
   * \param extensions Extensions being used for this file format. (Should include the dot prefix entry wise)
   */
  explicit FileFormat(const Kind kind, const std::set<std::string>& extensions);

  /// Returns the kind (ie. text, binary) of this format.
  Kind kind() const;
  /// Returns all file extensions considered as a common extension for this format.
  const std::set<std::string>& extensions() const;
  /*!
   * \brief hasExtensions Queries all extensions of this format and checks if on corresponds to the given one.
   * \param The file extennsion to check agains.
   * \return True if one this format extenions corresponds to the given extension.
   */
  bool hasExtension(const std::string& ext) const;

 private:
  Kind m_kind;
  std::set<std::string> m_extensions;
};

}  // namespace FileSystem
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <string>
#include <filesystem>

#include <Xi/TypeSafe/Flag.hpp>

namespace Xi {
namespace FileSystem {

enum struct Permission : uint16_t {
  OwnerRead = 1 << 0,
  OwnerWrite = 1 << 1,
  OwnerExecute = 1 << 3,

  GroupRead = 1 << 4,
  GroupWrite = 1 << 5,
  GroupExecute = 1 << 6,

  WorldRead = 1 << 7,
  WorldWrite = 1 << 8,
  WorldExecute = 1 << 9,

  None = 0,
  OwnerReadWrite = OwnerRead | OwnerWrite,
  OwnerAll = OwnerRead | OwnerWrite | OwnerExecute,
  GroupReadWrite = GroupRead | GroupWrite,
  GroupAll = GroupRead | GroupWrite | GroupExecute,
  WorldReadWrite = WorldRead | WorldWrite,
  WorldAll = WorldRead | WorldWrite | WorldExecute,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(Permission)

Permission fromStd(std::filesystem::perms stdperms);
std::filesystem::perms toStd(Permission perms);

std::string stringify(const Permission perm);

}  // namespace FileSystem
}  // namespace Xi

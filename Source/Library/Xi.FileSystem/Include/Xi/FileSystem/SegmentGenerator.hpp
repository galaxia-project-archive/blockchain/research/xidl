/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <filesystem>

namespace Xi {
namespace FileSystem {

/*!
 * \brief The SegmentGenerator class encapsulates the concepts of iterating the segments of a filepath.
 */
class SegmentGenerator {
 public:
  using iterator = std::filesystem::path::const_iterator;

  iterator begin() const;
  iterator end() const;
  iterator cbegin() const;
  iterator cend() const;

 private:
  explicit SegmentGenerator(iterator begin, iterator end);

  friend class File;
  friend class Directory;

 private:
  iterator m_begin;
  iterator m_end;
};

}  // namespace FileSystem
}  // namespace Xi

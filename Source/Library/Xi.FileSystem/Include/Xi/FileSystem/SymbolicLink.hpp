/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <filesystem>

#include <Xi/Result.hpp>

namespace Xi {
namespace FileSystem {

/*!
 * \brief maximumSymbolicLinkRedirections The maximum count of continious symbolic links followed until an error is
 * returned.
 * \return maximum count of continious symbolic links supported.
 */
inline constexpr uint8_t maximumSymbolicLinkRedirections() {
  return 32;
}

/*!
 * \brief resolveSymbolicLink Tries to follow symbolic links until an none symbolic entry was found.
 * \param path The path to a potential symbolic link.
 * \param redirectionsThreshold Maximum number of symbolic links to follow, if exceeded an error is returned.
 * \return If one resultion is a file entry or does not exist the path to this entry otherwise an error.
 */
Result<std::filesystem::path> resolveSymbolicLink(const std::filesystem::path& path,
                                                  size_t redirectionsThreshold = maximumSymbolicLinkRedirections());

}  // namespace FileSystem
}  // namespace Xi

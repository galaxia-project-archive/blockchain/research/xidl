/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/Directory.hpp"

#include <cstdlib>
#include <sstream>
#include <filesystem>

#if defined(_WIN32)
#include <ShlObj.h>
#include <Xi/Windows/ComError.hpp>
#endif

#include <Xi/Exceptions.hpp>

#include "Xi/FileSystem/DirectoryError.hpp"
#include "Xi/FileSystem/SymbolicLink.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

const std::string Directory::Seperator {
#if defined(_WIN32)
  "\\"
#else
  "/"
#endif
};

std::string Directory::name() const {
  return m_path.filename().string();
}

std::optional<Directory> Directory::parent() const {
  const auto parent_ = m_path.parent_path();
  if (parent_ == m_path) {
    return std::nullopt;
  } else {
    return Directory{parent_};
  }
}

Result<Permission> Directory::permissions() const {
  XI_ERROR_TRY
  const auto status = queryStatus();
  XI_ERROR_PROPAGATE(status)
  XI_SUCCEED(fromStd(status->permissions()))
  XI_ERROR_CATCH
}

Result<void> Directory::setPermissions(const Permission perms) {
  std::error_code ec{/* */};
  fs::permissions(m_path, toStd(perms), ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED()
}

Result<SpaceInfo> Directory::space() const {
  XI_ERROR_TRY
  std::error_code ec{/* */};
  const auto stdSpaceInfo = fs::space(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(fromStd(stdSpaceInfo))
  XI_ERROR_CATCH
}

Result<bool> Directory::exists() const {
  std::error_code ec{/* */};
  const auto stdExists = fs::exists(m_path);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(stdExists, false)
  const auto type_ = type();
  XI_ERROR_PROPAGATE(type_);
  XI_SUCCEED(true)
}

Result<DirectoryType> Directory::type() const {
  XI_ERROR_TRY
  const auto status = queryStatus();
  XI_ERROR_PROPAGATE(status)
  const auto stdtype = status->type();
  switch (stdtype) {
    case fs::file_type::directory:
      XI_SUCCEED(DirectoryType::Regular)

    case fs::file_type::symlink:
      XI_SUCCEED(DirectoryType::SymbolicLink)

    case fs::file_type::none:
      XI_SUCCEED(DirectoryType::None)

    case fs::file_type::regular:
    case fs::file_type::block:
    case fs::file_type::character:
    case fs::file_type::fifo:
    case fs::file_type::socket:
      XI_FAIL(DirectoryError::IsFile)

    default:
      XI_SUCCEED(DirectoryType::Unknown)
  }
  XI_ERROR_CATCH
}

SegmentGenerator Directory::segments() const {
  return SegmentGenerator{m_path.begin(), m_path.end()};
}

Result<EntryGenerator> Directory::entries() const {
  std::error_code ec{/* */};
  fs::directory_iterator iter{m_path, ec};
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(EntryGenerator{begin(iter), end(iter)});
}

std::string Directory::stringify() const {
  return m_path.string();
}

Result<File> Directory::relativeFile(const std::filesystem::path &sub) const {
  return makeFile(m_path / sub);
}

Result<File> Directory::relativeFile(const File &sub) const {
  return relativeFile(sub.m_path);
}

Result<Directory> Directory::relativeDirectory(const std::filesystem::path &sub) const {
  return makeDirectory(m_path / sub);
}

Result<Directory> Directory::relativeDirectory(const Directory &sub) const {
  return relativeDirectory(sub.m_path);
}

Result<Directory> Directory::relativePathTo(const Directory &dir) const {
  std::error_code ec{};
  auto thisAbsolute = fs::absolute(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  auto dirAbsolute = fs::absolute(dir.m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)

  bool equiv = fs::equivalent(thisAbsolute, dirAbsolute, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED_IF(equiv, Directory{"." + Seperator});

  std::vector<fs::path> thisPath{};
  thisPath.emplace_back(thisAbsolute);
  for (auto current = thisAbsolute.parent_path(); current != current.parent_path(); current = current.parent_path()) {
    thisPath.emplace_back(current);
  }
  std::vector<fs::path> otherPath{};
  thisPath.emplace_back(dirAbsolute);
  for (auto current = dirAbsolute.parent_path(); current != current.parent_path(); current = current.parent_path()) {
    thisPath.emplace_back(current);
  }

  equiv = fs::equivalent(thisPath.back(), otherPath.back(), ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED_IF_NOT(equiv, Directory{dirAbsolute});

  if (thisPath.size() == 1) {
    std::stringstream builder{"." + Seperator};
    for (size_t i = 2; i <= otherPath.size(); ++i) {
      builder << otherPath[otherPath.size() - i].filename().string() << Seperator;
    }
    XI_SUCCEED(Directory{builder.str()})
  }

  if (otherPath.size() == 1) {
    std::stringstream builder{".." + Seperator};
    for (size_t i = 2; i <= otherPath.size(); ++i) {
      builder << "../" << Seperator;
    }
    XI_SUCCEED(Directory{builder.str()})
  }

  size_t i = 1;
  while (equiv) {
    i += 1;
    const auto &thisSegment = thisPath[thisPath.size() - i];
    const auto &otherSegment = otherPath[otherPath.size() - i];
    equiv = fs::equivalent(thisSegment, otherSegment, ec);
    XI_ERROR_CODE_PROPAGATE(ec)
  }

  std::stringstream builder{};
  for (size_t j = i; j <= thisPath.size(); ++j) {
    builder << ".." << Seperator;
  }
  for (size_t j = i; j <= otherPath.size(); ++j) {
    builder << otherPath[otherPath.size() - j].filename().string() << Seperator;
  }
  XI_SUCCEED(Directory{builder.str()})
}

Result<File> Directory::relativePathTo(const File &file) const {
  auto dirPath = relativePathTo(file.directory());
  XI_ERROR_PROPAGATE(dirPath)
  return dirPath->relativeFile(file.fullName());
}

Result<void> Directory::createRegular(const Permission perms) {
  const auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_);
  XI_FAIL_IF(*exists_, DirectoryError::AlreadyExists);
  std::error_code ec{/* */};
  fs::create_directories(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_ERROR_PROPAGATE_CATCH(setPermissions(perms))
  XI_SUCCEED()
}

Result<void> Directory::createRegularIfNotExists(const Permission perms) {
  const auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_);
  XI_SUCCEED_IF(*exists_)
  std::error_code ec{/* */};
  fs::create_directories(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_ERROR_PROPAGATE_CATCH(setPermissions(perms))
  XI_SUCCEED()
}

Result<void> Directory::createSymlink(const Directory &target) {
  const auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_);
  XI_FAIL_IF(*exists_, DirectoryError::AlreadyExists);
  std::error_code ec{/* */};
  fs::create_directory_symlink(m_path, target.m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED()
}

Result<void> Directory::createSymlink(const Directory &target, const Permission perms) {
  XI_ERROR_PROPAGATE_CATCH(createSymlink(target)) XI_ERROR_PROPAGATE_CATCH(setPermissions(perms)) XI_SUCCEED()
}

bool Directory::operator==(const Directory &rhs) const {
  return m_path == rhs.m_path;
}

bool Directory::operator!=(const Directory &rhs) const {
  return m_path != rhs.m_path;
}

bool Directory::operator<(const Directory &rhs) const {
  return m_path < rhs.m_path;
}

bool Directory::operator<=(const Directory &rhs) const {
  return m_path <= rhs.m_path;
}

bool Directory::operator>(const Directory &rhs) const {
  return m_path > rhs.m_path;
}

bool Directory::operator>=(const Directory &rhs) const {
  return m_path >= rhs.m_path;
}

Directory::Directory(const std::filesystem::path &path) : m_path{path} {
  /* */
}

Result<std::filesystem::file_status> Directory::queryStatus() const {
  std::error_code ec{/* */};
  auto status = fs::status(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED(status)
}

Result<Directory> makeDirectory(const std::filesystem::path &path) {
  std::error_code ec{};
  [[maybe_unused]] auto _ = fs::absolute(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(Directory{path})
}

namespace {
#if defined(_WIN32)
Result<std::string> getFolderPath(int csidl) {
  std::array<char, MAX_PATH> buffer{};
  const auto ec = SHGetFolderPathA(nullptr,             // Reserved
                                   csidl,               // Special folder type + create if necessary
                                   nullptr,             // Optional Access Token
                                   SHGFP_TYPE_CURRENT,  // Follow redirected special folders
                                   buffer.data()        // Output buffer (\0 terminated iff successfull)
  );

  XI_SUCCEED_IF(SUCCEEDED(ec), std::string{buffer.data()})
  XI_FAIL(Windows::toComError(ec))
}
#endif

Result<std::string> applicationDataDirectory() {
#if defined(_WIN32)
  return getFolderPath(CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE);
#elif defined(MAC_OSX)
  XI_SUCCEED(std::string{"~/Library/Application Support/"});
#else
  XI_SUCCEED(std::string{"~"});
#endif
}

}  // namespace

Result<Directory> makeDirectory(const SpecialDirectory special) {
  switch (special) {
    case SpecialDirectory::ApplicationData: {
      const auto path = applicationDataDirectory();
      XI_ERROR_PROPAGATE(path);
      return makeDirectory(*path);
    }
    case SpecialDirectory::Temporary: {
      std::error_code ec{/* */};
      const auto tmp = fs::temp_directory_path(ec);
      XI_ERROR_CODE_PROPAGATE(ec)
      return makeDirectory(tmp);
    }
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace FileSystem
}  // namespace Xi

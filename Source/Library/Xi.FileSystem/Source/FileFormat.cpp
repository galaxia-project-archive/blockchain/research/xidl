/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/FileFormat.hpp"

#include <Xi/String/String.hpp>

namespace Xi {
namespace FileSystem {

const FileFormat FileFormat::Csv{Kind::Text, {".csv"}};
const FileFormat FileFormat::Xml{Kind::Text, {".xml"}};
const FileFormat FileFormat::Yaml{Kind::Text, {".yml", ".yaml"}};
const FileFormat FileFormat::Json{Kind::Text, {".json"}};

const FileFormat FileFormat::Bin{Kind::Binary, {".bin", ".dat", ".data", ".raw"}};

const FileFormat FileFormat::Zip{Kind::Archive, {".zip"}};

FileFormat::FileFormat(const FileFormat::Kind kind, const std::set<std::string> &extensions)
    : m_kind{kind}, m_extensions{extensions} {
  /* */
}

FileFormat::Kind FileFormat::kind() const {
  return m_kind;
}

const std::set<std::string> &FileFormat::extensions() const {
  return m_extensions;
}

bool FileFormat::hasExtension(const std::string &ext) const {
  return extensions().find(toLower(ext)) != extensions().end();
}

}  // namespace FileSystem
}  // namespace Xi

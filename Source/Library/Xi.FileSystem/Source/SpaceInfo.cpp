/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/SpaceInfo.hpp"

namespace Xi {
namespace FileSystem {

SpaceInfo::SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_) : SpaceInfo(available_, capacity_, available_) {
  /* */
}

SpaceInfo::SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_, Memory::Bytes free_)
    : m_available{available_}, m_capacity{capacity_}, m_free{free_} {
  /* */
}

Memory::Bytes SpaceInfo::available() const {
  return m_available;
}

Memory::Bytes SpaceInfo::capacity() const {
  return m_capacity;
}

Memory::Bytes SpaceInfo::free() const {
  return m_free;
}

Memory::Bytes SpaceInfo::taken() const {
  return capacity() - free();
}

SpaceInfo fromStd(const std::filesystem::space_info &space) {
  return SpaceInfo{Memory::Bytes{space.available}, Memory::Bytes{space.capacity}, Memory::Bytes{space.free}};
}

}  // namespace FileSystem
}  // namespace Xi

﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Http/HeaderContainer.h"

#include <sstream>
#include <stdexcept>
#include <iterator>

#include <Xi/Extern/Push.hh>
#include <boost/algorithm/string.hpp>
#include <boost/beast.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/String/String.hpp>
#include <Xi/Encoding/Base64.hh>

static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::WWWAuthenticate) ==
        boost::beast::http::field::www_authenticate,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Authorization) ==
        boost::beast::http::field::authorization,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Connection) ==
        boost::beast::http::field::connection,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::KeepAlive) ==
        boost::beast::http::field::keep_alive,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Accept) == boost::beast::http::field::accept,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::AcceptCharset) ==
        boost::beast::http::field::accept_charset,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::AcceptEncoding) ==
        boost::beast::http::field::accept_encoding,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::ContentType) ==
        boost::beast::http::field::content_type,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::ContentEncoding) ==
        boost::beast::http::field::content_encoding,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Location) == boost::beast::http::field::location,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Allow) == boost::beast::http::field::allow,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::Server) == boost::beast::http::field::server,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::AccessControlAllowOrigin) ==
        boost::beast::http::field::access_control_allow_origin,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderContainer::AccessControlAllowMethods) ==
        boost::beast::http::field::access_control_allow_methods,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");

void Xi::Http::HeaderContainer::setRequiredAuthenticationScheme(Xi::Http::AuthenticationType authType) {
  set(WWWAuthenticate, toString(authType));
}

boost::optional<Xi::Http::AuthenticationType> Xi::Http::HeaderContainer::requiredAuthenticationScheme() const {
  const auto search = get(WWWAuthenticate);
  if (search)
    return fromString<Xi::Http::AuthenticationType>(*search).takeOrThrow();
  else
    return boost::optional<Xi::Http::AuthenticationType>{};
}

void Xi::Http::HeaderContainer::setBasicAuthorization(const std::string &username, const std::string &password) {
  setBasicAuthorization(BasicCredentials{username, password});
}

void Xi::Http::HeaderContainer::setBasicAuthorization(const Xi::Http::BasicCredentials &credentials) {
  const auto credString = toString(credentials);
  const auto credEnc = Encoding::Base64::encode(asConstByteSpan(credString));
  set(Authorization, toString(AuthenticationType::Basic) + " " + credEnc);
}

void Xi::Http::HeaderContainer::setBearerAuthorization(const std::string &token) {
  BearerCredentials cred{token};
  setBearerAuthorization(cred);
}

void Xi::Http::HeaderContainer::setBearerAuthorization(const Xi::Http::BearerCredentials &bearer) {
  set(Authorization, toString(AuthenticationType::Bearer) + " " + bearer.token());
}

void Xi::Http::HeaderContainer::setAllow(std::initializer_list<Method> method) {
  if (method.size() == 0)
    throw std::invalid_argument{"you need to support at least one method."};
  std::stringstream builder{};
  builder << toString(*method.begin());
  for (auto it = std::next(method.begin()); it != method.end(); ++it)
    builder << ", " << toString(*it);
  set(Allow, builder.str());
}

void Xi::Http::HeaderContainer::setAccept(Xi::Http::ConstContentTypeSpan types) {
  std::vector<std::string> strtypes;
  strtypes.reserve(types.size());
  for (const auto type : types) {
    strtypes.emplace_back(toString(type));
  }
  set(Accept, join(strtypes, ","));
}

std::vector<Xi::Http::ContentType> Xi::Http::HeaderContainer::accept() const {
  std::vector<Xi::Http::ContentType> reval{};
  const auto search = get(Accept);
  if (search) {
    const auto strtypes = split(*search, ",", make_copy_v);
    for (const auto &str : strtypes) {
      const auto parts = split(trim(str), ";", make_copy_v);
      if (parts.empty()) {
        throw std::runtime_error{"empty accept part"};
      }
      reval.emplace_back(fromString<Xi::Http::ContentType>(parts[0]).valueOrThrow());
    }
  }
  return reval;
}

boost::optional<Xi::Http::BasicCredentials> Xi::Http::HeaderContainer::basicAuthorization() const {
  const auto search = get(Authorization);
  if (search) {
    const std::string authPrefix = toString(AuthenticationType::Basic) + " ";
    if (!Xi::startsWith(*search, authPrefix)) {
      return boost::optional<BasicCredentials>{};
    }
    const std::string encodedAuth = search->substr(authPrefix.size());
    const auto decodedAuth = Encoding::Base64::decode(encodedAuth).takeOrThrow();
    return fromString<BasicCredentials>(
               std::string{reinterpret_cast<const char *>(decodedAuth.data()), decodedAuth.size()})
        .takeOrThrow();
  } else
    return boost::optional<BasicCredentials>{};
}

std::optional<Xi::Http::BearerCredentials> Xi::Http::HeaderContainer::bearerAuthorization() const {
  const auto search = get(Authorization);
  if (search) {
    const std::string authPrefix = toString(AuthenticationType::Bearer) + " ";
    if (!Xi::startsWith(*search, authPrefix)) {
      return std::nullopt;
    }
    const std::string auth = search->substr(authPrefix.size());
    return fromString<BearerCredentials>(auth).takeOrThrow();
  } else
    return std::nullopt;
}

void Xi::Http::HeaderContainer::setContentType(Xi::Http::ContentType _contentType) {
  set(ContentType, toString(_contentType));
}

void Xi::Http::HeaderContainer::setAcceptedContentEncodings(
    std::initializer_list<Xi::Http::ContentEncoding> encodings) {
  if (encodings.size() == 0)
    throw std::invalid_argument{"you need to support at least one encoding."};
  std::stringstream builder{};
  builder << toString(*encodings.begin());
  for (auto it = std::next(encodings.begin()); it != encodings.end(); ++it)
    builder << ", " << toString(*it);
  set(AcceptEncoding, builder.str());
}

boost::optional<std::vector<Xi::Http::ContentEncoding> > Xi::Http::HeaderContainer::acceptedContentEncodings() const {
  const auto search = get(AcceptEncoding);
  if (search) {
    std::string encodings = *search;
    std::string weightings = "";
    auto findWeightingDelimiter = encodings.find(";");
    if (findWeightingDelimiter != std::string::npos) {
      weightings = encodings.substr(findWeightingDelimiter + 1);
      encodings = encodings.substr(0, findWeightingDelimiter);
    }
    std::vector<std::string> supportedEncodings;
    boost::split(supportedEncodings, *search, boost::is_any_of(","));
    for (auto &encoding : supportedEncodings)
      boost::trim(encoding);
    std::vector<Xi::Http::ContentEncoding> reval;
    reval.reserve(supportedEncodings.size());
    for (const auto &i : supportedEncodings) {
      reval.emplace_back(fromString<Xi::Http::ContentEncoding>(i).takeOrThrow());
    }
    return std::move(reval);
  } else
    return boost::optional<std::vector<Xi::Http::ContentEncoding> >{};
}

bool Xi::Http::HeaderContainer::acceptsContentEncoding(Xi::Http::ContentEncoding encoding) const {
  const auto acceptedEncodings = acceptedContentEncodings();
  if (acceptedEncodings) {
    return std::find(acceptedEncodings->begin(), acceptedEncodings->end(), encoding) != acceptedEncodings->end();
  } else {
    return false;
  }
}

void Xi::Http::HeaderContainer::setContentEncoding(Xi::Http::ContentEncoding encoding) {
  set(ContentEncoding, toString(encoding));
}

boost::optional<Xi::Http::ContentEncoding> Xi::Http::HeaderContainer::contentEncoding() const {
  const auto search = get(ContentEncoding);
  if (search)
    return fromString<Xi::Http::ContentEncoding>(*search).takeOrThrow();
  else
    return boost::optional<Xi::Http::ContentEncoding>{};
}

boost::optional<Xi::Http::ContentType> Xi::Http::HeaderContainer::contentType() const {
  const auto search = get(ContentType);
  if (search) {
    const auto raw = *search;
    auto seperator = std::find(raw.begin(), raw.end(), ';');
    return fromString<Xi::Http::ContentType>(std::string{raw.begin(), seperator}).takeOrThrow();
  } else
    return boost::optional<Xi::Http::ContentType>{};
}

boost::optional<std::string> Xi::Http::HeaderContainer::location() const {
  auto search = m_rawHeaders.find(Location);
  if (search == m_rawHeaders.end())
    return boost::optional<std::string>{};
  else
    return boost::optional<std::string>{search->second};
}

Xi::Http::HeaderContainer::const_iterator Xi::Http::HeaderContainer::begin() const {
  return m_rawHeaders.begin();
}

Xi::Http::HeaderContainer::const_iterator Xi::Http::HeaderContainer::end() const {
  return m_rawHeaders.end();
}

boost::optional<std::string> Xi::Http::HeaderContainer::get(Xi::Http::HeaderContainer::Header header) const {
  auto search = m_rawHeaders.find(header);
  if (search == m_rawHeaders.end())
    return boost::optional<std::string>{};
  else
    return boost::optional<std::string>{search->second};
}

void Xi::Http::HeaderContainer::setAccessControlRequestMethods(std::initializer_list<Xi::Http::Method> methods) {
  if (methods.size() == 0)
    throw std::invalid_argument{"you need to support at least one method."};
  std::stringstream builder{};
  builder << toString(*methods.begin());
  for (auto it = std::next(methods.begin()); it != methods.end(); ++it)
    builder << ", " << toString(*it);
  set(AccessControlAllowMethods, builder.str());
}

void Xi::Http::HeaderContainer::set(Xi::Http::HeaderContainer::Header header, const std::string &raw) {
  m_rawHeaders[header] = raw;
}

bool Xi::Http::HeaderContainer::contains(Xi::Http::HeaderContainer::Header header) const {
  return m_rawHeaders.find(header) != m_rawHeaders.end();
}

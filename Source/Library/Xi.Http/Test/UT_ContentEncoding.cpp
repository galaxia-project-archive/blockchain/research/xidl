﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <string>

#include <Xi/String/FromString.hpp>
#include <Xi/Testing/Result.hpp>
#include <Xi/Http/ContentEncoding.h>

#define XI_TESTSUITE T_Xi_Http_ContentEncoding

TEST(XI_TESTSUITE, ToString) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_ANY_THROW(toString(static_cast<Http::ContentEncoding>(-1)));
  EXPECT_THAT(toString(Http::ContentEncoding::Gzip), Eq("gzip"));
  EXPECT_THAT(toString(Http::ContentEncoding::Identity), Eq("identity"));
  EXPECT_THAT(toString(Http::ContentEncoding::Deflate), Eq("deflate"));
  EXPECT_THAT(toString(Http::ContentEncoding::Brotli), Eq("br"));
  EXPECT_THAT(toString(Http::ContentEncoding::Compress), Eq("compress"));
}

TEST(XI_TESTSUITE, LexicalCast) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Testing;

  EXPECT_THAT(fromString<Http::ContentEncoding>(""), IsFailure());
  EXPECT_THAT(fromString<Http::ContentEncoding>("GZIP"), IsFailure());
  EXPECT_THAT(fromString<Http::ContentEncoding>(" gzip"), IsFailure());
  EXPECT_THAT(fromString<Http::ContentEncoding>("br "), IsFailure());
  EXPECT_EQ(fromString<Http::ContentEncoding>("gzip").valueOrThrow(), Http::ContentEncoding::Gzip);
  EXPECT_EQ(fromString<Http::ContentEncoding>("compress").valueOrThrow(), Http::ContentEncoding::Compress);
  EXPECT_EQ(fromString<Http::ContentEncoding>("deflate").valueOrThrow(), Http::ContentEncoding::Deflate);
  EXPECT_EQ(fromString<Http::ContentEncoding>("identity").valueOrThrow(), Http::ContentEncoding::Identity);
  EXPECT_EQ(fromString<Http::ContentEncoding>("br").valueOrThrow(), Http::ContentEncoding::Brotli);
}

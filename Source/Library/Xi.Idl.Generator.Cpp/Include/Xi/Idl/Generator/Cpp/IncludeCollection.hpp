/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <vector>
#include <string>

#include <Xi/Global.hh>

#include "Xi/Idl/Generator/Cpp/Include.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

class IncludeCollection {
 public:
  using container_type = std::vector<Include>;

 public:
  explicit IncludeCollection() = default;
  XI_DEFAULT_COPY(IncludeCollection);
  XI_DEFAULT_MOVE(IncludeCollection);
  ~IncludeCollection() = default;

  void add(const Include& include);
  const container_type& includes(Include::Kind kind) const;
  container_type includes() const;

 private:
  mutable std::map<Include::Kind, container_type> m_includes;
};

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

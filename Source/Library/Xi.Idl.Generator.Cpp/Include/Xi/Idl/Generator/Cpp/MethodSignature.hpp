/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>
#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Idl/Generator/Namespace.hpp>

#include "Xi/Idl/Generator/Cpp/TypeSignature.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

class MethodArgument {
 public:
  struct Common {
    static const MethodArgument StringView;
  };

 public:
  explicit MethodArgument(const std::string& name_, const TypeSignature& type_);
  XI_DEFAULT_COPY(MethodArgument);
  XI_DEFAULT_MOVE(MethodArgument);
  ~MethodArgument() = default;

  const std::string& name() const;
  const TypeSignature& type() const;

 private:
  std::string m_name;
  TypeSignature m_type;
};

using MethodArgumentVector = std::vector<MethodArgument>;

class MethodSignature {
 public:
  XI_DEFAULT_COPY(MethodSignature);
  XI_DEFAULT_MOVE(MethodSignature);
  ~MethodSignature() = default;

  SharedConstNamespace namespace_() const;
  const std::string& name() const;
  const std::optional<TypeSignature>& containingType() const;
  const MethodArgumentVector& arguments() const;
  bool isConst() const;

 private:
  explicit MethodSignature() = default;
  friend class MethodSignatureBuilder;

 private:
  SharedConstNamespace m_namespace;
  std::string m_name;
  std::optional<TypeSignature> m_containingType;
  MethodArgumentVector m_arguments;
  bool m_isConst;
};

class MethodSignatureBuilder {
 public:
  MethodSignatureBuilder() = default;
  XI_DELETE_COPY(MethodSignatureBuilder);
  XI_DELETE_MOVE(MethodSignatureBuilder);
  ~MethodSignatureBuilder() = default;

  MethodSignatureBuilder& withNamespace(SharedConstNamespace ns);
  MethodSignatureBuilder& withName(const std::string& name);
  MethodSignatureBuilder& withContainingType(const std::optional<TypeSignature>& type);
  MethodSignatureBuilder& withArgument(const MethodArgument& arg);
  MethodSignatureBuilder& withIsConst(bool isConst);

  MethodSignature build();

 private:
  MethodSignature m_signature;
};

MethodSignature makeStructSerializationMethodSignature(const Type& type);

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

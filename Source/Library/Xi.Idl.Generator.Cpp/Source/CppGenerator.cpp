/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CppGenerator.hpp"

#include <algorithm>
#include <sstream>
#include <utility>

#include <Xi/Exceptions.hpp>
#include <Xi/String/Split.hpp>
#include <Xi/FileSystem.h>
#include <Xi/String/String.hpp>

#include <Xi/Idl/Generator/Field.hpp>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

namespace {
const std::string Copyright{
    R"^-.-^(/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */)^-.-^"};
const std::string IncludeGuard{"#pragma once"};
}  // namespace

std::string CppGenerator::includeStatement(const Include& include) {
  switch (include.kind()) {
    case Include::Kind::StandardLibrary:
    case Include::Kind::ExternalLibrary:
    case Include::Kind::InternalLibrary:
      return "#include <" + include.path() + ">";
    case Include::Kind::ThisLibrary:
    case Include::Kind::SourceHeader:
      return "#include \"" + include.path() + "\"";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

CppGenerator::CppGenerator(Stream::UniqueOutputStream out) : m_out{std::move(out)} {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, m_out);
}

Result<void> CppGenerator::printCopyright() {
  return stream().writeStrict(asConstByteSpan(Copyright));
}

Result<void> CppGenerator::printIncludeGuard() {
  return stream().writeStrict(asConstByteSpan(IncludeGuard));
}

Result<void> CppGenerator::printStaticHeaderPrefix() {
  XI_ERROR_PROPAGATE_CATCH(printCopyright());
  XI_ERROR_PROPAGATE_CATCH(printNewLine());
  XI_ERROR_PROPAGATE_CATCH(printNewLine());
  XI_ERROR_PROPAGATE_CATCH(printIncludeGuard());
  XI_SUCCEED()
}

Result<void> CppGenerator::printNewLine(size_t count) {
  while (count-- > 0) {
    XI_ERROR_PROPAGATE_CATCH(stream().writeStrict(asConstByteSpan(FileSystem::NewLine)));
  }
  XI_SUCCEED()
}

Result<void> CppGenerator::print(const std::string& raw) {
  return stream().writeStrict(asConstByteSpan(raw));
}

Result<void> CppGenerator::printDocumentationEntry(const std::string& entry, const std::string& value) {
  XI_SUCCEED_IF(value.empty())
  XI_ERROR_PROPAGATE(printNewLine())
  const auto lines = split(value, "\n", make_copy_v);
  std::string spaces{};
  spaces.resize(3U + entry.size(), ' ');
  if (lines.size() > 0) {
    XI_ERROR_PROPAGATE(print(" * \\"))
    XI_ERROR_PROPAGATE(print(entry))
    XI_ERROR_PROPAGATE(print(" "))
    XI_ERROR_PROPAGATE(print(lines.front()))
    for (size_t i = 1; i < lines.size(); i++) {
      const auto& line = lines[i];
      XI_ERROR_PROPAGATE(printNewLine())
      XI_ERROR_PROPAGATE(print(" *"))
      XI_ERROR_PROPAGATE(print(spaces))
      XI_ERROR_PROPAGATE(print(line))
    }
  }
  XI_SUCCEED()
}

Result<void> CppGenerator::printDocumentation(const Documentation& doc, const std::string& var) {
  XI_ERROR_PROPAGATE_CATCH(print("/*!"));
  if (!var.empty()) {
    XI_ERROR_PROPAGATE(printNewLine())
    XI_ERROR_PROPAGATE(print(" * \\var "))
    XI_ERROR_PROPAGATE(print(var))
  }
  XI_ERROR_PROPAGATE_CATCH(printDocumentationEntry("brief", doc.brief()));
  XI_ERROR_PROPAGATE_CATCH(printDocumentationEntry("details", doc.detailed()));
  XI_ERROR_PROPAGATE(printNewLine())
  XI_ERROR_PROPAGATE_CATCH(print(" */"));
  XI_SUCCEED()
}

Result<void> CppGenerator::printDocumentation(const Documentation& doc, const std::string& enum_,
                                              const std::string& field) {
  return printDocumentation(doc, enum_ + "::" + field);
}

Stream::OutputStream& CppGenerator::stream() {
  return *m_out;
}

Result<void> CppGenerator::printInclude(const Include& include) {
  return stream().writeStrict(asConstByteSpan(includeStatement(include)));
}

Result<void> CppGenerator::printInclude(const IncludeCollection& includes_) {
  auto includes = includes_.includes();
  XI_SUCCEED_IF(includes.empty())
  std::sort(begin(includes), end(includes));
  XI_ERROR_PROPAGATE_CATCH(printInclude(includes[0]))
  for (size_t i = 1; i < includes.size(); ++i) {
    XI_ERROR_PROPAGATE_CATCH(printNewLine());
    if (includes[i].kind() != includes[i - 1].kind()) {
      XI_ERROR_PROPAGATE_CATCH(printNewLine());
    }
    XI_ERROR_PROPAGATE_CATCH(printInclude(includes[i]))
  }
  XI_SUCCEED()
}

std::string CppGenerator::constant(const Constant& constant_) {
  if (const auto string = std::get_if<std::string>(std::addressof(constant_))) {
    auto escaped = replace(*string, "\n", "\\n");
    escaped = replace(escaped, "\"", "\\\"");
    return "\"" + escaped + "\"";
  } else if (const auto size = std::get_if<std::size_t>(std::addressof(constant_))) {
    return std::to_string(*size);
  } else if (const auto boolean = std::get_if<bool>(std::addressof(constant_))) {
    return *boolean ? "true" : "false";
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

Result<void> CppGenerator::printConstant(const Constant& constant_) {
  return stream().writeStrict(asConstByteSpan(constant(constant_)));
}

std::string CppGenerator::genericArgument(const TypeSignature::GenericArgument& arg,
                                          SharedConstNamespace currentNamespace) {
  if (const auto constant_ = std::get_if<Constant>(std::addressof(arg))) {
    return constant(*constant_);
  } else if (const auto signature_ = std::get_if<TypeSignature>(std::addressof(arg))) {
    return typeSignature(*signature_, currentNamespace);
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

std::string CppGenerator::typeSignature(const TypeSignature& signature, SharedConstNamespace currentNamespace) {
  std::stringstream builder{};
  switch (signature.decorator()) {
    case TypeSignature::Decorator::Const:
    case TypeSignature::Decorator::ConstPointer:
    case TypeSignature::Decorator::ConstReference:
      builder << "const ";
      break;

    default:
      break;
  }

  std::vector<SharedConstNamespace> nsPath;
  if (currentNamespace) {
    nsPath = currentNamespace->shortestPathTo(signature.namespace_());
  } else {
    builder << "::";
    nsPath = signature.namespace_()->path();
  }
  for (size_t i = 0; i < nsPath.size(); ++i) {
    builder << nsPath[i]->name() << "::";
  }

  builder << signature.name();
  const auto& genericArguments = signature.genericArguments();
  if (!genericArguments.empty()) {
    builder << "<";
    builder << genericArgument(genericArguments[0], currentNamespace);
    for (size_t i = 1; i < genericArguments.size(); ++i) {
      builder << ", " << genericArgument(genericArguments[i], currentNamespace);
    }
    builder << ">";
  }

  switch (signature.decorator()) {
    case TypeSignature::Decorator::Pointer:
    case TypeSignature::Decorator::ConstPointer:
      builder << "*";
      break;

    case TypeSignature::Decorator::Reference:
    case TypeSignature::Decorator::ConstReference:
      builder << "&";
      break;

    default:
      break;
  }

  return builder.str();
}

Result<void> CppGenerator::printTypeSignature(const TypeSignature& signature, SharedConstNamespace currentNamespace) {
  return stream().writeStrict(asConstByteSpan(typeSignature(signature, currentNamespace)));
}

std::string CppGenerator::beginStruct(const std::string& name) {
  return "struct " + name + " {";
}

std::string CppGenerator::beginStruct(const std::string& name, TypeSignature inheritance,
                                      SharedConstNamespace currentNamespace) {
  std::stringstream builder{};
  builder << "struct " << name << " : " << typeSignature(inheritance, currentNamespace) << " {";
  return builder.str();
}

Result<void> CppGenerator::printBeginStruct(const std::string& name) {
  return stream().writeStrict(asConstByteSpan(beginStruct(name)));
}

Result<void> CppGenerator::printBeginStruct(const std::string& name, TypeSignature inheritance,
                                            SharedConstNamespace currentNamespace) {
  return print(beginStruct(name, inheritance, currentNamespace));
}

std::string CppGenerator::endStruct(const std::string& name) {
  return "};  // struct " + name;
}

Result<void> CppGenerator::printEndStruct(const std::string& name) {
  return stream().writeStrict(asConstByteSpan(endStruct(name)));
}

std::string CppGenerator::structMember(const Field& field, SharedConstNamespace ns) {
  std::stringstream builder{};

  builder << typeSignature(makeTypeSignature(field.type()), ns) << " " << structMemberName(field) << ";";
  return builder.str();
}

Result<void> CppGenerator::printStructMember(const Field& field, SharedConstNamespace ns) {
  return stream().writeStrict(asConstByteSpan(structMember(field, ns)));
}

std::string CppGenerator::structMemberName(const Field& field) {
  auto fieldName = field.name();
  if (fieldName.size() > 0) {
    fieldName[0] = toLower(fieldName[0]);
  }
  return fieldName;
}

Result<void> CppGenerator::printStructMemberName(const Field& field) {
  return stream().writeStrict(asConstByteSpan(structMemberName(field)));
}

std::string CppGenerator::noDiscard() {
  return "[[nodiscard]]";
}

Result<void> CppGenerator::printNoDiscard() {
  return stream().writeStrict(asConstByteSpan(noDiscard()));
}

std::string CppGenerator::openNamespace(SharedConstNamespace ns) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns);
  return "namespace " + ns->name() + " {";
}

std::string CppGenerator::openNamespaces(const std::vector<SharedConstNamespace>& ns) {
  std::vector<std::string> statements{};
  std::transform(begin(ns), end(ns), std::back_inserter(statements),
                 std::bind(&CppGenerator::openNamespace, this, std::placeholders::_1));
  return join(statements, FileSystem::NewLine);
}

Result<void> CppGenerator::printOpenNamespace(SharedConstNamespace ns) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns);
  return stream().writeStrict(asConstByteSpan(openNamespace(ns)));
}

Result<void> CppGenerator::printOpenNamespaces(const std::vector<SharedConstNamespace>& ns) {
  return stream().writeStrict(asConstByteSpan(openNamespaces(ns)));
}

std::string CppGenerator::closeNamespace(SharedConstNamespace ns) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns);
  return "} // namespace " + ns->name();
}

std::string CppGenerator::closeNamespaces(const std::vector<SharedConstNamespace>& ns) {
  std::vector<std::string> statements{};
  std::transform(ns.rbegin(), ns.rend(), std::back_inserter(statements),
                 std::bind(&CppGenerator::closeNamespace, this, std::placeholders::_1));
  return join(statements, FileSystem::NewLine);
}

Result<void> CppGenerator::printCloseNamespace(SharedConstNamespace ns) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns);
  return stream().writeStrict(asConstByteSpan(closeNamespace(ns)));
}

Result<void> CppGenerator::printCloseNamespaces(const std::vector<SharedConstNamespace>& ns) {
  return stream().writeStrict(asConstByteSpan(closeNamespaces(ns)));
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

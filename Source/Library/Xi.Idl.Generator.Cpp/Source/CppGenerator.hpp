/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <utility>
#include <initializer_list>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Idl/Generator/Namespace.hpp>
#include <Xi/Idl/Generator/Field.hpp>
#include <Xi/Idl/Generator/Documentation.hpp>
#include <Xi/Idl/Generator/FlagEntry.hpp>
#include <Xi/Idl/Generator/EnumEntry.hpp>

#include "Xi/Idl/Generator/Cpp/Constant.hpp"
#include "Xi/Idl/Generator/Cpp/Include.hpp"
#include "Xi/Idl/Generator/Cpp/IncludeCollection.hpp"
#include "Xi/Idl/Generator/Cpp/TypeSignature.hpp"
#include "Xi/Idl/Generator/Cpp/MethodSignature.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

/*!
 * \brief The CppGenerator class generates actual cpp code.
 *
 * \attention all print functions are considered to print new lines only if they are included in their boundary. Every
 * previous/continuing method call will expect the stream at the last character printed.
 */
class CppGenerator {
 public:
  explicit CppGenerator(Stream::UniqueOutputStream out);
  XI_DELETE_COPY(CppGenerator);
  XI_DEFAULT_MOVE(CppGenerator);

  // Common
  Result<void> printCopyright();
  Result<void> printNewLine(size_t count = 1);
  Result<void> print(const std::string& raw);
  Result<void> printDocumentationEntry(const std::string& entry, const std::string& value);
  Result<void> printDocumentation(const Documentation& doc, const std::string& var = "");
  Result<void> printDocumentation(const Documentation& doc, const std::string& enum_, const std::string& field);
  Stream::OutputStream& stream();

  // Header
  Result<void> printIncludeGuard();
  Result<void> printStaticHeaderPrefix();

  // Includes
  std::string includeStatement(const Include& include);
  Result<void> printInclude(const Include& include);
  Result<void> printInclude(const IncludeCollection& includes);

  // Constants
  std::string constant(const Constant& constant);
  Result<void> printConstant(const Constant& constant);

  // Type Signatures
  std::string genericArgument(const TypeSignature::GenericArgument& arg,
                              SharedConstNamespace currentNamespace = nullptr);
  std::string typeSignature(const TypeSignature& signature, SharedConstNamespace currentNamespace = nullptr);
  Result<void> printTypeSignature(const TypeSignature& signature, SharedConstNamespace currentNamespace = nullptr);

  // Struct
  std::string beginStruct(const std::string& name);
  std::string beginStruct(const std::string& name, TypeSignature inheritance, SharedConstNamespace currentNamespace);
  Result<void> printBeginStruct(const std::string& name);
  Result<void> printBeginStruct(const std::string& name, TypeSignature inheritance,
                                SharedConstNamespace currentNamespace);
  std::string endStruct(const std::string& name);
  Result<void> printEndStruct(const std::string& name);
  std::string structMember(const Field& field, SharedConstNamespace ns);
  Result<void> printStructMember(const Field& field, SharedConstNamespace ns);

  std::string structMemberName(const Field& field);
  Result<void> printStructMemberName(const Field& field);

  // Methods
  std::string noDiscard();
  Result<void> printNoDiscard();

  // Namespace
  std::string openNamespace(SharedConstNamespace ns);
  std::string openNamespaces(const std::vector<SharedConstNamespace>& ns);
  Result<void> printOpenNamespace(SharedConstNamespace ns);
  Result<void> printOpenNamespaces(const std::vector<SharedConstNamespace>& ns);
  std::string closeNamespace(SharedConstNamespace ns);
  std::string closeNamespaces(const std::vector<SharedConstNamespace>& ns);
  Result<void> printCloseNamespace(SharedConstNamespace ns);
  Result<void> printCloseNamespaces(const std::vector<SharedConstNamespace>& ns);

 private:
  Stream::UniqueOutputStream m_out;
};

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/Include.hpp"

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

const Include Include::Std::cinttypes{"cinttypes", Kind::StandardLibrary};
const Include Include::Std::array{"array", Kind::StandardLibrary};
const Include Include::Std::vector{"vector", Kind::StandardLibrary};
const Include Include::Std::string{"string", Kind::StandardLibrary};
const Include Include::Std::variant{"variant", Kind::StandardLibrary};
const Include Include::Std::optional{"optional", Kind::StandardLibrary};
const Include Include::Std::utility{"utility", Kind::StandardLibrary};

const Include Include::Xi::Global{"Xi/Gloabl.hh", Kind::InternalLibrary};
const Include Include::Xi::Byte{"Xi/Byte.hh", Kind::InternalLibrary};
const Include Include::Xi::Result{"Xi/Result.hpp", Kind::InternalLibrary};
const Include Include::Xi::ErrorCode{"Xi/ErrorCode.hpp", Kind::InternalLibrary};

const Include Include::Xi::Serialization::Serializer{"Xi/Serialization/Serializer.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::Complex{"Xi/Serialization/Serialization.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::ArraySerialization{"Xi/Serialization/Array.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::VectorSerialization{"Xi/Serialization/Vector.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::OptionalSerialization{"Xi/Serialization/Optional.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::VariantSerialization{"Xi/Serialization/Variant.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::EnumSerialization{"Xi/Serialization/Enum.hpp", Kind::InternalLibrary};
const Include Include::Xi::Serialization::FlagSerialization{"Xi/Serialization/Flag.hpp", Kind::InternalLibrary};

const Include Include::Xi::TypeSafe::Flag{"Xi/TypeSafe/Flag.hpp", Kind::InternalLibrary};

const Include Include::Xi::Rpc::IServiceProvider{"Xi/Rpc/IServiceProvider.hpp", Kind::InternalLibrary};

Include::Include(const std::string &path_, const Include::Kind kind_) : m_path{path_}, m_kind{kind_} {
  /* */
}
const std::string &Include::path() const {
  return m_path;
}

Include::Kind Include::kind() const {
  return m_kind;
}

bool Include::operator==(const Include &rhs) const {
  return kind() == rhs.kind() && path() == rhs.path();
}

bool Include::operator!=(const Include &rhs) const {
  return kind() != rhs.kind() || path() != rhs.path();
}

bool Include::operator<(const Include &rhs) const {
  if (kind() < rhs.kind()) {
    return true;
  } else if (rhs.kind() < kind()) {
    return false;
  } else {
    return path() < rhs.path();
  }
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

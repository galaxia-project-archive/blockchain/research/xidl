/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/IncludeCollection.hpp"

#include <algorithm>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

void IncludeCollection::add(const Include& include) {
  auto& container = m_includes[include.kind()];
  auto search = std::find(begin(container), end(container), include);
  if (search == end(container)) {
    container.emplace_back(include);
  }
}

const IncludeCollection::container_type& IncludeCollection::includes(Include::Kind kind) const {
  return m_includes[kind];
}

IncludeCollection::container_type IncludeCollection::includes() const {
  container_type reval{};
  for (const auto& collection : m_includes) {
    std::copy(begin(collection.second), end(collection.second), std::back_inserter(reval));
  }
  return reval;
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/MethodSignature.hpp"

#include <Xi/Idl/Generator/Alias.hpp>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

MethodArgument::MethodArgument(const std::string &name_, const TypeSignature &type_) : m_name{name_}, m_type{type_} {
  /* */
}

const std::string &MethodArgument::name() const {
  return m_name;
}

const TypeSignature &MethodArgument::type() const {
  return m_type;
}

SharedConstNamespace MethodSignature::namespace_() const {
  return m_namespace;
}

const std::string &MethodSignature::name() const {
  return m_name;
}

const std::optional<TypeSignature> &MethodSignature::containingType() const {
  return m_containingType;
}

const MethodArgumentVector &MethodSignature::arguments() const {
  return m_arguments;
}

bool MethodSignature::isConst() const {
  return m_isConst;
}

MethodSignatureBuilder &MethodSignatureBuilder::withNamespace(SharedConstNamespace ns) {
  m_signature.m_namespace = ns;
  return *this;
}

MethodSignatureBuilder &MethodSignatureBuilder::withName(const std::string &name) {
  m_signature.m_name = name;
  return *this;
}

MethodSignatureBuilder &MethodSignatureBuilder::withContainingType(const std::optional<TypeSignature> &type) {
  m_signature.m_containingType = type;
  return *this;
}

MethodSignatureBuilder &MethodSignatureBuilder::withArgument(const MethodArgument &arg) {
  m_signature.m_arguments.push_back(arg);
  return *this;
}

MethodSignatureBuilder &MethodSignatureBuilder::withIsConst(bool isConst) {
  m_signature.m_isConst = isConst;
  return *this;
}

MethodSignature MethodSignatureBuilder::build() {
  return m_signature;
}

MethodSignature makeStructSerializationMethodSignature(const Type &type) {
  MethodArgument arg{"serializer", makeTypeSignature(TypeBuilder{}
                                                         .withName("ISerializer")
                                                         .withNamespace(Namespace::fromString("CryptoNote"))
                                                         .build()
                                                         .takeOrThrow())
                                       .decorate(TypeSignature::Decorator::Reference)};

  return MethodSignatureBuilder{}
      .withNamespace(type.namespace_())
      .withName("serialize")
      .withIsConst(false)
      .withContainingType(makeTypeSignature(type))
      .withArgument(arg)
      .build();
}

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

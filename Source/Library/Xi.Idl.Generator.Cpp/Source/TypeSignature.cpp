/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Cpp/TypeSignature.hpp"

#include <Xi/Idl/Generator/Primitive.hpp>
#include <Xi/Idl/Generator/Typedef.hpp>
#include <Xi/Idl/Generator/Alias.hpp>
#include <Xi/Idl/Generator/Vector.hpp>
#include <Xi/Idl/Generator/Array.hpp>
#include <Xi/Idl/Generator/Variant.hpp>

namespace Xi {
namespace Idl {
namespace Generator {
namespace Cpp {

namespace {
SharedConstNamespace StdNamespace{Namespace::fromString("std")};
SharedConstNamespace XiNamespace{Namespace::fromString("Xi")};
SharedConstNamespace NoNamespace{nullptr};
}  // namespace

SharedConstNamespace TypeSignature::namespace_() const {
  return m_namespace;
}

const std::string &TypeSignature::name() const {
  return m_name;
}

TypeSignature::Decorator TypeSignature::decorator() const {
  return m_decorator;
}

const TypeSignature::GenericContainer &TypeSignature::genericArguments() const {
  return m_genericArguments;
}

TypeSignature TypeSignature::decorate(TypeSignature::Decorator dec) const {
  TypeSignature reval = *this;
  reval.m_decorator = dec;
  return reval;
}

TypeSignature TypeSignature::toOptional() const {
  TypeSignature reval{StdNamespace, "optional"};
  reval.m_genericArguments.emplace_back(*this);
  return reval;
}

TypeSignature TypeSignature::toArray(size_t size) const {
  TypeSignature reval{StdNamespace, "array"};
  reval.m_genericArguments.emplace_back(*this);
  reval.m_genericArguments.emplace_back(Constant{size});
  return reval;
}

TypeSignature TypeSignature::toVector() const {
  TypeSignature reval{StdNamespace, "vector"};
  reval.m_genericArguments.emplace_back(*this);
  return reval;
}

TypeSignature::TypeSignature(SharedConstNamespace ns, const std::string &name_)
    : m_namespace{ns}, m_name{name_}, m_decorator{Decorator::None} {
  /* */
}

TypeSignature TypeSignature::toOptional(bool isOptional) {
  return isOptional ? toOptional() : *this;
}

TypeSignature makeTypeSignature(const Type &type) {
  if (type.isPrimitive()) {
    const auto &primitive = type.asPrimitive();
    switch (primitive.kind()) {
      case Primitive::Kind::Byte:
        return TypeSignature{XiNamespace, "Byte"}.toOptional(type.isOptional());
      case Primitive::Kind::Int8:
        return TypeSignature{StdNamespace, "int8_t"}.toOptional(type.isOptional());
      case Primitive::Kind::Int16:
        return TypeSignature{StdNamespace, "int16_t"}.toOptional(type.isOptional());
      case Primitive::Kind::Int32:
        return TypeSignature{StdNamespace, "int32_t"}.toOptional(type.isOptional());
      case Primitive::Kind::Int64:
        return TypeSignature{StdNamespace, "int64_t"}.toOptional(type.isOptional());
      case Primitive::Kind::UInt8:
        return TypeSignature{StdNamespace, "uint8_t"}.toOptional(type.isOptional());
      case Primitive::Kind::UInt16:
        return TypeSignature{StdNamespace, "uint16_t"}.toOptional(type.isOptional());
      case Primitive::Kind::UInt32:
        return TypeSignature{StdNamespace, "uint32_t"}.toOptional(type.isOptional());
      case Primitive::Kind::UInt64:
        return TypeSignature{StdNamespace, "uint64_t"}.toOptional(type.isOptional());
      case Primitive::Kind::String:
        return TypeSignature{StdNamespace, "string"}.toOptional(type.isOptional());
      case Primitive::Kind::Float16:
        return TypeSignature{NoNamespace, "float"}.toOptional(type.isOptional());
      case Primitive::Kind::Float32:
        return TypeSignature{NoNamespace, "double"}.toOptional(type.isOptional());
      case Primitive::Kind::Float64:
        return TypeSignature{NoNamespace, "long double"}.toOptional(type.isOptional());
      case Primitive::Kind::Boolean:
        return TypeSignature{NoNamespace, "bool"}.toOptional(type.isOptional());
    }
    XI_EXCEPTIONAL(InvalidEnumValueError)
  } else if (type.isArray()) {
    const auto &array = type.asArray();
    return makeTypeSignature(array.type()).toArray(array.size()).toOptional(type.isOptional());
  } else if (type.isVector()) {
    return makeTypeSignature(type.asVector().type()).toVector().toOptional(type.isOptional());
  } else {
    return TypeSignature{type.namespace_(), type.name()}.toOptional(type.isOptional());
  }
}  // namespace Cpp

}  // namespace Cpp
}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

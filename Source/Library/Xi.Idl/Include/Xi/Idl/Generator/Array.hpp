/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Type.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

using ArraySize = uint16_t;

class Array {
 public:
  XI_DEFAULT_COPY(Array);
  XI_DEFAULT_MOVE(Array);

  const Type& type() const;
  ArraySize size() const;

 private:
  Array() = default;

  void setType(const Type& type_);
  void setSize(ArraySize size_);

  friend class ArrayBuilder;

 private:
  Type m_type;
  uint16_t m_size;
};

class ArrayBuilder {
 public:
  ArrayBuilder();
  XI_DELETE_COPY(ArrayBuilder);
  XI_DELETE_MOVE(ArrayBuilder);

  ArrayBuilder& withType(const Type& type);
  ArrayBuilder& withSize(ArraySize size);

  Result<Array> build();

 private:
  Result<Array> m_array;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

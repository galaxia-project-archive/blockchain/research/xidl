/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Type.hpp"
#include "Xi/Idl/Generator/SerializationTag.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Field {
 public:
  XI_DEFAULT_COPY(Field);
  XI_DEFAULT_MOVE(Field);

  const std::string& name() const;
  const SerializationTag& tag() const;
  const Type& type() const;
  const Documentation& documentation() const;

 private:
  Field() = default;

  void setName(const std::string& name);
  void setTag(const SerializationTag& tag);
  void setType(const Type& t);
  void setDocumentation(const Documentation& doc_);

  friend class FieldBuilder;

 private:
  std::string m_name;
  SerializationTag m_tag;
  Type m_type;
  Documentation m_doc;
};

class FieldBuilder {
 public:
  FieldBuilder();
  XI_DELETE_COPY(FieldBuilder);
  XI_DELETE_MOVE(FieldBuilder);

  FieldBuilder& withName(const std::string& name);
  FieldBuilder& withTag(const SerializationTag& tag);
  FieldBuilder& withType(const Type& type);
  FieldBuilder& withDocumentation(const Documentation& doc);

  Result<Field> build();

 private:
  Result<Field> m_field;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

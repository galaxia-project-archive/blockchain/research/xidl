/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <stack>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Scope.hpp"
#include "Xi/Idl/Generator/Import.hpp"
#include "Xi/Idl/Generator/Type.hpp"
#include "Xi/Idl/Generator/Field.hpp"
#include "Xi/Idl/Generator/Package.hpp"
#include "Xi/Idl/Generator/Variant.hpp"
#include "Xi/Idl/Generator/Alias.hpp"
#include "Xi/Idl/Generator/Typedef.hpp"
#include "Xi/Idl/Generator/Enum.hpp"
#include "Xi/Idl/Generator/Flag.hpp"
#include "Xi/Idl/Generator/Contract.hpp"
#include "Xi/Idl/Generator/Service.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Generator {
 public:
  virtual ~Generator() = default;

  Result<void> begin(Scope& scope);
  Result<void> end(Scope& scope);

  Result<void> openNamespace(SharedConstNamespace ns);
  Result<void> closeNamespace(SharedConstNamespace ns);

  Result<void> onImport(const Import& import);
  Result<void> onPackage(const Package& package);
  Result<void> onVariant(const Variant& variant);
  Result<void> onAlias(const Alias& alias);
  Result<void> onTypedef(const Typedef& typedef_);
  Result<void> onEnum(const Enum& enum_);
  Result<void> onFlag(const Flag& flag);
  Result<void> onContract(const Contract& contract);
  Result<void> onService(const Service& service);

  Scope& currentScope();
  const Scope& currentScope() const;

 protected:
  virtual Result<void> doBegin(Scope& scope);
  virtual Result<void> doEnd(Scope& scope);

  virtual Result<void> doOpenNamespace(SharedConstNamespace ns);
  virtual Result<void> doCloseNamespace(SharedConstNamespace ns);

  virtual Result<void> doOnImport(const Import& import);
  virtual Result<void> doOnPackage(const Package& package);
  virtual Result<void> doOnVariant(const Variant& variant);
  virtual Result<void> doOnAlias(const Alias& alias);
  virtual Result<void> doOnTypedef(const Typedef& typedef_);
  virtual Result<void> doOnEnum(const Enum& enum_);
  virtual Result<void> doOnFlag(const Flag& flag);
  virtual Result<void> doOnContract(const Contract& contract);
  virtual Result<void> doOnService(const Service& service);

 private:
  Scope* m_currentScope = nullptr;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Scope.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Import {
 public:
  XI_DEFAULT_COPY(Import);
  XI_DEFAULT_MOVE(Import);

  const std::string& filepath() const;
  const std::string& alias() const;
  const Scope& scope() const;

 private:
  Import() = default;

  void setFilepath(const std::string& filepath_);
  void setAlias(const std::string& alias_);
  void setScope(Scope scope_);

  friend class ImportBuilder;

 private:
  std::string m_filepath;
  std::string m_alias;
  Scope m_scope;
};

class ImportBuilder {
 public:
  ImportBuilder();
  XI_DELETE_COPY(ImportBuilder);
  XI_DEFAULT_MOVE(ImportBuilder);

  ImportBuilder& withFilepath(const std::string& filepath);
  ImportBuilder& withAlias(const std::string& alias);
  ImportBuilder& withScope(Scope scope);

  Result<Import> build();

 private:
  Result<Import> m_import;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

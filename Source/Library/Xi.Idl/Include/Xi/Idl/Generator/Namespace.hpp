/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

XI_DECLARE_SMART_POINTER_CLASS(Namespace)

class Namespace : public std::enable_shared_from_this<Namespace> {
 public:
  static SharedConstNamespace Global;
  static SharedNamespace fromString(const std::string& str);

 public:
  const std::string& name() const;
  void setName(std::string name);
  std::string stringify() const;

  WeakNamespace parent();
  WeakConstNamespace parent() const;
  SharedConstNamespace root() const;
  std::vector<SharedConstNamespace> path() const;

  bool hasParent() const;
  bool isRoot() const;

  std::vector<SharedNamespace> children();
  std::vector<SharedConstNamespace> children() const;
  SharedNamespace searchChild(const std::string& name);
  SharedNamespace searchChild(ConstSpan<std::string> path);
  SharedConstNamespace searchChild(const std::string& name) const;
  SharedConstNamespace searchChild(ConstSpan<std::string> path) const;

  void addChild(SharedNamespace ns);

  /// Returns the shortest path from a common node to ns
  std::vector<SharedConstNamespace> shortestPathTo(SharedConstNamespace ns) const;

 private:
  Namespace() = default;

  void setParent(WeakNamespace parent);

 private:
  std::string m_name;
  WeakNamespace m_parent;
  std::vector<SharedNamespace> m_children;
};

bool equalTo(SharedConstNamespace lhs, SharedConstNamespace rhs);
bool notEqualTo(SharedConstNamespace lhs, SharedConstNamespace rhs);
bool lessThan(SharedConstNamespace lhs, SharedConstNamespace rhs);

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

namespace std {
template <>
struct less<Xi::Idl::Generator::SharedConstNamespace> {
  inline bool operator()(const Xi::Idl::Generator::SharedConstNamespace& lhs,
                         const Xi::Idl::Generator::SharedConstNamespace& rhs) const {
    return lessThan(lhs, rhs);
  }
};
}  // namespace std

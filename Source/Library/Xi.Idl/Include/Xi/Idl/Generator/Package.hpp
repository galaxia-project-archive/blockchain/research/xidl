/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>
#include <functional>

#include <Xi/Global.hh>

#include "Xi/Idl/Generator/Field.hpp"
#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Package {
 public:
  XI_DEFAULT_COPY(Package);
  XI_DEFAULT_MOVE(Package);

  const std::string& name() const;
  SharedConstNamespace namespace_() const;
  const std::vector<Field>& fields() const;
  const std::optional<Type>& inheritance() const;
  const Documentation& documentation() const;

 private:
  Package() = default;

  void setName(const std::string& id);
  void setNamespace(SharedConstNamespace ns);
  std::vector<Field>& fields();
  void setInheritance(const std::optional<Type>& inheritance_);
  void setDocumentation(const Documentation& doc_);

  friend class PackageBuilder;
  friend class Contract;

 private:
  std::string m_name;
  SharedConstNamespace m_namespace;
  std::vector<Field> m_fields;
  std::optional<Type> m_inheritance;
  Documentation m_doc;
};

class PackageBuilder {
 public:
  PackageBuilder();
  XI_DELETE_COPY(PackageBuilder);
  XI_DELETE_MOVE(PackageBuilder);

  PackageBuilder& withName(const std::string& name);
  PackageBuilder& withNamespace(SharedConstNamespace ns);
  PackageBuilder& withField(const Field& field);
  PackageBuilder& withInheritance(const std::optional<Type>& type);
  PackageBuilder& withDocumentation(const Documentation& doc);

  Result<Package> build();

 private:
  Result<Package> m_package;
};

Result<Type> makeType(const Package& package);

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

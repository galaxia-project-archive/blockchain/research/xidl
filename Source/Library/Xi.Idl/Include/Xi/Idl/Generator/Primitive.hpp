/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

class Primitive {
 public:
  enum struct Kind {
    Byte,
    Int8,
    Int16,
    Int32,
    Int64,
    UInt8,
    UInt16,
    UInt32,
    UInt64,
    Float16,
    Float32,
    Float64,
    Boolean,
    String,
  };

 public:
  XI_DEFAULT_COPY(Primitive);
  XI_DEFAULT_MOVE(Primitive);

  Kind kind() const;

 private:
  Primitive() = default;

  void setKind(Kind kind_);

  friend class PrimitiveBuilder;

 private:
  Kind m_kind;
};

class PrimitiveBuilder {
 public:
  PrimitiveBuilder();
  XI_DELETE_COPY(PrimitiveBuilder);
  XI_DELETE_MOVE(PrimitiveBuilder);

  PrimitiveBuilder& withKind(Primitive::Kind kind);

  Result<Primitive> build();

 private:
  Result<Primitive> m_primitive;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

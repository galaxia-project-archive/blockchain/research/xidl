/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

XI_ERROR_CODE_BEGIN(SerializationTag)
XI_ERROR_CODE_VALUE(NullBinaryTag, 0x0001)
XI_ERROR_CODE_VALUE(NullTextTag, 0x0002)
XI_ERROR_CODE_END(SerializationTag, "Idl::SerializationTagError")

class SerializationTag {
 public:
  using binary_type = std::uint32_t;
  using text_type = std::string;

 public:
  XI_DEFAULT_COPY(SerializationTag);
  XI_DEFAULT_MOVE(SerializationTag);
  ~SerializationTag() = default;

 public:
  const text_type& text() const;
  binary_type binary() const;

 private:
  text_type m_text;
  binary_type m_binary;

 private:
  SerializationTag() = default;
  friend class SerializationTagBuilder;

  friend class Field;
  friend class FlagEntry;
  friend class EnumEntry;
  friend class ServiceCommand;
};

class SerializationTagBuilder {
 public:
  SerializationTagBuilder();
  XI_DELETE_COPY(SerializationTagBuilder);
  XI_DELETE_MOVE(SerializationTagBuilder);
  ~SerializationTagBuilder() = default;

  SerializationTagBuilder& withText(const SerializationTag::text_type& text);
  SerializationTagBuilder& withBinary(const SerializationTag::binary_type binary);

  Result<SerializationTag> build();

 private:
  Result<SerializationTag> m_tag;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Idl::Generator, SerializationTag)

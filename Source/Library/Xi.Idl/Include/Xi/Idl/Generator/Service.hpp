/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Documentation.hpp"
#include "Xi/Idl/Generator/ServiceCommand.hpp"
#include "Xi/Idl/Generator/ServiceCommandCollection.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Service {
 public:
  XI_DEFAULT_COPY(Service);
  XI_DEFAULT_MOVE(Service);

  const std::string& name() const;
  const Documentation& documentation() const;
  SharedConstNamespace namespace_() const;
  const ServiceCommandCollection& contracts() const;

 private:
  Service() = default;

  void setName(const std::string& name_);
  void setDocumentation(const Documentation& doc);
  void setNamespace(SharedConstNamespace ns);
  ServiceCommandCollection& contracts();

  friend class ServiceBuilder;

 private:
  std::string m_name;
  Documentation m_doc;
  SharedConstNamespace m_namespace;
  ServiceCommandCollection m_commands;
};

class ServiceBuilder {
 public:
  ServiceBuilder();
  XI_DELETE_COPY(ServiceBuilder);
  XI_DELETE_MOVE(ServiceBuilder);

  ServiceBuilder& withName(const std::string name);
  ServiceBuilder& withDocumentation(const Documentation& doc);
  ServiceBuilder& withNamespace(SharedConstNamespace ns);
  ServiceBuilder& withCommand(const ServiceCommand& command);

  Result<Service> build();

 private:
  Result<Service> m_service;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

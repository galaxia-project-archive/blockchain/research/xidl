/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Documentation.hpp"
#include "Xi/Idl/Generator/SerializationTag.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class ServiceCommand {
 public:
  enum struct Kind {
    Service,
    Contract,
  };

 public:
  ServiceCommand(const ServiceCommand& other);
  ServiceCommand& operator=(const ServiceCommand& other);
  ServiceCommand(ServiceCommand&& other);
  ServiceCommand& operator=(ServiceCommand&& other);
  ~ServiceCommand();

  const SerializationTag& tag() const;
  const Documentation& documentation() const;
  Kind kind() const;

  bool isService() const;
  bool isContract() const;

  const class Service& asService() const;
  const class Contract& asContract() const;

 private:
  ServiceCommand();

  void setTag(const SerializationTag& tag_);
  void setDocumentation(const Documentation& doc);
  void setKind(const Kind kind_);
  void setData(const class Service& service);
  void setData(const class Contract& contract);

  friend class ServiceCommandBuilder;

 private:
  SerializationTag m_tag;
  Documentation m_doc;
  Kind m_kind;
  struct _StorageConcept;
  template <typename _DataT>
  struct _Storage;
  std::unique_ptr<_StorageConcept> m_data;
};

class ServiceCommandBuilder {
 public:
  ServiceCommandBuilder();
  XI_DELETE_COPY(ServiceCommandBuilder);
  XI_DELETE_MOVE(ServiceCommandBuilder);

  ServiceCommandBuilder& withTag(const SerializationTag& tag);
  ServiceCommandBuilder& withDocumentation(const Documentation& doc_);
  ServiceCommandBuilder& withKind(const ServiceCommand::Kind kind_);
  ServiceCommandBuilder& withData(const class Service& service);
  ServiceCommandBuilder& withData(const class Contract& contract);

  Result<ServiceCommand> build();

 private:
  Result<ServiceCommand> m_command;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

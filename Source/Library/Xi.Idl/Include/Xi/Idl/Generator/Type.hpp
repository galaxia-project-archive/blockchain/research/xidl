/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>
#include <map>
#include <variant>
#include <memory>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Parser/Type.hpp"
#include "Xi/Idl/Generator/Namespace.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class Type : public std::enable_shared_from_this<Type> {
 public:
  enum struct Kind {
    Primitive,
    Array,
    Vector,
    Typedef,
    Alias,
    Variant,
    Package,
    Enum,
    Flag,
  };

 public:
  Type(const Type& other);
  Type& operator=(const Type& other);
  Type(Type&& other);
  Type& operator=(Type&& other);
  ~Type();

  const std::string& name() const;
  bool isOptional() const;
  Kind kind() const;
  SharedConstNamespace namespace_() const;

  Type toOptional() const;
  Type toOptional(bool isOptional) const;

  bool isPrimitive() const;
  bool isArray() const;
  bool isVector() const;
  bool isTypedef() const;
  bool isAlias() const;
  bool isVariant() const;
  bool isPackage() const;
  bool isEnum() const;
  bool isFlag() const;

  const class Primitive& asPrimitive() const;
  const class Array& asArray() const;
  const class Vector& asVector() const;
  const class Typedef& asTypedef() const;
  const class Alias& asAlias() const;
  const class Variant& asVariant() const;
  const class Package& asPackage() const;
  const class Enum& asEnum() const;
  const class Flag& asFlag() const;

 private:
  Type();

  void setName(const std::string& n);
  void setIsOptional(bool is);
  void setKind(Kind k);
  void setNamespace(SharedConstNamespace ns);
  void setData(const class Primitive& data);
  void setData(const class Array& data);
  void setData(const class Vector& data);
  void setData(const class Typedef& data);
  void setData(const class Alias& data);
  void setData(const class Package& data);
  void setData(const class Variant& data);
  void setData(const class Enum& data);
  void setData(const class Flag& data);

  friend class TypeBuilder;
  friend class Field;
  friend class Alias;
  friend class Typedef;
  friend class Contract;
  friend class Array;
  friend class Vector;
  friend class Package;

 private:
  std::string m_name;
  bool m_isOptional;
  Kind m_kind;
  SharedConstNamespace m_namespace;
  struct _StorageConcept;
  template <typename _DataT>
  struct _Storage;
  std::unique_ptr<_StorageConcept> m_data;
};

class TypeBuilder {
 public:
  TypeBuilder();
  XI_DELETE_COPY(TypeBuilder);
  XI_DELETE_MOVE(TypeBuilder);

  TypeBuilder& withName(const std::string& name);
  TypeBuilder& withIsOptional(bool isOptional);
  TypeBuilder& withKind(Type::Kind kind);
  TypeBuilder& withNamespace(SharedConstNamespace ns);
  TypeBuilder& withData(const class Primitive& data);
  TypeBuilder& withData(const class Array& data);
  TypeBuilder& withData(const class Vector& data);
  TypeBuilder& withData(const class Typedef& data);
  TypeBuilder& withData(const class Alias& data);
  TypeBuilder& withData(const class Package& data);
  TypeBuilder& withData(const class Variant& data);
  TypeBuilder& withData(const class Enum& data);
  TypeBuilder& withData(const class Flag& data);

  Result<Type> build();

 private:
  Result<Type> m_type;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <vector>
#include <set>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Idl/Generator/Namespace.hpp"
#include "Xi/Idl/Generator/Type.hpp"
#include "Xi/Idl/Generator/Package.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

class TypeCollection {
 private:
  using container_type = std::vector<Type>;

 public:
  using const_iterator = container_type::const_iterator;

 public:
  Result<void> add(Type type);
  std::optional<Type> search(const std::string& name) const;

  const_iterator begin() const;
  const_iterator end() const;
  const_iterator cbegin() const;
  const_iterator cend() const;

 private:
  std::vector<Type> m_types;
};

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

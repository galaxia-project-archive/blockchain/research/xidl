/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <cinttypes>
#include <type_traits>

#include <Xi/String/FromString.hpp>

#include "Xi/Idl/Parser/Automaton.hpp"
#include "Xi/Idl/Parser/ParserError.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

class StringConstantAutomaton final : public ResultAutomaton<std::string> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<StringConstantAutomaton> makeStringConstantAutomaton(std::string&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<StringConstantAutomaton> makeStringConstantAutomaton(std::string& result);

template <typename _IntegralT>
class NumberConstantAutomaton;

template <typename _IntegralT>
std::shared_ptr<NumberConstantAutomaton<_IntegralT>> makeNumberConstantAutomaton(_IntegralT&, _IntegralT, _IntegralT);

template <typename _IntegralT>
class NumberConstantAutomaton final : public ResultAutomaton<_IntegralT> {
 private:
  using ResultAutomaton<_IntegralT>::ResultAutomaton;
  friend std::shared_ptr<NumberConstantAutomaton<_IntegralT>> makeNumberConstantAutomaton<_IntegralT>(_IntegralT&,
                                                                                                      _IntegralT,
                                                                                                      _IntegralT);

 private:
  _IntegralT m_min;
  _IntegralT m_max;

 protected:
  inline Result<void> doProcessToken(const Token& token) override {
    auto cast = fromString<_IntegralT>(token.value);
    XI_FAIL_IF_NOT(cast.isError(), ParserError::IllFormedNumber)
    XI_FAIL_IF(this->m_min > *cast, ParserError::NumberOutOfRange)
    XI_FAIL_IF(this->m_max < *cast, ParserError::NumberOutOfRange)
    this->m_result = *cast;
    this->markFinished();
    XI_SUCCEED()
  }
};

template <typename _IntegralT>
inline std::shared_ptr<NumberConstantAutomaton<_IntegralT>> makeNumberConstantAutomaton(_IntegralT& res, _IntegralT min,
                                                                                        _IntegralT max) {
  std::shared_ptr<NumberConstantAutomaton<_IntegralT>> reval{new NumberConstantAutomaton<_IntegralT>{res}};
  reval->m_min = min;
  reval->m_max = max;
  return reval;
}

template <typename _IntegralT>
inline std::shared_ptr<NumberConstantAutomaton<_IntegralT>> makeNumberConstantAutomaton(_IntegralT& res,
                                                                                        _IntegralT min) {
  return makeNumberConstantAutomaton(res, min, std::numeric_limits<_IntegralT>::max());
}

template <typename _IntegralT>
inline std::shared_ptr<NumberConstantAutomaton<_IntegralT>> makeNumberConstantAutomaton(_IntegralT& res) {
  return makeNumberConstantAutomaton(res, std::numeric_limits<_IntegralT>::min());
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

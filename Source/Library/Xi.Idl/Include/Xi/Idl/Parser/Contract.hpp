/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>
#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Type.hpp"
#include "Xi/Idl/Parser/Tag.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct Contract {
  Documentation doc;
  Identifier name;
  std::optional<Type> argument;
  std::optional<Type> returnType;
};
using ContractVector = std::vector<Contract>;

class ContractAutomaton final : public ResultAutomaton<Contract> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ContractAutomaton> makeContractAutomaton(Contract&);
};

std::shared_ptr<ContractAutomaton> makeContractAutomaton(Contract& result);

class ContractsAutomaton final : public ResultAutomaton<ContractVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ContractsAutomaton> makeContractsAutomaton(ContractVector&);

 protected:
  virtual Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<ContractsAutomaton> makeContractsAutomaton(ContractVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

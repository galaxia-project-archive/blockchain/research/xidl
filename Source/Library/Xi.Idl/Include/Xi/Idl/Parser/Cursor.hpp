/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <string>

#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/Stream/InputStream.hpp>

#include "Xi/Idl/Parser/Token.hpp"
#include "Xi/Idl/Parser/SourcePosition.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

XI_ERROR_CODE_BEGIN(Cursor)
XI_ERROR_CODE_END(Cursor, "Idl::Parser::CursorError")

class Cursor {
 public:
  explicit Cursor(Stream::InputStream& stream);

  bool isVoid(const char ch) const;
  bool isSymbol(const char ch) const;
  bool isMultilineSymbol(const char ch) const;

  /*!
   * \brief readToken Extracts the next token from the stream.
   * \return The token extracted or an endOfStream token if the stream is exhausted.
   */
  Result<Token> readToken();

  const SourcePosition& currentPosition() const;
  SourcePosition& currentPosition();

 private:
  void newLine();
  void resetLine();
  void advanceColumn(size_t offset = 1);

 private:
  Stream::InputStream& m_stream;
  SourcePosition m_pos{1, 1};
};

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Idl::Parser, Cursor)

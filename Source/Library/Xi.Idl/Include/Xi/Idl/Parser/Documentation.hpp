/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <variant>
#include <string>
#include <vector>

#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct NoDocumentationTag {};
struct BriefDocumentationTag {};
struct DetailedDocumentationTag {};
struct ExampleDocumentationTag {};

// clang-format off
using DocumentationTag = std::variant<

NoDocumentationTag,
BriefDocumentationTag,
DetailedDocumentationTag,
ExampleDocumentationTag

>;
// clang-format on

struct DocumentationEntry {
  DocumentationTag tag;
  std::string content;
};
using DocumentationEntryVector = std::vector<DocumentationEntry>;
using Documentation = DocumentationEntryVector;

class DocumentationTagAutomaton : public ResultAutomaton<DocumentationTag> {
 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<DocumentationTagAutomaton> makeDocumentationTagAutomaton(DocumentationTag&);
};

std::shared_ptr<DocumentationTagAutomaton> makeDocumentationTagAutomaton(DocumentationTag& result);

class DocumentationEntryAutomaton : public ResultAutomaton<DocumentationEntry> {
 public:
  void enableMultineEnvironment();

 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<DocumentationEntryAutomaton> makeDocumentationEntryAutomaton(DocumentationEntry&);

 private:
  bool m_isMultilineEnvironment = false;
};

std::shared_ptr<DocumentationEntryAutomaton> makeDocumentationEntryAutomaton(DocumentationEntry& result);

class DocumentationAutomaton : public ResultAutomaton<Documentation> {
 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<DocumentationAutomaton> makeDocumentationAutomaton(Documentation&);

 private:
  bool m_isMultilineEnvironment = false;
};

std::shared_ptr<DocumentationAutomaton> makeDocumentationAutomaton(Documentation& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

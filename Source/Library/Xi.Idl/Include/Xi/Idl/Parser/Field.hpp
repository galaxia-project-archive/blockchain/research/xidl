/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Type.hpp"
#include "Xi/Idl/Parser/Tag.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

using FieldIndex = uint16_t;

struct Field {
  Documentation doc;
  Identifier name;
  Tag tag;
  Type type;
};
using FieldVector = std::vector<Field>;

class FieldAutomaton final : public ResultAutomaton<Field> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FieldAutomaton> makeFieldAutomaton(Field&);
};

std::shared_ptr<FieldAutomaton> makeFieldAutomaton(Field& result);

class FieldsAutomaton final : public ResultAutomaton<FieldVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FieldsAutomaton> makeFieldsAutomaton(FieldVector&);

 protected:
  virtual Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<FieldsAutomaton> makeFieldsAutomaton(FieldVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

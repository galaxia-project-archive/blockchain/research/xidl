/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Tag.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct FlagEntry {
  Documentation doc;
  Identifier name;
  Tag tag;
};
using FlagEntryVector = std::vector<FlagEntry>;

struct Flag {
  Identifier name;
  FlagEntryVector values;
};

class FlagEntryAutomaton final : public ResultAutomaton<FlagEntry> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FlagEntryAutomaton> makeFlagEntryAutomaton(FlagEntry&);
};

std::shared_ptr<FlagEntryAutomaton> makeFlagEntryAutomaton(FlagEntry& result);

class FlagEntriesAutomaton final : public ResultAutomaton<FlagEntryVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FlagEntriesAutomaton> makeFlagEntriesAutomaton(FlagEntryVector&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<FlagEntriesAutomaton> makeFlagEntriesAutomaton(FlagEntryVector& result);

class FlagAutomaton final : public ResultAutomaton<Flag> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<FlagAutomaton> makeFlagAutomaton(Flag&);
};

std::shared_ptr<FlagAutomaton> makeFlagAutomaton(Flag& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

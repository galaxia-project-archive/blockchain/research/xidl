/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <vector>

#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

using Identifier = std::string;
using IdentifierVector = std::vector<Identifier>;

[[nodiscard]] bool isValidIdentifier(const Identifier::value_type ch);
[[nodiscard]] bool isValidIdentifier(const Identifier& id);

class IdentifierAutomaton final : public ResultAutomaton<Identifier> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<IdentifierAutomaton> makeIdentifierAutomaton(Identifier&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<IdentifierAutomaton> makeIdentifierAutomaton(Identifier& result);

class IdentifiersAutomaton final : public ResultAutomaton<IdentifierVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<IdentifiersAutomaton> makeIdentifiersAutomaton(IdentifierVector&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<IdentifiersAutomaton> makeIdentifiersAutomaton(IdentifierVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

/*!
 * \attention Filepaths may not have spaces.
 */
struct Import {
  /// string_constant considered as filepath of another xidl file to include.
  std::string filepath;
  /// Shorthand namespace alias to reference file imported declarations easier.
  Identifier alias;
};

using ImportVector = std::vector<Import>;

class ImportAutomaton final : public ResultAutomaton<Import> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ImportAutomaton> makeImportAutomaton(Import&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<ImportAutomaton> makeImportAutomaton(Import& result);

class ImportsAutomaton final : public ResultAutomaton<ImportVector> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<ImportsAutomaton> makeImportsAutomaton(ImportVector&);

 protected:
  Result<void> doProcessToken(const Token& token) override;
};

std::shared_ptr<ImportsAutomaton> makeImportsAutomaton(ImportVector& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

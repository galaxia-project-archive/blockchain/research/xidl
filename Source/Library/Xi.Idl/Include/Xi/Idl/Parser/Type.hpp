/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <variant>
#include <optional>

#include "Xi/Idl/Parser/Atomic.hpp"
#include "Xi/Idl/Parser/Array.hpp"
#include "Xi/Idl/Parser/Vector.hpp"
#include "Xi/Idl/Parser/Automaton.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

using ContainerType = std::variant<Array, Vector>;
using BasicType = std::variant<Atomic, ContainerType>;

struct Type {
  BasicType base;
  bool isOptional;
};

class BasicTypeAutomaton final : public ResultAutomaton<BasicType> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<BasicTypeAutomaton> makeBasicTypeAutomaton(BasicType&);

 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  Atomic m_cached;
};

std::shared_ptr<BasicTypeAutomaton> makeBasicTypeAutomaton(BasicType& result);

class TypeAutomaton final : public ResultAutomaton<Type> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<TypeAutomaton> makeTypeAutomaton(Type&);
};

std::shared_ptr<TypeAutomaton> makeTypeAutomaton(Type& result);

class OptionalTypeAutomaton final : public ResultAutomaton<std::optional<Type>> {
 protected:
  Result<void> doProcessToken(const Token& token) override;

 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<OptionalTypeAutomaton> makeOptionalTypeAutomaton(std::optional<Type>&);
};

std::shared_ptr<OptionalTypeAutomaton> makeOptionalTypeAutomaton(std::optional<Type>& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

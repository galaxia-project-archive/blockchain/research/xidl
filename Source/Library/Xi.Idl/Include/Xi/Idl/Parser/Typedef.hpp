/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Type.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

struct Typedef {
  Identifier name;
  Type type;
};

class TypedefAutomaton final : public ResultAutomaton<Typedef> {
 private:
  using ResultAutomaton::ResultAutomaton;
  friend std::shared_ptr<TypedefAutomaton> makeTypedefAutomaton(Typedef&);
};

std::shared_ptr<TypedefAutomaton> makeTypedefAutomaton(Typedef& result);

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Token.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

namespace {
static const char eosCharacter = '\0';
}

bool Token::isSpace(const char ch) {
  return ch == ' ' || ch == '\t';
}

bool Token::isNewLine(const char ch) {
  return ch == '\n' || ch == '\r';
}

bool Token::isVoid(const char ch) {
  return isSpace(ch) || isNewLine(ch);
}

bool Token::isEmpty() const {
  return value.empty();
}

bool Token::isEndOfStream() const {
  return value.size() == 1 && value[0] == eosCharacter;
}

bool Token::isSpace() const {
  if (isEmpty()) {
    return false;
  }
  for (const auto ch : value) {
    if (!isSpace(ch)) {
      return false;
    }
  }
  return true;
}

bool Token::isNewLine() const {
  if (isEmpty()) {
    return false;
  }
  for (const auto ch : value) {
    if (!isNewLine(ch)) {
      return false;
    }
  }
  return true;
}

bool Token::isVoid() const {
  if (isEmpty()) {
    return false;
  }
  for (const auto ch : value) {
    if (!isVoid(ch)) {
      return false;
    }
  }
  return true;
}

std::string Token::stringify() const {
  return value;
}

Token makeEndOfStreamToken(const SourcePosition &currentPosition) {
  return Token{std::string{eosCharacter}, currentPosition, currentPosition};
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

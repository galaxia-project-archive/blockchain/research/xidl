/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Contract.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Contract::name() const {
  return m_name;
}

SharedConstNamespace Contract::namespace_() const {
  return m_namespace;
}

const Documentation &Contract::documentation() const {
  return m_doc;
}

const std::optional<Type> &Contract::argument() const {
  return m_arg;
}

const std::optional<Type> &Contract::returnType() const {
  return m_returnType;
}

void Contract::setName(const std::string &id) {
  m_name = id;
}

void Contract::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Contract::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

void Contract::setArgument(const std::optional<Type> &type) {
  m_arg = type;
}

void Contract::setReturnType(const std::optional<Type> &type) {
  m_returnType = type;
}

ContractBuilder::ContractBuilder() : m_contract{result_success, Contract{/* */}} {
  /* */
}

ContractBuilder &ContractBuilder::withName(const std::string &name) {
  if (!m_contract.isError()) {
    m_contract->setName(name);
  }
  return *this;
}

ContractBuilder &ContractBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_contract.isError()) {
    m_contract->setNamespace(ns);
  }
  return *this;
}

ContractBuilder &ContractBuilder::withDocumentation(const Documentation &doc) {
  if (!m_contract.isError()) {
    m_contract->setDocumentation(doc);
  }
  return *this;
}

ContractBuilder &ContractBuilder::withArgument(const std::optional<Type> &arg) {
  if (!m_contract.isError()) {
    m_contract->setArgument(arg);
  }
  return *this;
}

ContractBuilder &ContractBuilder::withReturnType(const std::optional<Type> &type) {
  if (!m_contract.isError()) {
    m_contract->setReturnType(type);
  }
  return *this;
}

Result<Contract> ContractBuilder::build() {
  return m_contract;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

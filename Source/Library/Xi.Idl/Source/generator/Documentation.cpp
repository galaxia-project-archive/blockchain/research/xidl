/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Documentation::brief() const {
  return m_brief;
}

const std::string &Documentation::detailed() const {
  return m_detailed;
}

const std::vector<std::string> &Documentation::examples() const {
  return m_examples;
}

DocumentationBuilder::DocumentationBuilder() : m_result{result_success, Documentation{/* */}} {
  /* */
}

DocumentationBuilder &DocumentationBuilder::withBrief(const std::string &brief) {
  if (!m_result.isError()) {
    m_result->m_brief += brief;
  }
  return *this;
}

DocumentationBuilder &DocumentationBuilder::withDetailed(const std::string &detailed) {
  if (!m_result.isError()) {
    m_result->m_detailed += detailed;
  }
  return *this;
}

DocumentationBuilder &DocumentationBuilder::withExample(const std::string &example) {
  if (!m_result.isError()) {
    m_result->m_examples.push_back(example);
  }
  return *this;
}

Result<Documentation> DocumentationBuilder::build() {
  return m_result;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Enum.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Enum::name() const {
  return m_name;
}

SharedConstNamespace Enum::namespace_() const {
  return m_namespace;
}

const Documentation &Enum::documentation() const {
  return m_doc;
}

const std::vector<EnumEntry> Enum::entries() const {
  return m_entries;
}

void Enum::setName(const std::string &name_) {
  m_name = name_;
}

void Enum::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

void Enum::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

std::vector<EnumEntry> &Enum::entries() {
  return m_entries;
}

EnumBuilder::EnumBuilder() : m_enum{result_success, Enum{}} {
  /* */
}

EnumBuilder &EnumBuilder::withName(const std::string &name) {
  if (!m_enum.isError()) {
    m_enum->setName(name);
  }
  return *this;
}

EnumBuilder &EnumBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_enum.isError()) {
    m_enum->setNamespace(ns);
  }
  return *this;
}

EnumBuilder &EnumBuilder::withDocumentation(const Documentation &doc) {
  if (!m_enum.isError()) {
    m_enum->setDocumentation(doc);
  }
  return *this;
}

EnumBuilder &EnumBuilder::withEntry(const EnumEntry &entry) {
  if (!m_enum.isError()) {
    m_enum->entries().emplace_back(entry);
  }
  return *this;
}

Result<Enum> EnumBuilder::build() {
  return m_enum;
}

Result<Type> makeType(const Enum &enum_) {
  return TypeBuilder{}
      .withKind(Type::Kind::Enum)
      .withName(enum_.name())
      .withNamespace(enum_.namespace_())
      .withIsOptional(false)
      .withData(enum_)
      .build();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

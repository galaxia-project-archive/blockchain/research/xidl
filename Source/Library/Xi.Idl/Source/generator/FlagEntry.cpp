/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/FlagEntry.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &FlagEntry::name() const {
  return m_name;
}

const SerializationTag &FlagEntry::tag() const {
  return m_tag;
}

const Documentation &FlagEntry::documentation() const {
  return m_doc;
}

void FlagEntry::setName(const std::string &name_) {
  m_name = name_;
}

void FlagEntry::setTag(const SerializationTag &tag_) {
  m_tag = tag_;
}

void FlagEntry::setDocumentation(const Documentation &doc_) {
  m_doc = doc_;
}

FlagEntryBuilder::FlagEntryBuilder() : m_entry{result_success, FlagEntry{}} {
  /* */
}

FlagEntryBuilder &FlagEntryBuilder::withName(const std::string &name) {
  if (!m_entry.isError()) {
    m_entry->setName(name);
  }
  return *this;
}

FlagEntryBuilder &FlagEntryBuilder::withTag(const SerializationTag &tag) {
  if (!m_entry.isError()) {
    m_entry->setTag(tag);
  }
  return *this;
}

FlagEntryBuilder &FlagEntryBuilder::withDocumentation(const Documentation &doc) {
  if (!m_entry.isError()) {
    m_entry->setDocumentation(doc);
  }
  return *this;
}

Result<FlagEntry> FlagEntryBuilder::build() {
  return m_entry;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

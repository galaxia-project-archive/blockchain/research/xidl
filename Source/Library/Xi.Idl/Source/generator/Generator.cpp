/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Generator.hpp"

#include <Xi/Exceptions.hpp>

#include "Xi/Idl/Generator/Package.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

Result<void> Generator::begin(Scope &scope) {
  XI_ERROR_TRY
  m_currentScope = std::addressof(scope);
  return doBegin(scope);
  XI_ERROR_CATCH
}

Result<void> Generator::end(Scope &scope) {
  XI_ERROR_TRY
  m_currentScope = nullptr;
  return doEnd(scope);
  XI_ERROR_CATCH
}

Result<void> Generator::openNamespace(SharedConstNamespace ns) {
  XI_ERROR_TRY
  currentScope().namespaces().setCurrentNamespace(ns);
  return doOpenNamespace(ns);
  XI_ERROR_CATCH
}

Result<void> Generator::closeNamespace(SharedConstNamespace ns) {
  XI_ERROR_TRY
  const auto ec = doCloseNamespace(ns);
  XI_ERROR_PROPAGATE(ec)
  currentScope().namespaces().setCurrentNamespace(nullptr);
  return success();
  XI_ERROR_CATCH
}

Result<void> Generator::onImport(const Import &import) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(currentScope().import(import.scope()));
  if (!import.alias().empty()) {
    XI_ERROR_PROPAGATE_CATCH(currentScope().namespaces().addAlias(import.alias(), import.scope().currentNamespace()));
  }
  return doOnImport(import);
  XI_ERROR_CATCH
}

Result<void> Generator::onPackage(const Package &package) {
  XI_ERROR_TRY
  auto type = makeType(package);
  XI_ERROR_PROPAGATE(type)
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(package.namespace_()).add(type.take()));
  return doOnPackage(package);
  XI_ERROR_CATCH
}

Result<void> Generator::onVariant(const Variant &variant) {
  XI_ERROR_TRY
  auto type = makeType(variant);
  XI_ERROR_PROPAGATE(type)
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(variant.namespace_()).add(type.take()));
  return doOnVariant(variant);
  XI_ERROR_CATCH
}

Result<void> Generator::onAlias(const Alias &alias) {
  XI_ERROR_TRY
  auto type = makeType(alias);
  XI_ERROR_PROPAGATE_CATCH(type);
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(alias.namespace_()).add(type.take()));
  return doOnAlias(alias);
  XI_ERROR_CATCH
}

Result<void> Generator::onTypedef(const Typedef &typedef_) {
  XI_ERROR_TRY
  auto type = makeType(typedef_);
  XI_ERROR_PROPAGATE_CATCH(type);
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(typedef_.namespace_()).add(type.take()));
  return doOnTypedef(typedef_);
  XI_ERROR_CATCH
}

Result<void> Generator::onEnum(const Enum &enum_) {
  XI_ERROR_TRY
  auto type = makeType(enum_);
  XI_ERROR_PROPAGATE(type)
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(enum_.namespace_()).add(type.take()))
  return doOnEnum(enum_);
  XI_ERROR_CATCH
}

Result<void> Generator::onFlag(const Flag &flag) {
  XI_ERROR_TRY
  auto type = makeType(flag);
  XI_ERROR_PROPAGATE(type)
  XI_ERROR_PROPAGATE_CATCH(currentScope().types(flag.namespace_()).add(type.take()))
  return doOnFlag(flag);
  XI_ERROR_CATCH
}

Result<void> Generator::onContract(const Contract &contract) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(currentScope().contracts(contract.namespace_()).add(contract))
  return doOnContract(contract);
  XI_ERROR_CATCH
}

Result<void> Generator::onService(const Service &service) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(currentScope().services(service.namespace_()).add(service));
  return doOnService(service);
  XI_ERROR_CATCH
}

Scope &Generator::currentScope() {
  XI_EXCEPTIONAL_IF(NotFoundError, m_currentScope == nullptr, "no current scope given")
  return *m_currentScope;
}

const Scope &Generator::currentScope() const {
  XI_EXCEPTIONAL_IF(NotFoundError, m_currentScope == nullptr, "no current scope given")
  return *m_currentScope;
}

Result<void> Generator::doBegin(Scope &) {
  return success();
}

Result<void> Generator::doEnd(Scope &) {
  return success();
}

Result<void> Generator::doOpenNamespace(SharedConstNamespace) {
  return success();
}

Result<void> Generator::doCloseNamespace(SharedConstNamespace) {
  return success();
}

Result<void> Generator::doOnImport(const Import &) {
  return success();
}

Result<void> Generator::doOnPackage(const Package &) {
  return success();
}

Result<void> Generator::doOnVariant(const Variant &) {
  return success();
}

Result<void> Generator::doOnAlias(const Alias &) {
  return success();
}

Result<void> Generator::doOnTypedef(const Typedef &) {
  return success();
}

Result<void> Generator::doOnEnum(const Enum &) {
  return success();
}

Result<void> Generator::doOnFlag(const Flag &) {
  return success();
}

Result<void> Generator::doOnContract(const Contract &) {
  return success();
}

Result<void> Generator::doOnService(const Service &) {
  return success();
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

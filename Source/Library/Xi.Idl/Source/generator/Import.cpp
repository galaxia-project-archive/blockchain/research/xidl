/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Import.hpp"

#include <utility>

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Import::filepath() const {
  return m_filepath;
}

const std::string &Import::alias() const {
  return m_alias;
}

const Scope &Import::scope() const {
  return m_scope;
}

void Import::setFilepath(const std::string &filepath_) {
  m_filepath = filepath_;
}

void Import::setAlias(const std::string &alias_) {
  m_alias = alias_;
}

void Import::setScope(Scope scope_) {
  m_scope = scope_;
}

ImportBuilder::ImportBuilder() : m_import{result_success, Import{/* */}} {
  /* */
}

ImportBuilder &ImportBuilder::withFilepath(const std::string &filepath) {
  if (!m_import.isError()) {
    m_import->setFilepath(filepath);
  }
  return *this;
}

ImportBuilder &ImportBuilder::withAlias(const std::string &alias) {
  if (!m_import.isError()) {
    m_import->setAlias(alias);
  }
  return *this;
}

ImportBuilder &ImportBuilder::withScope(Scope scope) {
  if (!m_import.isError()) {
    m_import->setScope(std::move(scope));
  }
  return *this;
}

Result<Import> ImportBuilder::build() {
  return m_import;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Namespace.hpp"

#include <utility>
#include <cassert>
#include <vector>
#include <algorithm>

#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

SharedConstNamespace Namespace::Global{fromString("::GLOBAL::")};

SharedNamespace Namespace::fromString(const std::string &str) {
  SharedNamespace reval{new Namespace};
  reval->setName(str);
  return reval;
}

const std::string &Namespace::name() const {
  return m_name;
}

void Namespace::setName(std::string name) {
  m_name = std::move(name);
}

std::string Namespace::stringify() const {
  auto parent_ = parent().lock();
  if (parent_) {
    std::stringstream builder{};
    builder << toString(*parent_);
    builder << ".";
    builder << name();
    return builder.str();
  } else {
    return name();
  }
}

bool Namespace::hasParent() const {
  return m_parent.lock() != nullptr;
}

bool Namespace::isRoot() const {
  return !hasParent();
}

WeakNamespace Namespace::parent() {
  return m_parent;
}

WeakConstNamespace Namespace::parent() const {
  return m_parent;
}

SharedConstNamespace Namespace::root() const {
  if (const auto p = parent().lock()) {
    return p->root();
  } else {
    return shared_from_this();
  }
}

std::vector<SharedConstNamespace> Namespace::path() const {
  if (const auto p = parent().lock()) {
    auto reval = p->path();
    reval.push_back(shared_from_this());
    return reval;
  } else {
    return {shared_from_this()};
  }
}

std::vector<SharedNamespace> Namespace::children() {
  return m_children;
}

std::vector<SharedConstNamespace> Namespace::children() const {
  std::vector<SharedConstNamespace> reval{};
  std::copy(begin(m_children), end(m_children), std::back_inserter(reval));
  return reval;
}

SharedNamespace Namespace::searchChild(const std::string &name) {
  const auto search = std::find_if(begin(m_children), end(m_children), [&name](auto i) { return i->name() == name; });
  if (search == end(m_children)) {
    return nullptr;
  } else {
    return *search;
  }
}

SharedNamespace Namespace::searchChild(ConstSpan<std::string> path) {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty())
  const auto ns = searchChild(path[0]);
  if (path.size() > 1) {
    return ns->searchChild(path.slice(1));
  } else {
    return ns;
  }
}

SharedConstNamespace Namespace::searchChild(const std::string &name) const {
  const auto search =
      std::find_if(begin(m_children), end(m_children), [&name](const auto i) { return i->name() == name; });
  if (search == end(m_children)) {
    return nullptr;
  } else {
    return *search;
  }
}

SharedConstNamespace Namespace::searchChild(ConstSpan<std::string> path) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty())
  const auto ns = searchChild(path[0]);
  if (ns && path.size() > 1) {
    return ns->searchChild(path.slice(1));
  } else {
    return ns;
  }
}

void Namespace::addChild(SharedNamespace ns) {
  XI_EXCEPTIONAL_IF(NullArgumentError, ns == nullptr);
  XI_EXCEPTIONAL_IF(InvalidArgumentError, ns->hasParent(), "namespace already has a parent");
  ns->setParent(shared_from_this());
  m_children.push_back(ns);
}

std::vector<SharedConstNamespace> Namespace::shortestPathTo(SharedConstNamespace ns) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, ns == nullptr);
  auto thisPath = path();
  assert(!thisPath.empty());
  auto nsPath = ns->path();
  assert(!nsPath.empty());

  size_t i = 0;
  while (i < thisPath.size() && i < nsPath.size() && thisPath[i]->name() == nsPath[i]->name()) {
    i += 1;
  }
  nsPath.erase(begin(nsPath), std::next(begin(nsPath), static_cast<std::ptrdiff_t>(i)));
  return nsPath;
}

void Namespace::setParent(WeakNamespace parent) {
  m_parent = parent;
}

bool lessThan(SharedConstNamespace lhs, SharedConstNamespace rhs) {
  if (lhs.get() == rhs.get()) {
    return false;
  } else if (lhs == nullptr) {
    return true;
  } else if (rhs == nullptr) {
    return false;
  } else {
    auto lhsPath = lhs->path();
    auto rhsPath = rhs->path();

    if (lhsPath.size() < rhsPath.size()) {
      return true;
    } else if (lhsPath.size() > rhs->path().size()) {
      return false;
    }

    size_t i = 0;
    while (i < lhsPath.size() && i < rhsPath.size()) {
      if (lhsPath[i]->name() < rhsPath[i]->name()) {
        return true;
      } else if (rhsPath[i]->name() < lhsPath[i]->name()) {
        return false;
      }
      i += 1;
    }
    return false;  // equal
  }
}

bool notEqualTo(SharedConstNamespace lhs, SharedConstNamespace rhs) {
  if (lhs == nullptr && rhs == nullptr) {
    return false;
  } else if (lhs == nullptr || rhs == nullptr) {
    return true;
  } else if (lhs.get() == rhs.get()) {
    return false;
  } else {
    return lhs->name() != rhs->name() || (notEqualTo(lhs->parent().lock(), rhs->parent().lock()));
  }
}

bool equalTo(SharedConstNamespace lhs, SharedConstNamespace rhs) {
  if (lhs.get() == rhs.get()) {
    return true;
  } else {
    return !(notEqualTo(lhs, rhs));
  }
}

}  // namespace Generator

// namespace Generator
}  // namespace Idl
}  // namespace Xi

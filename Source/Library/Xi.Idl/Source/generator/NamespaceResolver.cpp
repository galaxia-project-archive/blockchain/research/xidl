/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/NamespaceResolver.hpp"

#include <cassert>

#include <Xi/Exceptions.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Idl::Generator, NamespaceResolver)
XI_ERROR_CODE_DESC(AliasOverload, "alias is used twice resolving to different namespaces")
XI_ERROR_CODE_DESC(AliasAmbigious, "alias is equal to a root namespace name")
XI_ERROR_CODE_DESC(NamespaceAmbigious, "namespace reference is ambigious")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Idl {
namespace Generator {

Result<SharedConstNamespace> NamespaceResolver::addNamespace(ConstSpan<std::string> path) {
  const auto res = addMutableNamespace(path);
  XI_ERROR_PROPAGATE(res);
  return emplaceSuccess<SharedConstNamespace>(*res);
}

Result<SharedConstNamespace> NamespaceResolver::addAlias(const std::string &alias, SharedConstNamespace ns) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, ns);
  XI_FAIL_IF(const auto existingRoot = searchRoot(alias), NamespaceResolverError::AliasAmbigious)

  if (const auto existingAlias = searchAlias(alias)) {
    XI_FAIL_IF(notEqualTo(existingAlias, ns), NamespaceResolverError::AliasOverload)
    return success(existingAlias);
  } else {
    m_alias[alias] = ns;
    return success(ns);
  }
}

Result<SharedConstNamespace> NamespaceResolver::addAlias(const std::string &alias, ConstSpan<std::string> path) {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty());
  XI_FAIL_IF(const auto existingRoot = searchRoot(alias), NamespaceResolverError::AliasAmbigious)
  auto ns = addMutableNamespace(path);
  XI_ERROR_PROPAGATE(ns)
  return addAlias(alias, *ns);
}

SharedConstNamespace NamespaceResolver::searchRoot(const std::string &name) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, name.empty());
  const auto search = m_rootNamespaces.find(name);
  return search == end(m_rootNamespaces) ? nullptr : search->second;
}

SharedConstNamespace NamespaceResolver::searchAlias(const std::string &name) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, name.empty());
  const auto search = m_alias.find(name);
  return search == end(m_alias) ? nullptr : search->second;
}

SharedConstNamespace NamespaceResolver::searchNamespace(const std::string &name) const {
  return searchNamespace(makeConstSpan(name));
}

SharedConstNamespace NamespaceResolver::searchNamespace(ConstSpan<std::string> path) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty());
  const auto root = searchRoot(path[0]);
  if (!root) {
    return nullptr;
  } else if (path.size() > 1) {
    return root->searchChild(path.slice(1));
  } else {
    return root;
  }
}

SharedConstNamespace NamespaceResolver::resolveNamespace(const std::string &name) const {
  return resolveNamespace(makeConstSpan(name));
}

SharedConstNamespace NamespaceResolver::resolveNamespace(ConstSpan<std::string> path) const {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty());
  if (path.size() == 1) {
    auto alias = searchAlias(path[0]);
    XI_RETURN_SC_IF(alias, alias);
  }

  auto ns = currentNamespace();
  while (ns) {
    auto search = ns->searchChild(path);
    XI_RETURN_SC_IF(search, search);
    ns = ns->parent().lock();
  }

  auto root = m_rootNamespaces.find(path[0]);
  if (root != m_rootNamespaces.end()) {
    XI_RETURN_SC_IF(path.size() == 1, root->second);
    XI_RETURN_SC(root->second->searchChild(path.slice(1)));
  }

  XI_RETURN_SC(nullptr);
}

Result<SharedNamespace> NamespaceResolver::addMutableNamespace(ConstSpan<std::string> path) {
  XI_EXCEPTIONAL_IF(NullArgumentError, path.empty());
  XI_FAIL_IF(searchAlias(path[0]), NamespaceResolverError::AliasOverload)

  SharedNamespace ns = nullptr;
  if (const auto root = m_rootNamespaces.find(path[0]); root != end(m_rootNamespaces)) {
    ns = root->second;
  } else {
    ns = Namespace::fromString(path[0]);
    m_rootNamespaces[path[0]] = ns;
  }
  path = path.slice(1);

  while (!path.empty()) {
    auto search = ns->searchChild(path[0]);
    if (search) {
      ns = search;
    } else {
      auto newNs = Namespace::fromString(path[0]);
      ns->addChild(newNs);
      ns = newNs;
    }
    path = path.slice(1);
  }
  return success(ns);
}

SharedConstNamespace NamespaceResolver::currentNamespace() const {
  return m_currentNamespace;
}

void NamespaceResolver::setCurrentNamespace(SharedConstNamespace ns) {
  m_currentNamespace = ns;
}

Result<void> NamespaceResolver::import(const NamespaceResolver &other) {
  for (const auto &ns : other.m_rootNamespaces) {
    XI_ERROR_PROPAGATE_CATCH(import(ns.second));
  }
  XI_SUCCEED();
}

Result<void> NamespaceResolver::import(SharedConstNamespace ns) {
  auto nsPath = ns->path();
  std::vector<std::string> strNsPath{};
  strNsPath.reserve(nsPath.size());
  std::transform(nsPath.begin(), nsPath.end(), std::back_inserter(strNsPath),
                 [](const auto &iNs) { return iNs->name(); });
  XI_ERROR_PROPAGATE_CATCH(addNamespace(strNsPath))
  for (const auto &child : ns->children()) {
    XI_ERROR_PROPAGATE_CATCH(import(child))
  }
  XI_SUCCEED()
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Scope.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const Scope Scope::EmptyScope{/* */};

const TypeCollection &Scope::types(SharedConstNamespace ns) const {
  return m_types[ns];
}

const TypeCollection &Scope::types() const {
  return types(currentNamespace());
}

TypeCollection &Scope::types(SharedConstNamespace ns) {
  return m_types[ns];
}

TypeCollection &Scope::types() {
  return types(currentNamespace());
}

const ContractCollection &Scope::contracts(SharedConstNamespace ns) const {
  return m_contracts[ns];
}

const ContractCollection &Scope::contracts() const {
  return contracts(currentNamespace());
}

ContractCollection &Scope::contracts(SharedConstNamespace ns) {
  return m_contracts[ns];
}

ContractCollection &Scope::contracts() {
  return contracts(currentNamespace());
}

const ServiceCollection &Scope::services(SharedConstNamespace ns) const {
  return m_services[ns];
}

const ServiceCollection &Scope::services() const {
  return services(currentNamespace());
}

ServiceCollection &Scope::services(SharedConstNamespace ns) {
  return m_services[ns];
}

ServiceCollection &Scope::services() {
  return services(currentNamespace());
}

const NamespaceResolver Scope::namespaces() const {
  return m_namespaces;
}

NamespaceResolver &Scope::namespaces() {
  return m_namespaces;
}

SharedConstNamespace Scope::currentNamespace() const {
  return namespaces().currentNamespace();
}

Result<void> Scope::import(const Scope &other) {
  XI_ERROR_PROPAGATE_CATCH(namespaces().import(other.namespaces()));
  for (const auto &typeCollection : other.m_types) {
    for (const auto &type : typeCollection.second) {
      XI_ERROR_PROPAGATE_CATCH(types(type.namespace_()).add(type));
    }
  }
  for (const auto &contractCollection : other.m_contracts) {
    for (const auto &contract : contractCollection.second) {
      XI_ERROR_PROPAGATE_CATCH(contracts(contract.namespace_()).add(contract));
    }
  }
  for (const auto &serviceCollection : other.m_services) {
    for (const auto &service : serviceCollection.second) {
      XI_ERROR_PROPAGATE_CATCH(services(service.namespace_()).add(service));
    }
  }
  XI_SUCCEED()
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

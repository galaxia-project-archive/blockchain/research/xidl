/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/SerializationTag.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Idl::Generator, SerializationTag)
XI_ERROR_CODE_DESC(NullBinaryTag, "binary tags must be at least 1")
XI_ERROR_CODE_DESC(NullTextTag, "text tags may not be empty")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Idl {
namespace Generator {

const SerializationTag::text_type &SerializationTag::text() const {
  return m_text;
}

SerializationTag::binary_type SerializationTag::binary() const {
  return m_binary;
}

SerializationTagBuilder::SerializationTagBuilder() : m_tag{result_success, SerializationTag{/* */}} {
  /* */
}

SerializationTagBuilder &SerializationTagBuilder::withText(const SerializationTag::text_type &text) {
  if (!m_tag.isError()) {
    if (text.empty()) {
      m_tag = makeError(SerializationTagError::NullTextTag);
    } else {
      m_tag->m_text = text;
    }
  }
  return *this;
}

SerializationTagBuilder &SerializationTagBuilder::withBinary(const SerializationTag::binary_type binary) {
  if (!m_tag.isError()) {
    if (binary == 0) {
      m_tag = makeError(SerializationTagError::NullBinaryTag);
    } else {
      m_tag->m_binary = binary;
    }
  }
  return *this;
}

Result<SerializationTag> SerializationTagBuilder::build() {
  return m_tag;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Service.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

const std::string &Service::name() const {
  return m_name;
}

const Documentation &Service::documentation() const {
  return m_doc;
}

SharedConstNamespace Service::namespace_() const {
  return m_namespace;
}

const ServiceCommandCollection &Service::contracts() const {
  return m_commands;
}

void Service::setName(const std::string &name_) {
  m_name = name_;
}

void Service::setDocumentation(const Documentation &doc) {
  m_doc = doc;
}

void Service::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

ServiceCommandCollection &Service::contracts() {
  return m_commands;
}

ServiceBuilder::ServiceBuilder() : m_service{result_success, Service{/* */}} {
  /* */
}

ServiceBuilder &ServiceBuilder::withName(const std::string name) {
  if (!m_service.isError()) {
    m_service->setName(name);
  }
  return *this;
}

ServiceBuilder &ServiceBuilder::withDocumentation(const Documentation &doc) {
  if (!m_service.isError()) {
    m_service->setDocumentation(doc);
  }
  return *this;
}

ServiceBuilder &ServiceBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_service.isError()) {
    m_service->setNamespace(ns);
  }
  return *this;
}

ServiceBuilder &ServiceBuilder::withCommand(const ServiceCommand &command) {
  if (!m_service.isError()) {
    const auto ec = m_service->contracts().add(command);
    if (ec.isError()) {
      m_service = ec.error();
    }
  }
  return *this;
}

Result<Service> ServiceBuilder::build() {
  return m_service;
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

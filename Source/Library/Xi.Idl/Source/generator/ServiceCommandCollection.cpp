/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/ServiceCommandCollection.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

Result<void> ServiceCommandCollection::add(ServiceCommand command) {
  XI_ERROR_TRY
  m_commands.emplace_back(std::move(command));
  return success();
  XI_ERROR_CATCH
}

ServiceCommandCollection::const_iterator ServiceCommandCollection::begin() const {
  return std::begin(m_commands);
}

ServiceCommandCollection::const_iterator ServiceCommandCollection::end() const {
  return std::end(m_commands);
}

ServiceCommandCollection::const_iterator ServiceCommandCollection::cbegin() const {
  return std::cbegin(m_commands);
}

ServiceCommandCollection::const_iterator ServiceCommandCollection::cend() const {
  return std::cend(m_commands);
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

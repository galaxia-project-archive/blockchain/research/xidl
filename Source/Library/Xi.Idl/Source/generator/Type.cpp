/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/Type.hpp"

#include <typeinfo>

#include <Xi/Exceptions.hpp>

#include "Xi/Idl/Generator/Primitive.hpp"
#include "Xi/Idl/Generator/Array.hpp"
#include "Xi/Idl/Generator/Vector.hpp"
#include "Xi/Idl/Generator/Typedef.hpp"
#include "Xi/Idl/Generator/Alias.hpp"
#include "Xi/Idl/Generator/Package.hpp"
#include "Xi/Idl/Generator/Variant.hpp"
#include "Xi/Idl/Generator/Enum.hpp"
#include "Xi/Idl/Generator/Flag.hpp"

namespace Xi {
namespace Idl {
namespace Generator {

struct Type::_StorageConcept {
  virtual ~_StorageConcept() = default;

  virtual const std::type_info& type() const = 0;
  virtual const void* data() const = 0;
  virtual std::unique_ptr<_StorageConcept> clone() const = 0;
};

template <typename _DataT>
struct Type::_Storage final : Type::_StorageConcept {
  _DataT dataStorage;

  _Storage(const _DataT& data) : dataStorage{data} {
    /* */
  }

  const std::type_info& type() const {
    return typeid(dataStorage);
  }

  const void* data() const {
    return std::addressof(dataStorage);
  }

  std::unique_ptr<_StorageConcept> clone() const {
    return std::make_unique<_Storage<_DataT>>(dataStorage);
  }
};

Type::Type(const Type& other)
    : enable_shared_from_this(other),
      m_name{other.name()},
      m_isOptional{other.isOptional()},
      m_kind{other.kind()},
      m_namespace{other.namespace_()},
      m_data{nullptr} {
  if (other.m_data) {
    m_data = other.m_data->clone();
  }
}

Type& Type::operator=(const Type& other) {
  setName(other.name());
  setIsOptional(other.isOptional());
  setKind(other.kind());
  setNamespace(other.namespace_());
  if (other.m_data) {
    m_data = other.m_data->clone();
  }
  return *this;
}

Type::Type(Type&& other)
    : m_name{other.name()},
      m_isOptional{other.isOptional()},
      m_kind{other.kind()},
      m_namespace{other.namespace_()},
      m_data{std::move(other.m_data)} {
}

Type& Type::operator=(Type&& other) {
  setName(other.name());
  setIsOptional(other.isOptional());
  setKind(other.kind());
  setNamespace(other.namespace_());
  m_data = std::move(other.m_data);
  return *this;
}

Type::~Type() {
  /* */
}

const std::string& Type::name() const {
  return m_name;
}

void Type::setName(const std::string& n) {
  m_name = n;
}

bool Type::isOptional() const {
  return m_isOptional;
}

void Type::setIsOptional(bool is) {
  m_isOptional = is;
}

Type::Kind Type::kind() const {
  return m_kind;
}

void Type::setKind(Type::Kind k) {
  m_kind = k;
}

SharedConstNamespace Type::namespace_() const {
  return m_namespace;
}

Type Type::toOptional() const {
  return toOptional(true);
}

Type Type::toOptional(bool isOptional_) const {
  Type reval = *this;
  reval.m_isOptional = isOptional_;
  return reval;
}

Type::Type() {
  /* */
}

void Type::setNamespace(SharedConstNamespace ns) {
  m_namespace = ns;
}

TypeBuilder::TypeBuilder() {
  m_type = Result<Type>{result_success, Type{}};
}

TypeBuilder& TypeBuilder::withName(const std::string& name) {
  if (!m_type.isError()) {
    m_type->setName(name);
  }
  return *this;
}

TypeBuilder& TypeBuilder::withIsOptional(bool isOptional) {
  if (!m_type.isError()) {
    m_type->setIsOptional(isOptional);
  }
  return *this;
}

TypeBuilder& TypeBuilder::withKind(Type::Kind kind) {
  if (!m_type.isError()) {
    m_type->setKind(kind);
  }
  return *this;
}

TypeBuilder& TypeBuilder::withNamespace(SharedConstNamespace ns) {
  if (!m_type.isError()) {
    m_type->setNamespace(ns);
  }
  return *this;
}

Result<Type> TypeBuilder::build() {
  return m_type;
}

#define XI_STORAGE_TYPE_CASE(TYPE)                                                  \
  bool Type::is##TYPE() const {                                                     \
    return kind() == Kind::TYPE;                                                    \
  }                                                                                 \
  const TYPE& Type::as##TYPE() const {                                              \
    XI_EXCEPTIONAL_IF_NOT(RuntimeError, m_data)                                     \
    XI_EXCEPTIONAL_IF_NOT(InvalidVariantTypeError, m_data->type() == typeid(TYPE)); \
    return *static_cast<const TYPE*>(m_data->data());                               \
  }                                                                                 \
  void Type::setData(const TYPE& data) {                                            \
    m_data = std::make_unique<_Storage<TYPE>>(data);                                \
  }                                                                                 \
  TypeBuilder& TypeBuilder::withData(const TYPE& data) {                            \
    if (!m_type.isError()) {                                                        \
      m_type->setData(data);                                                        \
    }                                                                               \
    return *this;                                                                   \
  }

XI_STORAGE_TYPE_CASE(Primitive)
XI_STORAGE_TYPE_CASE(Array)
XI_STORAGE_TYPE_CASE(Vector)
XI_STORAGE_TYPE_CASE(Typedef)
XI_STORAGE_TYPE_CASE(Alias)
XI_STORAGE_TYPE_CASE(Package)
XI_STORAGE_TYPE_CASE(Variant)
XI_STORAGE_TYPE_CASE(Enum)
XI_STORAGE_TYPE_CASE(Flag)

#undef XI_STORAGE_TYPE_CASE

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

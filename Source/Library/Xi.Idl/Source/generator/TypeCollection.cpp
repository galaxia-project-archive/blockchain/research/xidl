/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Generator/TypeCollection.hpp"

#include <utility>
#include <algorithm>

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Idl {
namespace Generator {

Result<void> TypeCollection::add(Type type) {
  m_types.push_back(std::move(type));
  return success();
}

std::optional<Type> TypeCollection::search(const std::string &name) const {
  const auto it = std::find_if(begin(), end(), [&name](const auto &t) { return t.name() == name; });
  if (it == end()) {
    return std::nullopt;
  } else {
    return *it;
  }
}

TypeCollection::const_iterator TypeCollection::begin() const {
  return std::begin(m_types);
}

TypeCollection::const_iterator TypeCollection::end() const {
  return std::end(m_types);
}

TypeCollection::const_iterator TypeCollection::cbegin() const {
  return std::cbegin(m_types);
}

TypeCollection::const_iterator TypeCollection::cend() const {
  return std::cend(m_types);
}

}  // namespace Generator
}  // namespace Idl
}  // namespace Xi

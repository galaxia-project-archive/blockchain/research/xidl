/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Atomic.hpp"

#include <Xi/Log/Log.hpp>
#include <Xi/String/ToString.hpp>

#include "Xi/Idl/Parser/ParserError.hpp"

XI_LOGGER("Idl/Parser")

namespace Xi {
namespace Idl {
namespace Parser {

Result<void> AtomicAutomaton::doProcessToken(const Token &token) {
  if (token.value == "byte") {
    m_result = Byte{/* */};
  } else if (token.value == "uint8") {
    m_result = UInt8{/* */};
  } else if (token.value == "int8") {
    m_result = Int8{/* */};
  } else if (token.value == "uint16") {
    m_result = UInt16{/* */};
  } else if (token.value == "int16") {
    m_result = Int16{/* */};
  } else if (token.value == "uint32") {
    m_result = UInt32{/* */};
  } else if (token.value == "int32") {
    m_result = Int32{/* */};
  } else if (token.value == "uint64") {
    m_result = UInt64{/* */};
  } else if (token.value == "int64") {
    m_result = Int64{/* */};
  } else if (token.value == "float16") {
    m_result = Float16{/* */};
  } else if (token.value == "float32") {
    m_result = Float32{/* */};
  } else if (token.value == "float64") {
    m_result = Float64{/* */};
  } else if (token.value == "boolean") {
    m_result = Boolean{/* */};
  } else if (token.value == "string") {
    m_result = String{/* */};
  } else if (token.value == "#") {
    m_result = Reference{/* */};
    auto ref = makeReferenceAutomaton(std::get<Reference>(m_result));
    chain(ref);
    markFinished();
    return ref->processToken(token);
  } else {
    XI_ERROR("Unknown type keyword: {} at {}", token.value, token.begin)
    XI_FAIL(ParserError::UnexpectedToken)
  }
  markFinished();
  XI_SUCCEED()
}

std::shared_ptr<AtomicAutomaton> makeAtomicAutomaton(Atomic &result) {
  std::shared_ptr<AtomicAutomaton> reval{new AtomicAutomaton{result}};
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Automaton.hpp"

#include <optional>

#include <Xi/Exceptions.hpp>
#include <Xi/Stream/InMemoryInputStream.hpp>

#include "Xi/Idl/Parser/ParserError.hpp"
#include "Xi/Idl/Parser/Documentation.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

class SpaceAutomaton : public Automaton {
 public:
  SpaceAutomaton() {
    listenToVoid();
  }

  Result<void> doProcessToken(const Token &token) override {
    XI_SUCCEED_IF(token.isSpace());
    return propagateBack(token);
  }
};

class NewLineAutomaton : public Automaton {
 public:
  NewLineAutomaton() {
    listenToVoid();
  }

  Result<void> doProcessToken(const Token &token) override {
    XI_SUCCEED_IF(token.isNewLine());
    return propagateBack(token);
  }
};

class VoidAutomaton : public Automaton {
 public:
  VoidAutomaton() {
    listenToVoid();
  }

  Result<void> doProcessToken(const Token &token) override {
    XI_SUCCEED_IF(token.isVoid());
    return propagateBack(token);
  }
};

class SkipLineAutomaton : public Automaton {
 public:
  SkipLineAutomaton() {
    listenToVoid();
  }

  Result<void> doProcessToken(const Token &token) override {
    if (token.isNewLine()) {
      markFinished();
    }
    XI_SUCCEED();
  }
};

class MultilineCommentAutomaton : public Automaton {
 public:
  Result<void> doProcessToken(const Token &token) override {
    if (token.value == "%>") {
      markFinished();
    }
    XI_SUCCEED();
  }
};

Automaton::~Automaton() {
  /* */
}

Result<void> Automaton::process(const std::string &content) {
  Stream::InMemoryInputStream stream{asConstByteSpan(content)};
  Cursor cursor{stream};
  return process(cursor);
}

Result<void> Automaton::process(Cursor &cursor) {
  Token token{};
  do {
    XI_SUCCEED_IF(isFinished()) {
      auto ec = cursor.readToken();
      XI_ERROR_PROPAGATE(ec)
      token = ec.take();
    }
    {
      auto ec = processToken(token);
      XI_ERROR_PROPAGATE(ec)
    }
  } while (!token.isEndOfStream());
  XI_SUCCEED()
}

Result<void> Automaton::processToken(const Token &token) {
  XI_ERROR_TRY
  if (token.value == "%") {
    chainFront(std::make_shared<SkipLineAutomaton>());
    XI_SUCCEED()
  } else if (token.value == "<%") {
    chainFront(std::make_shared<MultilineCommentAutomaton>());
    XI_SUCCEED()
  }
  for (auto ichild = currentChild(); ichild; ichild = currentChild()) {
    auto childFinished = ichild->isFinished();
    if (!childFinished) {
      return ichild->processToken(token);
    }
    m_chain.pop_front();
  }
  if (isFinished()) {
    return propagateBack(token);
  } else {
    XI_SUCCEED_IF(token.isSpace() && !m_processSpaces);
    XI_SUCCEED_IF(token.isNewLine() && !m_processNewLine);
    return doProcessToken(token);
  }
  XI_ERROR_CATCH
}

bool Automaton::isFinished() {
  for (auto ichild = currentChild(); ichild && ichild->isFinished(); ichild = currentChild()) {
    m_chain.pop_front();
  }
  XI_RETURN_SC_IF_NOT(m_chain.empty(), false);
  XI_RETURN_SC_IF(m_finished, true);
  XI_RETURN_SC(false);
}

void Automaton::chain(SharedAutomaton child) {
  chainBack(child);
}

void Automaton::chainFront(SharedAutomaton child) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, child)
  child->m_parent = shared_from_this();
  m_chain.push_front(child);
}

void Automaton::chainBack(SharedAutomaton child) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, child)
  child->m_parent = shared_from_this();
  m_chain.push_back(child);
}

void Automaton::skipSpaces() {
  chain(std::make_shared<SpaceAutomaton>());
}

void Automaton::skipNewLines() {
  chain(std::make_shared<NewLineAutomaton>());
}

void Automaton::skipVoid() {
  chain(std::make_shared<VoidAutomaton>());
}

Result<void> Automaton::propagateBack(const Token &token) {
  markFinished();

  auto parent = m_parent.lock();
  if (!parent) {
    for (auto &&docEntry : m_docCache) {
      parent->m_docCache.emplace_back(std::move(docEntry));
    }
    XI_FAIL_IF_NOT(token.isEndOfStream(), ParserError::UnexpectedToken)
    XI_SUCCEED()
  } else {
    return parent->processToken(token);
  }
}

void Automaton::markFinished() {
  m_finished = true;
}

void Automaton::listenToSpaces() {
  m_processSpaces = true;
}

void Automaton::listenToNewLine() {
  m_processNewLine = true;
}

void Automaton::listenToVoid() {
  listenToSpaces();
  listenToNewLine();
}

void Automaton::chainDocumentation() {
  chain(makeDocumentationAutomaton(m_docCache));
}

void Automaton::emplaceDocumentation(std::vector<DocumentationEntry> &doc) {
  doc = m_docCache;
  m_docCache.clear();
}

Automaton::Automaton() {
  /* */
}

SharedAutomaton Automaton::currentChild() const {
  if (m_chain.empty()) {
    return nullptr;
  } else {
    return m_chain.front();
  }
}

Result<void> Automaton::doProcessToken(const Token &token) {
  return propagateBack(token);
}

Result<void> ReadLineAutomaton::doProcessToken(const Token &token) {
  if (token.isNewLine()) {
    markFinished();
    XI_SUCCEED()
  } else {
    m_result += token.value;
    XI_SUCCEED()
  }
}

std::shared_ptr<ReadLineAutomaton> makeReadLineAutomaton(std::string &result) {
  std::shared_ptr<ReadLineAutomaton> reval{new ReadLineAutomaton{result}};
  reval->listenToVoid();
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

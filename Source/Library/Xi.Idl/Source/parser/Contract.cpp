/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Contract.hpp"

#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

std::shared_ptr<ContractAutomaton> makeContractAutomaton(Contract& result) {
  std::shared_ptr<ContractAutomaton> reval{new ContractAutomaton{result}};
  reval->chain(makeKeywordAutomaton("contract"));
  reval->chain(makeIdentifierAutomaton(result.name));
  reval->chain(makeKeywordAutomaton("("));
  reval->chain(makeOptionalTypeAutomaton(result.argument));
  reval->chain(makeKeywordAutomaton(")"));
  reval->chain(makeKeywordAutomaton(":"));
  reval->chain(makeOptionalTypeAutomaton(result.returnType));
  reval->chain(makeKeywordAutomaton(";"));
  return reval;
}

Result<void> ContractsAutomaton::doProcessToken(const Token& token) {
  if (token.value == "contract") {
    m_result.emplace_back();
    emplaceDocumentation(m_result.back().doc);
    auto iArg = makeContractAutomaton(m_result.back());
    chain(iArg);
    chainDocumentation();
    return iArg->processToken(token);
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<ContractsAutomaton> makeContractsAutomaton(ContractVector& result) {
  std::shared_ptr<ContractsAutomaton> reval{new ContractsAutomaton{result}};
  reval->chainDocumentation();
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Documentation.hpp"

#include <Xi/Log/Log.hpp>

#include "Xi/Idl/Parser/ParserError.hpp"
#include "Xi/Idl/Parser/Keyword.hpp"

XI_LOGGER("Idl/Parser")

namespace Xi {
namespace Idl {
namespace Parser {

Result<void> DocumentationTagAutomaton::doProcessToken(const Token &token) {
  if (token.isVoid()) {
    m_result = NoDocumentationTag{/* */};
  } else if (token.value == "brief") {
    m_result = BriefDocumentationTag{/* */};
  } else if (token.value == "detailed") {
    m_result = DetailedDocumentationTag{/* */};
  } else if (token.value == "example") {
    m_result = ExampleDocumentationTag{/* */};
  } else {
    XI_FAIL(ParserError::UnexpectedToken)
  }
  markFinished();
  XI_SUCCEED()
}

std::shared_ptr<DocumentationTagAutomaton> makeDocumentationTagAutomaton(DocumentationTag &result) {
  std::shared_ptr<DocumentationTagAutomaton> reval{new DocumentationTagAutomaton{result}};
  reval->listenToVoid();
  return reval;
}

void DocumentationEntryAutomaton::enableMultineEnvironment() {
  m_isMultilineEnvironment = true;
}

Result<void> DocumentationEntryAutomaton::doProcessToken(const Token &token) {
  if (m_isMultilineEnvironment) {
    if (token.value == "@" || token.value == "@>") {
      return propagateBack(token);
    } else {
      if (!m_result.content.empty()) {
        m_result.content.push_back(' ');
      }
      auto line = makeReadLineAutomaton(m_result.content);
      chain(line);
      skipVoid();
      return line->processToken(token);
    }
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<DocumentationEntryAutomaton> makeDocumentationEntryAutomaton(DocumentationEntry &result) {
  std::shared_ptr<DocumentationEntryAutomaton> reval{new DocumentationEntryAutomaton{result}};
  reval->listenToVoid();
  reval->chain(makeDocumentationTagAutomaton(result.tag));
  reval->skipSpaces();
  reval->chain(makeReadLineAutomaton(result.content));
  reval->skipVoid();
  return reval;
}

Result<void> DocumentationAutomaton::doProcessToken(const Token &token) {
  if (token.value == "@") {
    m_result.emplace_back();
    auto entryAutomaton = makeDocumentationEntryAutomaton(m_result.back());
    if (m_isMultilineEnvironment) {
      entryAutomaton->enableMultineEnvironment();
    }
    chain(entryAutomaton);
    XI_SUCCEED()
  } else if (token.value == "<@") {
    m_isMultilineEnvironment = true;
    if (!m_result.empty()) {
      XI_ERROR("Multiline documentation after single line documentation at {}", token.begin)
      XI_FAIL(ParserError::UnexpectedToken)
    }
    m_result.emplace_back();
    auto entryAutomaton = makeDocumentationEntryAutomaton(m_result.back());
    entryAutomaton->enableMultineEnvironment();
    chain(entryAutomaton);
    XI_SUCCEED()
  } else if (token.value == "@>") {
    markFinished();
    XI_SUCCEED()
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<DocumentationAutomaton> makeDocumentationAutomaton(Documentation &result) {
  std::shared_ptr<DocumentationAutomaton> reval{new DocumentationAutomaton{result}};
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

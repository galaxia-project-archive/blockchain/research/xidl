/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Enum.hpp"

#include "Xi/Idl/Parser/Constants.hpp"
#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

std::shared_ptr<EnumEntryAutomaton> makeEnumEntryAutomaton(EnumEntry& result) {
  std::shared_ptr<EnumEntryAutomaton> reval{new EnumEntryAutomaton{result}};
  reval->chain(makeDocumentationAutomaton(result.doc));
  reval->chain(makeIdentifierAutomaton(result.name));
  reval->chain(makeTagAutomaton(result.tag));
  reval->chain(makeKeywordAutomaton(";"));
  return reval;
}

Result<void> EnumEntriesAutomaton::doProcessToken(const Token& token) {
  if (token.value != "}") {
    m_result.emplace_back();
    auto ifield = makeEnumEntryAutomaton(m_result.back());
    chain(ifield);
    return ifield->processToken(token);
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<EnumEntriesAutomaton> makeEnumEntriesAutomaton(EnumEntryVector& result) {
  std::shared_ptr<EnumEntriesAutomaton> reval{new EnumEntriesAutomaton{result}};
  result.emplace_back();
  reval->chain(makeEnumEntryAutomaton(result.back()));
  return reval;
}

std::shared_ptr<EnumAutomaton> makeEnumAutomaton(Enum& result) {
  std::shared_ptr<EnumAutomaton> reval{new EnumAutomaton{result}};
  reval->chain(makeKeywordAutomaton("enum"));
  reval->chain(makeIdentifierAutomaton(result.name));
  reval->chain(makeKeywordAutomaton("{"));
  reval->chain(makeEnumEntriesAutomaton(result.values));
  reval->chain(makeKeywordAutomaton("}"));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

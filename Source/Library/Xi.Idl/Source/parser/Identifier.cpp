/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Identifier.hpp"

#include <cctype>
#include <algorithm>

#include <Xi/Exceptions.hpp>

#include "Xi/Idl/Parser/ParserError.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

bool isValidIdentifier(const Identifier::value_type ch) {
  return isalpha(ch) || isdigit(ch) || (ch == '_');
}

bool isValidIdentifier(const Identifier &id) {
  return !id.empty() && isalpha(id[0]) &&
         std::all_of(id.begin(), id.end(), [](const auto ch) { return isValidIdentifier(ch); });
}

Result<void> IdentifierAutomaton::doProcessToken(const Token &token) {
  if (!m_result.empty()) {
    return propagateBack(token);
  }
  XI_FAIL_IF_NOT(isValidIdentifier(token.value), ParserError::InvalidIdentifier);
  m_result = token.value;
  markFinished();
  XI_SUCCEED()
}

std::shared_ptr<IdentifierAutomaton> makeIdentifierAutomaton(Identifier &result) {
  return std::shared_ptr<IdentifierAutomaton>{new IdentifierAutomaton{result}};
}

Result<void> IdentifiersAutomaton::doProcessToken(const Token &token) {
  if (token.value == ".") {
    XI_FAIL_IF(m_result.empty() || m_result.front().empty(), ParserError::UnexpectedToken);
    m_result.emplace_back();
    chain(makeIdentifierAutomaton(m_result.back()));
    XI_SUCCEED()
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<IdentifiersAutomaton> makeIdentifiersAutomaton(IdentifierVector &result) {
  std::shared_ptr<IdentifiersAutomaton> reval{new IdentifiersAutomaton{result}};
  reval->m_result.emplace_back();
  reval->chain(makeIdentifierAutomaton(result.back()));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

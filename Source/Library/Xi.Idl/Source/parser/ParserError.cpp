/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/ParserError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Idl::Parser, Parser)
XI_ERROR_CODE_DESC(NamespaceMissing, "namespace declaration is missing")
XI_ERROR_CODE_DESC(InvalidNamespaceDeclaration, "namespace declaration is ill formed")
XI_ERROR_CODE_DESC(UnexpectedEndOfStream, "stream ended but more tokens were expected")
XI_ERROR_CODE_DESC(InvalidIdentifier, "identifier phrasing is invalid")
XI_ERROR_CODE_DESC(IllFormedNumber, "number expected but token encodes an invalid number format")
XI_ERROR_CODE_DESC(UnexpectedToken, "token at that position is invalid")
XI_ERROR_CODE_DESC(NumberOutOfRange, "number is out of range")
XI_ERROR_CODE_DESC(MissingSemicolon, "semicolon missing")
XI_ERROR_CODE_CATEGORY_END()

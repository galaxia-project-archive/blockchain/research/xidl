/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Service.hpp"

#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

std::shared_ptr<ServiceCommandAutomaton> makeServiceCommandAutomaton(ServiceCommand& result) {
  std::shared_ptr<ServiceCommandAutomaton> reval{new ServiceCommandAutomaton{result}};
  reval->chain(makeDocumentationAutomaton(result.doc));
  reval->chain(makeKeywordAutomaton("serves"));
  reval->chain(makeReferenceAutomaton(result.contract));
  reval->chain(makeKeywordAutomaton("as"));
  reval->chain(makeTagAutomaton(result.tag));
  reval->chain(makeKeywordAutomaton(";"));
  return reval;
}

Result<void> ServiceCommandsAutomaton::doProcessToken(const Token& token) {
  if (token.value != "}") {
    m_result.emplace_back();
    chain(makeDocumentationAutomaton(m_result.back().doc));
    chain(makeServiceCommandAutomaton(m_result.back()));
    return processToken(token);
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<ServiceCommandsAutomaton> makeServiceCommandsAutomaton(ServiceCommandVector& result) {
  return std::shared_ptr<ServiceCommandsAutomaton>{new ServiceCommandsAutomaton{result}};
}

std::shared_ptr<ServiceAutomaton> makeServiceAutomaton(Service& result) {
  std::shared_ptr<ServiceAutomaton> reval{new ServiceAutomaton{result}};
  reval->chain(makeKeywordAutomaton("service"));
  reval->chain(makeIdentifierAutomaton(result.name));
  reval->chain(makeKeywordAutomaton("{"));
  reval->chain(makeServiceCommandsAutomaton(result.commands));
  reval->chain(makeKeywordAutomaton("}"));
  return reval;
}

Result<void> ServicesAutomaton::doProcessToken(const Token& token) {
  if (token.value == "service") {
    m_result.emplace_back();
    emplaceDocumentation(m_result.back().doc);
    auto iArg = makeServiceAutomaton(m_result.back());
    chain(iArg);
    chainDocumentation();
    return iArg->processToken(token);
  } else {
    return propagateBack(token);
  }
}

std::shared_ptr<ServicesAutomaton> makeServicesAutomaton(ServiceVector& result) {
  std::shared_ptr<ServicesAutomaton> reval{new ServicesAutomaton{result}};
  reval->chainDocumentation();
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Idl/Parser/Tag.hpp"

#include "Xi/Idl/Parser/Identifier.hpp"
#include "Xi/Idl/Parser/Constants.hpp"
#include "Xi/Idl/Parser/Keyword.hpp"

namespace Xi {
namespace Idl {
namespace Parser {

std::shared_ptr<TagAutomaton> makeTagAutomaton(Tag &result) {
  std::shared_ptr<TagAutomaton> reval{new TagAutomaton{result}};
  reval->chain(makeKeywordAutomaton("("));
  reval->chain(makeNumberConstantAutomaton(result.binary));
  reval->chain(makeKeywordAutomaton(";"));
  reval->chain(makeIdentifierAutomaton(result.text));
  reval->chain(makeKeywordAutomaton(")"));
  return reval;
}

}  // namespace Parser
}  // namespace Idl
}  // namespace Xi

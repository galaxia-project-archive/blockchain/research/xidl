/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Generator/Namespace.hpp>

#define XI_TEST_SUITE Xi_Idl_Generator_Namespace

namespace {
const std::string RootFirst{"Xi0"};
const std::string RootSecond{"Xi1"};
const std::string ChildFirst{"Data0"};
const std::string ChildSecond{"Data1"};
const std::string ChildChildFirst{"Model0"};
const std::string ChildChildSecond{"Model1"};
}  // namespace

TEST(XI_TEST_SUITE, SearchChild) {
  using namespace Xi::Idl::Generator;

  auto root = Namespace::fromString(RootFirst);
  auto child = Namespace::fromString(ChildFirst);
  root->addChild(child);

  ASSERT_EQ(child->parent().lock(), root);
  EXPECT_TRUE(root->searchChild(ChildFirst));
  EXPECT_FALSE(root->searchChild(RootFirst));
  EXPECT_FALSE(child->searchChild(RootFirst));
}

TEST(XI_TEST_SUITE, RootParentPredicates) {
  using namespace Xi::Idl::Generator;

  auto root = Namespace::fromString(RootFirst);
  auto child = Namespace::fromString(ChildFirst);
  root->addChild(child);

  EXPECT_TRUE(root->isRoot());
  EXPECT_FALSE(root->hasParent());

  EXPECT_FALSE(child->isRoot());
  EXPECT_TRUE(child->hasParent());
}

TEST(XI_TEST_SUITE, SearchChildPath) {
  using namespace Xi::Idl::Generator;

  auto root = Namespace::fromString(RootFirst);
  auto child = Namespace::fromString(ChildFirst);
  auto childChild = Namespace::fromString(ChildChildFirst);
  root->addChild(child);
  child->addChild(childChild);

  ASSERT_EQ(child->parent().lock(), root);
  ASSERT_EQ(childChild->parent().lock(), child);
  EXPECT_TRUE(root->searchChild(ChildFirst));
  std::vector<std::string> path{{ChildFirst, ChildChildFirst}};
  EXPECT_TRUE(root->searchChild(path));
  std::vector<std::string> pathInvalid{{ChildFirst, ChildChildSecond}};
  EXPECT_FALSE(root->searchChild(pathInvalid));
}

TEST(XI_TEST_SUITE, ShortestPath) {
  using namespace ::testing;
  using namespace Xi::Idl::Generator;

  auto n0 = Namespace::fromString("n0");
  auto n0_0 = Namespace::fromString("n0_0");
  auto n0_0_0 = Namespace::fromString("DUPLICATE");
  auto n0_0_1 = Namespace::fromString("n0_0_1");
  auto n0_1 = Namespace::fromString("MORE");

  auto n1 = Namespace::fromString("n1");
  auto n1_0 = Namespace::fromString("n1_1");
  auto n1_0_0 = Namespace::fromString("DUPLICATE");
  auto n1_0_1 = Namespace::fromString("n1_0_1");
  auto n1_1 = Namespace::fromString("MORE");

  n0->addChild(n0_0);
  n0_0->addChild(n0_0_0);
  n0_0->addChild(n0_0_1);
  n0->addChild(n0_1);

  n1->addChild(n1_0);
  n1_0->addChild(n1_0_0);
  n1_0->addChild(n1_0_1);
  n1->addChild(n1_1);

  {
    auto path = n1_0_1->shortestPathTo(n0_0_1);
    ASSERT_THAT(path, SizeIs(Eq(3)));
    EXPECT_EQ(path[0], n0);
    EXPECT_EQ(path[1], n0_0);
    EXPECT_EQ(path[2], n0_0_1);
  }

  {
    auto path = n1_0_0->shortestPathTo(n0_0_0);
    ASSERT_THAT(path, SizeIs(Eq(3)));
    EXPECT_EQ(path[0], n0);
    EXPECT_EQ(path[1], n0_0);
    EXPECT_EQ(path[2], n0_0_0);
  }

  {
    auto path = n1_1->shortestPathTo(n0_1);
    ASSERT_THAT(path, SizeIs(Eq(2)));
    EXPECT_EQ(path[0], n0);
    EXPECT_EQ(path[1], n0_1);
  }

  {
    auto path = n0_1->shortestPathTo(n0_0);
    ASSERT_THAT(path, SizeIs(Eq(1)));
    EXPECT_EQ(path[0], n0_0);
  }

  {
    auto path = n0->shortestPathTo(n0_0_1);
    ASSERT_THAT(path, SizeIs(Eq(2)));
    EXPECT_EQ(path[0], n0_0);
    EXPECT_EQ(path[1], n0_0_1);
  }

  {
    auto path = n0_1->shortestPathTo(n0_0_1);
    ASSERT_THAT(path, SizeIs(Eq(2)));
    EXPECT_EQ(path[0], n0_0);
    EXPECT_EQ(path[1], n0_0_1);
  }

  {
    auto path = n0_0_1->shortestPathTo(n0);
    EXPECT_THAT(path, SizeIs(Eq(0)));
  }
}

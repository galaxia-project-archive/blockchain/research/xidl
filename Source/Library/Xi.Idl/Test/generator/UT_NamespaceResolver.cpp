/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Generator/NamespaceResolver.hpp>

#define XI_TEST_SUITE Xi_Idl_Generator_NamespaceResolver

namespace {
const std::string RootFirst{"Xi0"};
const std::string RootSecond{"Xi1"};
const std::string ChildFirst{"Data0"};
const std::string ChildSecond{"Data1"};
const std::string ChildChildFirst{"Model0"};
const std::string ChildChildSecond{"Model1"};
}  // namespace

TEST(XI_TEST_SUITE, AddAndQuery) {
  using namespace Xi::Idl::Generator;

  auto n0 = Namespace::fromString(RootFirst);
  auto n0_0 = Namespace::fromString(ChildFirst);
  auto n0_0_0 = Namespace::fromString(ChildChildFirst);

  std::vector<std::string> p0{{RootFirst}};
  std::vector<std::string> p0_0{{RootFirst, ChildFirst}};
  std::vector<std::string> p0_0_0{{RootFirst, ChildFirst, ChildChildFirst}};

  NamespaceResolver resolver{};
  resolver.addNamespace(p0_0).throwOnError();

  EXPECT_TRUE(resolver.resolveNamespace(n0->name()));
  EXPECT_FALSE(resolver.resolveNamespace(n0_0->name()));
  EXPECT_TRUE(resolver.resolveNamespace(p0_0));
  EXPECT_FALSE(resolver.resolveNamespace(p0_0_0));
}

TEST(XI_TEST_SUITE, ResolveAlias) {
  using namespace Xi::Idl::Generator;

  auto n0 = Namespace::fromString(RootFirst);
  auto n0_0 = Namespace::fromString(ChildFirst);
  auto n0_0_0 = Namespace::fromString(ChildChildFirst);

  n0->addChild(n0_0);
  n0_0->addChild(n0_0_0);

  std::vector<std::string> p0{{RootFirst}};
  std::vector<std::string> p0_0{{RootFirst, ChildFirst}};
  std::vector<std::string> p0_0_0{{RootFirst, ChildFirst, ChildChildFirst}};

  NamespaceResolver resolver{};
  resolver.addNamespace(p0_0).throwOnError();

  std::string a0_0 = "__";
  resolver.addAlias(a0_0, p0_0).throwOnError();

  auto search = resolver.searchAlias(a0_0);
  ASSERT_TRUE(search);
  EXPECT_TRUE(equalTo(search, n0_0));

  search = resolver.resolveNamespace(a0_0);
}

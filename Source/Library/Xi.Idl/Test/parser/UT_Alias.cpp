/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Alias.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Alias

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Alias alias;
  std::shared_ptr<Xi::Idl::Parser::AliasAutomaton> automaton;

  XI_TEST_SUITE() : alias{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    alias = Xi::Idl::Parser::Alias{/* */};
    automaton = Xi::Idl::Parser::makeAliasAutomaton(alias);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process("alias Hash = #Byte[32];");
  EXPECT_THAT(alias.name, Eq("Hash"));
  EXPECT_FALSE(alias.type.isOptional);
  ASSERT_TRUE(std::holds_alternative<ContainerType>(alias.type.base));
  const auto& container = std::get<ContainerType>(alias.type.base);
  ASSERT_TRUE(std::holds_alternative<Array>(container));
  const auto& array = std::get<Array>(container);
  EXPECT_THAT(array.size, Eq(32));
  ASSERT_TRUE(std::holds_alternative<Reference>(array.baseType));
  const auto& ref = std::get<Reference>(array.baseType);
  ASSERT_THAT(ref.nested, SizeIs(Eq(1)));
  EXPECT_THAT(ref.nested[0], Eq("Byte"));
}

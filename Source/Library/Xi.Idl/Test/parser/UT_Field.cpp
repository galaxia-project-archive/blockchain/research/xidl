/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Field.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Field

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Field field;
  std::shared_ptr<Xi::Idl::Parser::FieldAutomaton> automaton;

  XI_TEST_SUITE() : field{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    field = Xi::Idl::Parser::Field{/* */};
    automaton = Xi::Idl::Parser::makeFieldAutomaton(field);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process("BlockHash (11;block_hash) : #Crypto.Hash?;");
  EXPECT_THAT(field.name, Eq("BlockHash"));
  EXPECT_THAT(field.tag.text, Eq("block_hash"));
  EXPECT_THAT(field.tag.binary, Eq(11));
  EXPECT_TRUE(field.type.isOptional);
  ASSERT_TRUE(std::holds_alternative<Atomic>(field.type.base));
  const auto& atomic = std::get<Atomic>(field.type.base);
  ASSERT_TRUE(std::holds_alternative<Reference>(atomic));
  const auto& ref = std::get<Reference>(atomic);
  ASSERT_THAT(ref.nested, SizeIs(Eq(2)));
  EXPECT_THAT(ref.nested[0], Eq("Crypto"));
  EXPECT_THAT(ref.nested[1], Eq("Hash"));
}

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/TypeDeclaration.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_TypeDecleration

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::TypeDeclarationVector declarations;
  std::shared_ptr<Xi::Idl::Parser::TypeDeclarationsAutomaton> automaton;

  XI_TEST_SUITE() : declarations{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    declarations = Xi::Idl::Parser::TypeDeclarationVector{/* */};
    automaton = Xi::Idl::Parser::makeTypeDeclarationsAutomaton(declarations);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
alias Hash = #Byte[32];

enum TransactionContainer {
  Main (1;main);
  Alternative (2;alternative);
  Pool (3;pool);
}

flag BlockFeatures {
  StaticReward (1;static_reward);
  ProofOfWork (2;proof_of_work);
  BlockReward (3;block_reward);
}

package Block extends #BlockHeader {
  Transactions (1;transactions) : #Transaction[*];
  CumulativeFees (2;cumulative_fees) : uint64;
}

typedef Hash = #Byte[32];

variant Search {
  Hash    (1;hash) : #Crpto.Hash;
  Height  (2;height) : uint64;
}
)__");

  ASSERT_THAT(declarations, SizeIs(Eq(6)));

  {
    ASSERT_TRUE(std::holds_alternative<Alias>(declarations[0].value));
    const auto& alias = std::get<Alias>(declarations[0].value);

    EXPECT_THAT(alias.name, Eq("Hash"));
    EXPECT_FALSE(alias.type.isOptional);
    ASSERT_TRUE(std::holds_alternative<ContainerType>(alias.type.base));
    const auto& container = std::get<ContainerType>(alias.type.base);
    ASSERT_TRUE(std::holds_alternative<Array>(container));
    const auto& array = std::get<Array>(container);
    EXPECT_THAT(array.size, Eq(32));
    ASSERT_TRUE(std::holds_alternative<Reference>(array.baseType));
    const auto& ref = std::get<Reference>(array.baseType);
    ASSERT_THAT(ref.nested, SizeIs(Eq(1)));
    EXPECT_THAT(ref.nested[0], Eq("Byte"));
  }

  {
    ASSERT_TRUE(std::holds_alternative<Enum>(declarations[1].value));
    const auto& enum_ = std::get<Enum>(declarations[1].value);

    EXPECT_THAT(enum_.name, Eq("TransactionContainer"));
    ASSERT_THAT(enum_.values, SizeIs(Eq(3)));
    {
      const auto& value = enum_.values.at(0);
      EXPECT_THAT(value.name, Eq("Main"));
      EXPECT_THAT(value.tag.binary, Eq(1));
      EXPECT_THAT(value.tag.text, Eq("main"));
    }
    {
      const auto& value = enum_.values.at(1);
      EXPECT_THAT(value.name, Eq("Alternative"));
      EXPECT_THAT(value.tag.binary, Eq(2));
      EXPECT_THAT(value.tag.text, Eq("alternative"));
    }
    {
      const auto& value = enum_.values.at(2);
      EXPECT_THAT(value.name, Eq("Pool"));
      EXPECT_THAT(value.tag.binary, Eq(3));
      EXPECT_THAT(value.tag.text, Eq("pool"));
    }
  }

  {
    ASSERT_TRUE(std::holds_alternative<Flag>(declarations[2].value));
    const auto& flag = std::get<Flag>(declarations[2].value);

    EXPECT_THAT(flag.name, Eq("BlockFeatures"));
    ASSERT_THAT(flag.values, SizeIs(Eq(3)));
    {
      const auto& value = flag.values.at(0);
      EXPECT_THAT(value.name, Eq("StaticReward"));
      EXPECT_THAT(value.tag.binary, Eq(1));
      EXPECT_THAT(value.tag.text, Eq("static_reward"));
    }
    {
      const auto& value = flag.values.at(1);
      EXPECT_THAT(value.name, Eq("ProofOfWork"));
      EXPECT_THAT(value.tag.binary, Eq(2));
      EXPECT_THAT(value.tag.text, Eq("proof_of_work"));
    }
    {
      const auto& value = flag.values.at(2);
      EXPECT_THAT(value.name, Eq("BlockReward"));
      EXPECT_THAT(value.tag.binary, Eq(3));
      EXPECT_THAT(value.tag.text, Eq("block_reward"));
    }
  }

  {
    ASSERT_TRUE(std::holds_alternative<Package>(declarations[3].value));
    const auto& package = std::get<Package>(declarations[3].value);

    EXPECT_THAT(package.name, Eq("Block"));
    ASSERT_TRUE(package.inheritance.has_value());
    ASSERT_THAT(package.inheritance->nested, SizeIs(Eq(1)));
    EXPECT_THAT(package.inheritance->nested[0], Eq("BlockHeader"));

    ASSERT_THAT(package.fields, SizeIs(Eq(2)));
    {
      const auto& field = package.fields.at(0);
      EXPECT_THAT(field.name, Eq("Transactions"));
      EXPECT_THAT(field.tag.binary, Eq(1));
      EXPECT_THAT(field.tag.text, Eq("transactions"));
    }
    {
      const auto& field = package.fields.at(1);
      EXPECT_THAT(field.name, Eq("CumulativeFees"));
      EXPECT_THAT(field.tag.binary, Eq(2));
      EXPECT_THAT(field.tag.text, Eq("cumulative_fees"));
    }
  }

  {
    ASSERT_TRUE(std::holds_alternative<Typedef>(declarations[4].value));
    const auto& typedef_ = std::get<Typedef>(declarations[4].value);

    EXPECT_THAT(typedef_.name, Eq("Hash"));
    EXPECT_FALSE(typedef_.type.isOptional);
    ASSERT_TRUE(std::holds_alternative<ContainerType>(typedef_.type.base));
    const auto& container = std::get<ContainerType>(typedef_.type.base);
    ASSERT_TRUE(std::holds_alternative<Array>(container));
    const auto& array = std::get<Array>(container);
    EXPECT_THAT(array.size, Eq(32));
    ASSERT_TRUE(std::holds_alternative<Reference>(array.baseType));
    const auto& ref = std::get<Reference>(array.baseType);
    ASSERT_THAT(ref.nested, SizeIs(Eq(1)));
    EXPECT_THAT(ref.nested[0], Eq("Byte"));
  }

  {
    ASSERT_TRUE(std::holds_alternative<Variant>(declarations[5].value));
    const auto& variant = std::get<Variant>(declarations[5].value);

    EXPECT_THAT(variant.name, Eq("Search"));

    ASSERT_THAT(variant.fields, SizeIs(Eq(2)));
    {
      const auto& field = variant.fields.at(0);
      EXPECT_THAT(field.name, Eq("Hash"));
      EXPECT_THAT(field.tag.binary, Eq(1));
      EXPECT_THAT(field.tag.text, Eq("hash"));
    }
    {
      const auto& field = variant.fields.at(1);
      EXPECT_THAT(field.name, Eq("Height"));
      EXPECT_THAT(field.tag.binary, Eq(2));
      EXPECT_THAT(field.tag.text, Eq("height"));
    }
  }
}

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Idl/Parser/Variant.hpp>

#define XI_TEST_SUITE Xi_Idl_Parser_Variant

class XI_TEST_SUITE : public ::testing::Test {
 public:
  Xi::Idl::Parser::Variant variant;
  std::shared_ptr<Xi::Idl::Parser::VariantAutomaton> automaton;

  XI_TEST_SUITE() : variant{}, automaton{nullptr} {
    /* */
  }

  void process(const std::string& content) {
    variant = Xi::Idl::Parser::Variant{/* */};
    automaton = Xi::Idl::Parser::makeVariantAutomaton(variant);
    auto ec = automaton->process(content);
    ASSERT_THAT(ec, Xi::Testing::IsSuccess());
  }
};

TEST_F(XI_TEST_SUITE, WellFormed) {
  using namespace Xi::Idl::Parser;
  using namespace ::testing;

  process(R"__(
variant Search {
  Hash    (1;hash) : #Crpto.Hash;
  Height  (2;height) : uint64;
}
)__");

  EXPECT_THAT(variant.name, Eq("Search"));

  ASSERT_THAT(variant.fields, SizeIs(Eq(2)));
  {
    const auto& field = variant.fields.at(0);
    EXPECT_THAT(field.name, Eq("Hash"));
    EXPECT_THAT(field.tag.text, Eq("hash"));
    EXPECT_THAT(field.tag.binary, Eq(1));
  }
  {
    const auto& field = variant.fields.at(1);
    EXPECT_THAT(field.name, Eq("Height"));
    EXPECT_THAT(field.tag.text, Eq("height"));
    EXPECT_THAT(field.tag.binary, Eq(2));
  }
}

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

#include "Xi/Log/Wrapper.hpp"

namespace Xi {
namespace Log {

class ConsoleLogger final : public Wrapper {
 public:
  ~ConsoleLogger() = default;

 private:
  using Wrapper::Wrapper;
  friend class ConsoleLoggerBuilder;
};

class ConsoleLoggerBuilder final {
 public:
  ConsoleLoggerBuilder() = default;
  XI_DELETE_COPY(ConsoleLoggerBuilder);
  XI_DELETE_MOVE(ConsoleLoggerBuilder);
  ~ConsoleLoggerBuilder() = default;

  ConsoleLoggerBuilder& withColoring();
  ConsoleLoggerBuilder& withoutColoring();
  ConsoleLoggerBuilder& withErrorStream();
  ConsoleLoggerBuilder& withStandardStream();

  SharedILogger build();

 private:
  bool m_colored = false;
  bool m_errorStream = false;
};

}  // namespace Log
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Extern/Push.hh>
#include <fmt/format.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>
#include <Xi/String/String.hpp>

#include "Xi/Log/ILogger.hpp"
#include "Xi/Log/Category.hpp"
#include "Xi/Log/Context.hpp"

namespace Xi {
namespace Log {

class Handle final {
 private:
  void print(const Level level, std::string_view message);

 public:
  Handle(WeakCategory category, WeakILogger logger);
  XI_DEFAULT_COPY(Handle);
  ~Handle() = default;

  bool isFiltered(const Level level) const;

  void log(const Level level, std::string_view message);
  void log(const Level level, std::string_view message, fmt::format_args args);

  template <Level _LevelV, typename... _ArgsT>
  void log(std::string_view message, _ArgsT&&... args) {
    if constexpr (sizeof...(_ArgsT) == 0) {
      log(_LevelV, message);
    } else {
      this->log(_LevelV, message, fmt::make_format_args(std::forward<_ArgsT>(args)...));
    }
  }

#define XI_LOG_FORWARD(LEVEL, NAME)                                             \
  template <typename... _ArgsT>                                                 \
  void NAME(std::string_view message, _ArgsT&&... args) {                       \
    this->log<Level::LEVEL, _ArgsT...>(message, std::forward<_ArgsT>(args)...); \
  }

  XI_LOG_FORWARD(Fatal, fatal)
  XI_LOG_FORWARD(Error, error)
  XI_LOG_FORWARD(Warn, warn)
  XI_LOG_FORWARD(Info, info)
  XI_LOG_FORWARD(Debug, debug)
  XI_LOG_FORWARD(Trace, trace)

 private:
  WeakCategory m_category;
  WeakILogger m_logger;
};

}  // namespace Log
}  // namespace Xi

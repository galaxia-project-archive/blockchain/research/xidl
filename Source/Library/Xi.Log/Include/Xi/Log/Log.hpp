/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#if !defined(XI_LOG_LOG_HPP)
#define XI_LOG_LOG_HPP 1

#include "Xi/Log/Category.hpp"
#include "Xi/Log/Level.hpp"
#include "Xi/Log/Context.hpp"
#include "Xi/Log/Registry.hpp"
#include "Xi/Log/Handle.hpp"

#define XI_LOGGER(CATEGORY) static ::Xi::Log::Handle LogHandle = ::Xi::Log::Registry::get(CATEGORY);

#define XI_LOG(LEVEL, ...) LogHandle.log<::Xi::Log::Level::LEVEL>(__VA_ARGS__);
#define XI_LOG_IF(LEVEL, COND, ...) \
  if (COND) {                       \
    XI_LOG(LEVEL, __VA_ARGS__)      \
  }
#define XI_LOG_IF_NOT(LEVEL, COND, ...) \
  if (!(COND)) {                        \
    XI_LOG(LEVEL, __VA_ARGS__)          \
  }

#define XI_TRACE(...) XI_LOG(Trace, __VA_ARGS__)
#define XI_TRACE_IF(COND, ...) XI_LOG_IF(Trace, COND, __VA_ARGS__)
#define XI_TRACE_IF_NOT(COND, ...) XI_LOG_IF_NOT(Trace, COND, __VA_ARGS__)

#define XI_DEBUG(...) XI_LOG(Debug, __VA_ARGS__)
#define XI_DEBUG_IF(COND, ...) XI_LOG_IF(Debug, COND, __VA_ARGS__)
#define XI_DEBUG_IF_NOT(COND, ...) XI_LOG_IF_NOT(Debug, COND, __VA_ARGS__)

#define XI_INFO(...) XI_LOG(Info, __VA_ARGS__)
#define XI_INFO_IF(COND, ...) XI_LOG_IF(Info, COND, __VA_ARGS__)
#define XI_INFO_IF_NOT(COND, ...) XI_LOG_IF_NOT(Info, COND, __VA_ARGS__)

#define XI_WARN(...) XI_LOG(Warn, __VA_ARGS__)
#define XI_WARN_IF(COND, ...) XI_LOG_IF(Warn, COND, __VA_ARGS__)
#define XI_WARN_IF_NOT(COND, ...) XI_LOG_IF_NOT(Warn, COND, __VA_ARGS__)

#define XI_ERROR(...) XI_LOG(Error, __VA_ARGS__)
#define XI_ERROR_IF(COND, ...) XI_LOG_IF(Error, COND, __VA_ARGS__)
#define XI_ERROR_IF_NOT(COND, ...) XI_LOG_IF_NOT(Error, COND, __VA_ARGS__)

#define XI_FATAL(...) XI_LOG(Fatal, __VA_ARGS__)
#define XI_FATAL_IF(COND, ...) XI_LOG_IF(Fatal, COND, __VA_ARGS__)
#define XI_FATAL_IF_NOT(COND, ...) XI_LOG_IF_NOT(Fatal, COND, __VA_ARGS__)

#endif  // !defined(XI_LOG_LOG_HPP)

#if defined(XI_VERBOSE)
#undef XI_VERBOSE
#endif

#if defined(XI_VERBOSE_IF)
#undef XI_VERBOSE_IF
#endif

#if defined(XI_VERBOSE_IF_NOT)
#undef XI_VERBOSE_IF_NOT
#endif

#if defined(XI_LOG_VERBOSE)

#define XI_VERBOSE(...) XI_DEBUG(__VA_ARGS__)
#define XI_VERBOSE_IF(COND, ...) XI_DEBUG_IF(COND, __VA_ARGS__)
#define XI_VERBOSE_IF_NOT(COND, ...) XI_DEBUG_IF_NOT(COND, __VA_ARGS__)

#undef XI_LOG_VERBOSE
#else

#include <Xi/Global.hh>

#define XI_VERBOSE(...)
#define XI_VERBOSE_IF(...)
#define XI_VERBOSE_IF_NOT(...)

#endif

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Network/Protocol.hpp"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Network {

Result<void> parse(const std::string &str, Protocol &out) {
  XI_ERROR_TRY
  if (str == "http") {
    out = Protocol::Http;
    XI_SUCCEED();
  } else if (str == "https") {
    out = Protocol::Https;
    XI_SUCCEED();
  } else if (str == "xi") {
    out = Protocol::Xi;
    XI_SUCCEED();
  } else if (str == "xis") {
    out = Protocol::Xis;
    XI_SUCCEED();
  } else if (str == "xip") {
    out = Protocol::Xip;
    XI_SUCCEED();
  } else {
    exceptional<NotFoundError>("Unknown network protocol: {}", str);
  }
  XI_ERROR_CATCH
}

std::string stringify(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
      return "http";
    case Protocol::Https:
      return "https";
    case Protocol::Xi:
      return "xi";
    case Protocol::Xis:
      return "xis";
    case Protocol::Xip:
      return "xip";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError, "Unrecognized network protocol");
}

bool isHttpBased(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
    case Protocol::Https:
    case Protocol::Xi:
    case Protocol::Xis:
      return true;

    default:
      return false;
  }
}

bool isHttp(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
    case Protocol::Xi:
      return true;

    default:
      return false;
  }
}

bool isHttps(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Https:
    case Protocol::Xis:
      return true;

    default:
      return false;
  }
}

}  // namespace Network
}  // namespace Xi

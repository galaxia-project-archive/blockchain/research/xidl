/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <utility>
#include <memory>
#include <cassert>

#include <Xi/Global.hh>
#include <Xi/Serialization/Object.hpp>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace Rpc {

/*!
 * \brief The ContractPayload class abstracts a payload storage.
 *
 * Typed request/response data is not managed by a stack and the corresponding call must be able to inform the
 * endpoint about data that is expected to be read/written to the client.
 *
 * This class encapsulates an abritrary object (or none at all 'Null') to be serialized. The program must ensure
 * once the request is read the corresponding contract is called with exactly that payload that was created from this
 * contract. The contract then may assume the correct object type and cast the data back into the correct type.
 */
class ContractPayload final {
 private:
  /// Abstraction of an arbritrary object storage.
  struct _StorageConcept {
    virtual ~_StorageConcept() = default;

    /// Returns a mutable pointer to the underlying payload object (if any otherwise nullptr).
    virtual void* data() = 0;
    /// Returns an immutable pointer to the underyling payload object (if any otherwise nullptr).
    virtual const void* data() const = 0;
    virtual Serialization::Object object() = 0;

#if !defined(NDEBUG)
    /// Queries the type identifier of the underlying stored type.
    virtual const std::type_info& type() const = 0;
#endif
  };

  /// Implementation of an object storage concept for a specific type.
  template <typename _DataT>
  struct _Storage final : _StorageConcept {
    using data_type = _DataT;

    data_type dataStorage;

    /// Construct a new object storage container.
    explicit _Storage() : dataStorage{/* */} {
      /* */
    }
    ~_Storage() override = default;

    void* data() override {
      return std::addressof(dataStorage);
    }
    const void* data() const override {
      return std::addressof(dataStorage);
    }
    Serialization::Object object() override {
      return Serialization::makeObject(dataStorage);
    }
#if !defined(NDEBUG)
    virtual const std::type_info& type() const {
      return typeid(data_type);
    }
#endif
  };

 public:
  ContractPayload();
  XI_DELETE_COPY(ContractPayload);
  XI_DEFAULT_MOVE(ContractPayload);
  virtual ~ContractPayload() = default;

  template <typename _ValueT>
  friend ContractPayload makeContractPayload();

 private:
  std::unique_ptr<_StorageConcept> m_storage;

 public:
  Serialization::Object object();

  template <typename _ValueT>
  inline _ValueT* cast() {
    if (this->m_storage == nullptr) {
      return nullptr;
    } else {
#if !defined(NDEBUG)
      assert(typeid(_ValueT) == this->m_storage->type());
#endif
      return static_cast<_ValueT*>(this->m_storage->data());
    }
  }

  template <typename _ValueT>
  inline const _ValueT* cast() const {
    if (this->m_storage == nullptr) {
      return nullptr;
    } else {
#if !defined(NDEBUG)
      assert(typeid(_ValueT) == this->m_storage->type());
#endif
      return static_cast<const _ValueT*>(this->m_storage->data());
    }
  }
};

template <typename _ValueT = void>
ContractPayload makeContractPayload() {
  ContractPayload reval{};
  std::unique_ptr<ContractPayload::_Storage<_ValueT>> storage{new ContractPayload::_Storage<_ValueT>{/* */}};
  reval.m_storage = std::move(storage);
  return reval;
}

template <>
ContractPayload makeContractPayload<void>();

Result<void> serialize(ContractPayload& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace Rpc
}  // namespace Xi

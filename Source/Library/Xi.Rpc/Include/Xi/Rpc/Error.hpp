/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <string>
#include <stdexcept>

#include <Xi/Global.hh>
#include <Xi/Error.hpp>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace Rpc {

class Error {
 public:
  using Code = int;
  using Category = std::string;
  using Message = std::string;

 public:
  explicit Error();
  explicit Error(const Xi::Error& error);

  Code code() const;
  const Category& category() const;
  const Message& message() const;

 private:
  Code m_code;
  Category m_category;
  Message m_message;

 public:
  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(m_code, 0x0001, "code")
  XI_SERIALIZATION_MEMBER(m_message, 0x0002, "message")
  XI_SERIALIZATION_COMPLEX_END
};

}  // namespace Rpc
}  // namespace Xi

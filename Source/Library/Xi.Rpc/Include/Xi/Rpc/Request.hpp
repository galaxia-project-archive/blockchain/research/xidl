/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Rpc/RequestPrefix.hpp"
#include "Xi/Rpc/ContractPayload.hpp"

namespace Xi {
namespace Rpc {

class Request final : public RequestPrefix {
 public:
  static const Serialization::Tag ParameterTag;

 public:
  explicit Request();
  explicit Request(RequestPrefix prefix);
  explicit Request(RequestPrefix prefix, ContractPayload parameter_);
  XI_DELETE_COPY(Request);
  XI_DEFAULT_MOVE(Request);
  ~Request() override = default;

  ContractPayload& parameter();
  const ContractPayload& parameter() const;
  void setParameter(ContractPayload parameter_);

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  ContractPayload m_parameter;
};

}  // namespace Rpc
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serializer.hpp>

#include "Xi/Rpc/Error.hpp"
#include "Xi/Rpc/Version.hpp"
#include "Xi/Rpc/Identifier.hpp"
#include "Xi/Rpc/ResponseData.hpp"
#include "Xi/Rpc/ContractPayload.hpp"

namespace Xi {
namespace Rpc {

class Response {
 public:
  explicit Response();

  Version version() const;
  void setVersion(const Version version_);

  Identifier identifier() const;
  void setIdentifier(const Identifier identifier_);

  const ResponseData& data() const;
  void setData(Error error);
  void setData(ContractPayload object);

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  Version m_version;
  Identifier m_identifier;
  ResponseData m_data;
};

}  // namespace Rpc
}  // namespace Xi

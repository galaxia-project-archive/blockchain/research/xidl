/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <utility>

#include <Xi/ErrorCode.hpp>
#include <Xi/Stream/InputStream.hpp>
#include <Xi/Stream/InMemoryInputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/InputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {

XI_ERROR_CODE_BEGIN(InputSerializer)
XI_ERROR_CODE_VALUE(TrailingBytes, 0x0001)
XI_ERROR_CODE_END(InputSerializer, "Binary::InputSerializerError")

class InputSerializer final : public Serialization::InputSerializer {
 public:
  explicit InputSerializer(Stream::InputStream& stream, Options options = Options{});
  ~InputSerializer() override = default;

  Format format() const override;

  Result<void> readInt8(std::int8_t& value, const Tag& nameTag) override;
  Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) override;
  Result<void> readInt16(std::int16_t& value, const Tag& nameTag) override;
  Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) override;
  Result<void> readInt32(std::int32_t& value, const Tag& nameTag) override;
  Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) override;
  Result<void> readInt64(std::int64_t& value, const Tag& nameTag) override;
  Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) override;

  Result<void> readBoolean(bool& value, const Tag& nameTag) override;

  Result<void> readFloat(float& value, const Tag& nameTag) override;
  Result<void> readDouble(double& value, const Tag& nameTag) override;

  Result<void> readTag(Tag& value, const Tag& nameTag) override;
  Result<void> readFlag(TagVector& value, const Tag& nameTag) override;

  Result<void> readString(std::string& value, const Tag& nameTag) override;
  Result<void> readBinary(ByteVector& out, const Tag& nameTag) override;

  Result<void> readBlob(ByteSpan out, const Tag& nameTag) override;

  Result<void> beginReadComplex(const Tag& nameTag) override;
  Result<void> endReadComplex() override;

  Result<void> beginReadVector(size_t& size, const Tag& nameTag) override;
  Result<void> endReadVector() override;

  Result<void> beginReadArray(size_t size, const Tag& nameTag) override;
  Result<void> endReadArray() override;

  Result<void> checkValue(bool& value, const Tag& nameTag) override;

 private:
  Stream::InputStream& m_stream;
  Options m_options;
};

template <typename _ValueT>
Result<_ValueT> fromBinary(Stream::InputStream& stream, Options options = Options{}) {
  InputSerializer input{stream, options};
  Serializer serializer{input};
  _ValueT reval{};
  XI_ERROR_PROPAGATE_CATCH(serializer(reval, Tag::Null));
  XI_SUCCEED(std::move(reval));
}

template <typename _ValueT>
Result<_ValueT> fromBinary(ConstByteSpan data, Options options = Options{}) {
  Stream::InMemoryInputStream stream{data};
  auto bin = fromBinary<_ValueT>(stream, options);
  XI_ERROR_PROPAGATE(bin)
  auto isEos = stream.isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF_NOT(*isEos, InputSerializerError::TrailingBytes);
  return bin;
}

template <typename _ValueT>
Result<_ValueT> fromBinary(const ByteVector& data, Options options = Options{}) {
  return fromBinary<_ValueT>(asConstByteSpan(data.data(), data.size()), options);
}

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Binary, InputSerializer)

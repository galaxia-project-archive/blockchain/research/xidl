/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <stack>
#include <utility>

#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Stream/ByteVectorOutputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/OutputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {
class OutputSerializer final : public Serialization::OutputSerializer {
 public:
  OutputSerializer(Stream::OutputStream& stream, Options options = Options{});
  ~OutputSerializer() override = default;

  Format format() const override;

  Result<void> writeInt8(std::int8_t value, const Tag& nameTag) override;
  Result<void> writeUInt8(std::uint8_t value, const Tag& nameTag) override;
  Result<void> writeInt16(std::int16_t value, const Tag& nameTag) override;
  Result<void> writeUInt16(std::uint16_t value, const Tag& nameTag) override;
  Result<void> writeInt32(std::int32_t value, const Tag& nameTag) override;
  Result<void> writeUInt32(std::uint32_t value, const Tag& nameTag) override;
  Result<void> writeInt64(std::int64_t value, const Tag& nameTag) override;
  Result<void> writeUInt64(std::uint64_t value, const Tag& nameTag) override;

  Result<void> writeBoolean(bool value, const Tag& nameTag) override;

  Result<void> writeFloat(float value, const Tag& nameTag) override;
  Result<void> writeDouble(double value, const Tag& nameTag) override;

  Result<void> writeTag(const Tag& value, const Tag& nameTag) override;
  Result<void> writeFlag(const TagVector& flag, const Tag& nameTag) override;

  Result<void> writeString(const std::string_view value, const Tag& nameTag) override;
  Result<void> writeBinary(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> writeBlob(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> beginWriteComplex(const Tag& nameTag) override;
  Result<void> endWriteComplex() override;

  Result<void> beginWriteVector(size_t size, const Tag& nameTag) override;
  Result<void> endWriteVector() override;

  Result<void> beginWriteArray(size_t size, const Tag& nameTag) override;
  Result<void> endWriteArray() override;

  Result<void> writeNull(const Tag& nameTag) override;
  Result<void> writeNotNull(const Tag& nameTag) override;

 private:
  Stream::OutputStream& m_stream;
  Options m_options;
};

template <typename _ValueT>
Result<void> toBinary(Stream::OutputStream& stream, const _ValueT& value, Options options = Options{}) {
  OutputSerializer output{stream, options};
  Serializer serializer{output};
  return serializer(const_cast<_ValueT&>(value), Tag::Null);
}

template <typename _ValueT>
Result<ByteVector> toBinary(const _ValueT& value, Options options = Options{}) {
  ByteVector reval{};
  Stream::ByteVectorOutputStream stream{reval};
  XI_ERROR_PROPAGATE_CATCH(toBinary(stream, value, options));
  XI_SUCCEED(std::move(reval));
}

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi

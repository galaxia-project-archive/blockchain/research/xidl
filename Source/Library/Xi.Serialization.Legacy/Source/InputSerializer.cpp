/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Legacy/InputSerializer.hpp"

#include <Serialization/ISerializer.h>

#include "Xi/Serialization/Legacy/LegacyError.hpp"

namespace Xi {
namespace Serialization {
namespace Legacy {

InputSerializer::InputSerializer(CryptoNote::ISerializer &serializer) : m_serializer{serializer} {
  /* */
}

Format InputSerializer::format() const {
  return m_serializer.isHumanReadable() ? Format::HumanReadable : Format::Binary;
}

Result<void> InputSerializer::readInt8(int8_t &, const Tag &) {
  XI_FAIL(LegacyError::Unsupported);
}

Result<void> InputSerializer::readUInt8(uint8_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readInt16(int16_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readUInt16(uint16_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readInt32(int32_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readUInt32(uint32_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readInt64(int64_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readUInt64(uint64_t &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readBoolean(bool &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readFloat(float &, const Tag &) {
  XI_FAIL(LegacyError::Unsupported);
}

Result<void> InputSerializer::readDouble(double &value, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(value, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readString(std::string &string, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer(string, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readBinary(ByteVector &blob, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.binary(blob, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readBlob(ByteSpan out, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.binary(out.data(), out.size(), nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::readTag(Tag &value, const Tag &nameTag) {
  CryptoNote::TypeTag tag = CryptoNote::TypeTag::Null;
  XI_FAIL_IF_NOT(m_serializer.typeTag(tag, nameTag.text()), LegacyError::Internal);
  value = Tag{tag.binary(), tag.text()};
  XI_SUCCEED();
}

Result<void> InputSerializer::readFlag(TagVector &value, const Tag &nameTag) {
  CryptoNote::TypeTagVector tags{};
  XI_FAIL_IF_NOT(m_serializer.flag(tags, nameTag.text()), LegacyError::Internal);
  value.clear();
  value.reserve(tags.size());
  for (const auto &tag : tags) {
    value.emplace_back(tag.binary(), tag.text());
  }
  XI_SUCCEED();
}

Result<void> InputSerializer::beginReadComplex(const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginObject(nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::endReadComplex() {
  XI_FAIL_IF_NOT(m_serializer.endObject(), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::beginReadVector(size_t &size, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginArray(size, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::endReadVector() {
  XI_FAIL_IF_NOT(m_serializer.endArray(), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::beginReadArray(size_t size, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.beginStaticArray(size, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::endReadArray() {
  XI_FAIL_IF_NOT(m_serializer.endArray(), LegacyError::Internal);
  XI_SUCCEED();
}

Result<void> InputSerializer::checkValue(bool &isNull, const Tag &nameTag) {
  XI_FAIL_IF_NOT(m_serializer.maybe(isNull, nameTag.text()), LegacyError::Internal);
  XI_SUCCEED();
}

}  // namespace Legacy
}  // namespace Serialization
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

/*!
 * \section Serialization
 *
 * \attention Binary blobs should be serialized using ISerializer::binary, otherwise thiis
 * serialization method may make an array for a blob in its representation. Ie. for Json [ 122, 10 ].
 */
template <typename _ValueT, size_t _SizeV>
Result<void> serialize(std::array<_ValueT, _SizeV>& value, const Tag& name, Serializer& serializer) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginArray(_SizeV, name));
  for (size_t i = 0; i < _SizeV; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endArray());
  XI_SUCCEED()
}

template <typename _ValueT, size_t _SizeV>
Result<void> serialize(_ValueT value[_SizeV], const Tag& name, Serializer& serializer) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginArray(_SizeV, name));
  for (size_t i = 0; i < _SizeV; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endArray());
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi

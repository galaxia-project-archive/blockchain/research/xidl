/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <cassert>
#include <cinttypes>

#include <Xi/ErrorCode.hpp>

#include "Xi/Serialization/Serializer.hpp"
#include "Xi/Serialization/Tag.hpp"

namespace Xi {
namespace Serialization {

XI_ERROR_CODE_BEGIN(Enum)
XI_ERROR_CODE_VALUE(InvalidTag, 0x0001)
XI_ERROR_CODE_VALUE(UnknownEnum, 0x0002)
XI_ERROR_CODE_VALUE(MissingTag, 0x0003)
XI_ERROR_CODE_END(Enum, "Serialization::EnumError")

namespace Impl {

template <typename _EnumT>
struct EnumRange {
  static inline constexpr uint8_t begin() {
    return 0;
  }
  static inline constexpr uint8_t end() {
    return 0;
  }
};

template <typename _EnumT, uint8_t _IndexV>
Tag getEnumTag();

template <typename _EnumT, uint8_t _IndexV>
inline constexpr bool hasEnumTag() {
  return false;
}

template <typename _EnumT, uint8_t _IndexV>
[[nodiscard]] inline Result<void> serializeIndexedEnumInput(_EnumT& value, const Tag& name, const Tag& tag) {
  using Range = EnumRange<_EnumT>;
  if constexpr (_IndexV > Range::end()) {
    XI_UNUSED(value, name, tag);
    XI_FAIL(EnumError::InvalidTag)
  } else if constexpr (!hasEnumTag<_EnumT, _IndexV>()) {
    return serializeIndexedEnumInput<_EnumT, _IndexV + 1>(value, name, tag);
  } else {
    if (tag == getEnumTag<_EnumT, _IndexV>()) {
      value = static_cast<_EnumT>(_IndexV);
      XI_SUCCEED()
    } else {
      return serializeIndexedEnumInput<_EnumT, _IndexV + 1>(value, name, tag);
    }
  }
}

template <typename _EnumT, uint8_t _IndexV>
[[nodiscard]] inline Result<void> serializeIndexedEnumOutput(_EnumT& value, const Tag& name, Serializer& serializer) {
  using Range = EnumRange<_EnumT>;
  if constexpr (_IndexV > Range::end()) {
    XI_UNUSED(value, name, serializer);
    XI_FAIL(EnumError::UnknownEnum)
  } else {
    if (value == static_cast<_EnumT>(_IndexV)) {
      if constexpr (!hasEnumTag<_EnumT, _IndexV>()) {
        XI_FAIL(EnumError::MissingTag)
      } else {
        Tag tag = getEnumTag<_EnumT, _IndexV>();
        XI_FAIL_IF(tag.isNull(), EnumError::InvalidTag);
        XI_ERROR_PROPAGATE_CATCH(serializer.typeTag(tag, name));
        XI_SUCCEED()
      }
    } else {
      return serializeIndexedEnumOutput<_EnumT, _IndexV + 1>(value, name, serializer);
    }
  }
}

template <typename _EnumT>
[[nodiscard]] inline Result<void> serializeEnum(_EnumT& value, const Tag& name, Serializer& serializer) {
  if (serializer.isInputMode()) {
    Tag tag = Tag::Null;
    XI_ERROR_PROPAGATE_CATCH(serializer.typeTag(tag, name));
    XI_FAIL_IF(tag.isNull(), EnumError::InvalidTag);
    return serializeIndexedEnumInput<_EnumT, EnumRange<_EnumT>::begin()>(value, name, tag);
  } else {
    return serializeIndexedEnumOutput<_EnumT, EnumRange<_EnumT>::begin()>(value, name, serializer);
  }
}

}  // namespace Impl

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Enum)

#define XI_SERIALIZATION_ENUM(ENUM_TYPE)                                                                    \
  [[nodiscard]] inline ::Xi::Result<void> serialize(ENUM_TYPE& value, const ::Xi::Serialization::Tag& name, \
                                                    ::Xi::Serialization::Serializer& serializer) {          \
    return ::Xi::Serialization::Impl::serializeEnum<ENUM_TYPE>(value, name, serializer);                    \
  }

#define XI_SERIALIZATION_ENUM_RANGE(ENUM_TYPE, ENUM_BEGIN, ENUM_END)          \
  namespace Xi {                                                              \
  namespace Serialization {                                                   \
  namespace Impl {                                                            \
  template <>                                                                 \
  struct EnumRange<ENUM_TYPE> {                                               \
    static inline constexpr uint8_t begin() {                                 \
      return static_cast<uint8_t>(ENUM_TYPE::ENUM_BEGIN);                     \
    }                                                                         \
    static inline constexpr uint8_t end() {                                   \
      return static_cast<uint8_t>(ENUM_TYPE::ENUM_END);                       \
    }                                                                         \
    static_assert(0 < static_cast<uint8_t>(ENUM_TYPE::ENUM_BEGIN) &&          \
                      static_cast<uint8_t>(ENUM_TYPE::ENUM_END) < 0b01111111, \
                  "invalid enum range");                                      \
  };                                                                          \
  }                                                                           \
  }                                                                           \
  }

#define XI_SERIALIZATION_ENUM_TAG(ENUM_TYPE, ENUM_VALUE, TEXT)                                 \
  namespace Xi {                                                                               \
  namespace Serialization {                                                                    \
  namespace Impl {                                                                             \
  template <>                                                                                  \
  inline constexpr bool hasEnumTag<ENUM_TYPE, static_cast<uint8_t>(ENUM_TYPE::ENUM_VALUE)>() { \
    return true;                                                                               \
  }                                                                                            \
  template <>                                                                                  \
  inline Tag getEnumTag<ENUM_TYPE, static_cast<uint8_t>(ENUM_TYPE::ENUM_VALUE)>() {            \
    return Tag{static_cast<uint8_t>(ENUM_TYPE::ENUM_VALUE), TEXT};                             \
  }                                                                                            \
  }                                                                                            \
  }                                                                                            \
  }

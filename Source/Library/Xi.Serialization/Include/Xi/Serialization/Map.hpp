/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <unordered_map>

#include <Xi/Global.hh>
#include <Xi/ErrorCode.hpp>
#include <Xi/Result.hpp>
#include <Xi/Exceptions.hpp>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

namespace Impl {
extern const Tag __MapKeyTag;
extern const Tag __MapValueTag;
}  // namespace Impl

XI_ERROR_CODE_BEGIN(Map)
XI_ERROR_CODE_VALUE(DuplicateEntry, 0x0001)
XI_ERROR_CODE_END(Map, "Serialization::MapError")

template <typename _KeyT, typename _ValueT>
Result<void> serialize(std::map<_KeyT, _ValueT>& value, const Tag& name, Serializer& serializer) {
  if (serializer.isInputMode()) {
    value.clear();
    size_t size = 0;
    XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
    for (size_t i = 0; i < size; ++i) {
      _KeyT ikey{};
      _ValueT ivalue{};

      XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(Tag::Null));
      XI_ERROR_PROPAGATE_CATCH(serializer(ikey, Impl::__MapKeyTag));
      XI_ERROR_PROPAGATE_CATCH(serializer(ivalue, Impl::__MapValueTag));
      XI_ERROR_PROPAGATE_CATCH(serializer.endComplex());

      XI_FAIL_IF_NOT(value
                         .emplace(std::piecewise_construct, std::forward_as_tuple(std::move(ikey)),
                                  std::forward_as_tuple(std::move(ivalue)))
                         .second,
                     MapError::DuplicateEntry);
    }
    XI_ERROR_PROPAGATE_CATCH(serializer.endVector())
    XI_SUCCEED()
  } else if (serializer.isOutputMode()) {
    size_t size = value.size();
    XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
    for (const auto& entry : value) {
      XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(Tag::Null));
      XI_ERROR_PROPAGATE_CATCH(serializer(const_cast<_KeyT&>(entry.first), Impl::__MapKeyTag));
      XI_ERROR_PROPAGATE_CATCH(serializer(const_cast<_ValueT&>(entry.second), Impl::__MapValueTag));
      XI_ERROR_PROPAGATE_CATCH(serializer.endComplex());
    }
    XI_ERROR_PROPAGATE_CATCH(serializer.endVector())
    XI_SUCCEED()
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Map)

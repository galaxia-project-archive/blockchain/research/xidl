/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

class Object {
 private:
  struct _Concept {
    virtual ~_Concept() = default;
    virtual Result<void> serializeObject(const Tag& name, Serializer& serializer) = 0;
  };
  template <typename _ValueT>
  struct _Impl final : _Concept {
    _ValueT& ref;

    _Impl(_ValueT& ref_) : ref{ref_} {
      /* */
    }
    ~_Impl() override = default;

    Result<void> serializeObject(const Tag& name, Serializer& serializer) override {
      return serializer(ref, name);
    }
  };

 public:
  Object();
  XI_DELETE_COPY(Object);
  XI_DEFAULT_MOVE(Object);
  ~Object() = default;

  Result<void> serialize(const Tag& name, Serializer& serializer);

 private:
  std::unique_ptr<_Concept> m_concept;

  template <typename _ValueT>
  explicit Object(_ValueT& value) : m_concept{new _Impl<_ValueT>(value)} {
    /* */
  }

  template <typename _ValueT>
  friend Object makeObject(_ValueT&);
};

template <typename _ValueT>
Object makeObject(_ValueT& value) {
  return Object{value};
}

Result<void> serialize(Object& value, const Tag& name, Serializer& serializer);

}  // namespace Serialization
}  // namespace Xi

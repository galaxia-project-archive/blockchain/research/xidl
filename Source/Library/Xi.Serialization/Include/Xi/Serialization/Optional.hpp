/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>
#include <type_traits>
#include <cassert>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _ValueT>
Result<void> serialize(std::optional<_ValueT> &value, const Tag &name, Serializer &serializer) {
  using native_t = typename std::remove_cv_t<_ValueT>;
  static_assert(std::is_default_constructible_v<native_t>,
                "optional serialization expects default constructible types");

  bool hasValue = value.has_value();
  XI_ERROR_PROPAGATE_CATCH(serializer.maybe(hasValue, name));
  if (serializer.isInputMode()) {
    if (hasValue) {
      value.emplace();
      return serializer(*value, name);
    } else {
      value = std::nullopt;
      XI_SUCCEED()
    }
  } else {
    assert(serializer.isOutputMode());
    if (hasValue) {
      return serializer(*value, name);
    } else {
      XI_SUCCEED()
    }
  }
}

}  // namespace Serialization
}  // namespace Xi

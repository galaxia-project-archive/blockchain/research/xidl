/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Serialization/InputSerializer.hpp"
#include "Xi/Serialization/OutputSerializer.hpp"
#include "Xi/Serialization/Serializer.hpp"
#include "Xi/Serialization/Object.hpp"
#include "Xi/Serialization/Array.hpp"
#include "Xi/Serialization/Enum.hpp"
#include "Xi/Serialization/Flag.hpp"
#include "Xi/Serialization/Vector.hpp"
#include "Xi/Serialization/Variant.hpp"
#include "Xi/Serialization/Optional.hpp"
#include "Xi/Serialization/Blob.hpp"
#include "Xi/Serialization/Null.hpp"
#include "Xi/Serialization/Map.hpp"

#define XI_SERIALIZATION_COMPLEX_BEGIN(...) \
  ::Xi::Result<void> serialize(::Xi::Serialization::Serializer& serializer, __VA_ARGS__) {
#define XI_SERIALIZATION_BASE(CLASS) XI_ERROR_PROPAGATE_CATCH(this->CLASS::serialize(serializer))
#define XI_SERIALIZATION_MEMBER(MEMBER, BINARY, TEXT)                  \
  static const ::Xi::Serialization::Tag __##MEMBER##Tag{BINARY, TEXT}; \
  XI_ERROR_PROPAGATE_CATCH(serializer(MEMBER, __##MEMBER##Tag))
#define XI_SERIALIZATION_COMPLEX_END \
  return ::Xi::success();            \
  }

#define XI_SERIALIZATION_COMPLEX_EXTERN(...) \
  ::Xi::Result<void> serialize(::Xi::Serialization::Serializer& serializer, __VA_ARGS__);
#define XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(CLASS, ...) \
  ::Xi::Result<void> CLASS::serialize(::Xi::Serialization::Serializer& serializer, __VA_ARGS__) {
#define XI_SERIALIZATION_COMPLEX_EXTERN_END \
  return ::Xi::success();                   \
  }

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */
#include "Xi/Serialization/Object.hpp"

#include "Xi/Serialization/Null.hpp"

namespace Xi {
namespace Serialization {

namespace {
static Null placeholder;
}

Object::Object() : m_concept{new _Impl<Null>(placeholder)} {
  /* */
}

Result<void> Object::serialize(const Tag &name, Serializer &serializer) {
  return m_concept->serializeObject(name, serializer);
}

Result<void> serialize(Object &value, const Tag &name, Serializer &serializer) {
  return value.serialize(name, serializer);
}

}  // namespace Serialization
}  // namespace Xi

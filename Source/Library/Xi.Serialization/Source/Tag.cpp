/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Tag.hpp"

#include <utility>
#include <limits>
#include <cinttypes>

const Xi::Serialization::Tag Xi::Serialization::Tag::Null{NoBinaryTag, NoTextTag};

const Xi::Serialization::Tag::binary_type Xi::Serialization::Tag::NoBinaryTag{
    std::numeric_limits<Xi::Serialization::Tag::binary_type>::min()};

const Xi::Serialization::Tag::text_type Xi::Serialization::Tag::NoTextTag{""};

Xi::Serialization::Tag::Tag(binary_type binary, text_type text_) : m_binary{binary}, m_text{std::move(text_)} {
  /* */
}

Xi::Serialization::Tag::binary_type Xi::Serialization::Tag::binary() const {
  return m_binary;
}

const Xi::Serialization::Tag::text_type &Xi::Serialization::Tag::text() const {
  return m_text;
}

bool Xi::Serialization::Tag::isNull() const {
  return binary() == NoBinaryTag && text() == NoTextTag;
}

bool Xi::Serialization::Tag::operator==(const Xi::Serialization::Tag &rhs) const {
  if (binary() != NoBinaryTag && binary() == rhs.binary()) {
    return true;
  } else if (text() != NoTextTag && text() == rhs.text()) {
    return true;
  } else {
    return false;
  }
}

bool Xi::Serialization::Tag::operator!=(const Xi::Serialization::Tag &rhs) const {
  return !(*this == rhs);
}

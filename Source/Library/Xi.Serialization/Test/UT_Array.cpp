/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Array.hpp>

#include "GenericSerializerTest.hpp"

XI_GENERIC_SERIALIZER_TEST(Array) {
  using namespace Xi::Serialization;
  std::array<std::uint64_t, 3> values{{0, 42, std::numeric_limits<std::uint64_t>::max()}};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}

XI_GENERIC_SERIALIZER_TEST(EmptyArray) {
  using namespace Xi::Serialization;
  std::array<std::string, 0> values{};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}

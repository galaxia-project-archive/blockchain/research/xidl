/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Enum.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
enum struct AnyEnum {
  First = 1,
  Second = 2,
};
XI_SERIALIZATION_ENUM(AnyEnum)
struct EnumStorage {
  AnyEnum enum_;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(enum_, 0x0001, "enum")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer
XI_SERIALIZATION_ENUM_RANGE(Xi_Serialization_Serializer::AnyEnum, First, Second)
XI_SERIALIZATION_ENUM_TAG(Xi_Serialization_Serializer::AnyEnum, First, "first")
XI_SERIALIZATION_ENUM_TAG(Xi_Serialization_Serializer::AnyEnum, Second, "second")

XI_GENERIC_SERIALIZER_TEST(Enum) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  EnumStorage storage{AnyEnum::Second};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->enum_, storage.enum_);
}

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Flag.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
enum struct AnyFlag {
  None = 0,
  First = 1 << 0,
  Second = 1 << 1,
  Both = First | Second,
};
XI_SERIALIZATION_FLAG(AnyFlag)
XI_TYPESAFE_FLAG_MAKE_OPERATIONS(AnyFlag)

struct FlagStorage {
  AnyFlag flag;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(flag, 0x0001, "flag")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer
XI_SERIALIZATION_FLAG_RANGE(Xi_Serialization_Serializer::AnyFlag, First, Second)
XI_SERIALIZATION_FLAG_TAG(Xi_Serialization_Serializer::AnyFlag, First, "first")
XI_SERIALIZATION_FLAG_TAG(Xi_Serialization_Serializer::AnyFlag, Second, "second")

XI_GENERIC_SERIALIZER_TEST(FlagSingle) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::Second};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}

XI_GENERIC_SERIALIZER_TEST(FlagCombination) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::Both};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}

XI_GENERIC_SERIALIZER_TEST(FlagNone) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::None};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}

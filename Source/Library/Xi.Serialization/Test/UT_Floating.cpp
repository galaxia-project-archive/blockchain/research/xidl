/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <limits>

#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
struct FloatingStorage {
  float float_;
  double double_;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(float_, 0x0001, "float")
  XI_SERIALIZATION_MEMBER(double_, 0x0002, "double")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer

XI_GENERIC_SERIALIZER_TEST(Floating) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FloatingStorage storage{/* */};
  storage.float_ = -12e10f;
  storage.double_ = 1e128;
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->float_, storage.float_);
  EXPECT_EQ(_storage->double_, storage.double_);
}

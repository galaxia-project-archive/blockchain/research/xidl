/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <limits>

#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
template <typename _IntegerT>
struct IntegerStorage {
  _IntegerT min;
  _IntegerT max;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(min, 0x0001, "min")
  XI_SERIALIZATION_MEMBER(max, 0x0002, "max")
  XI_SERIALIZATION_COMPLEX_END
};

#define XI_GENERIC_SERIALIZER_INTEGER_TEST(TYPE)            \
  XI_GENERIC_SERIALIZER_TEST(TYPE) {                        \
    using namespace Xi_Serialization_Serializer;            \
    using namespace Xi::Serialization;                      \
    IntegerStorage<TYPE> storage{/* */};                    \
    storage.min = std::numeric_limits<TYPE>::min();         \
    storage.max = std::numeric_limits<TYPE>::max();         \
    auto _storage = _this.serializeAndDeserialize(storage); \
    ASSERT_TRUE(isSuccess(_storage));                       \
    EXPECT_EQ(_storage->min, storage.min);                  \
    EXPECT_EQ(_storage->max, storage.max);                  \
  }
}  // namespace Xi_Serialization_Serializer

XI_GENERIC_SERIALIZER_INTEGER_TEST(uint8_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(int8_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(uint16_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(int16_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(uint32_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(int32_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(uint64_t)
XI_GENERIC_SERIALIZER_INTEGER_TEST(int64_t)

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <cinttypes>
#include <optional>

#include <Xi/Serialization/Map.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

XI_GENERIC_SERIALIZER_TEST(EmptyMap) {
  using namespace Xi::Serialization;
  using namespace ::testing;
  std::map<std::string, std::string> map{};
  auto smap = _this.serializeAndDeserialize(map);
  ASSERT_TRUE(isSuccess(smap));
  EXPECT_THAT(*smap, SizeIs(0));
}

XI_GENERIC_SERIALIZER_TEST(FilledMap) {
  using namespace Xi::Serialization;
  using namespace ::testing;
  std::map<std::string, std::string> map{};
  map[""] = "EMPTY";
  map["Some cats"] = "may meow!";
  auto smap = _this.serializeAndDeserialize(map);
  ASSERT_TRUE(isSuccess(smap));
  EXPECT_THAT(*smap, SizeIs(2));
  EXPECT_THAT((*smap)[""], Eq("EMPTY"));
  EXPECT_THAT((*smap)["Some cats"], Eq("may meow!"));
}

/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <cinttypes>
#include <optional>

#include <Xi/Serialization/Optional.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace {
struct MaybeStorage {
  std::optional<int64_t> value;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(value, 0x0001, "value")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace

XI_GENERIC_SERIALIZER_TEST(OptionalNoValue) {
  using namespace Xi::Serialization;
  MaybeStorage maybe{};
  maybe.value = std::nullopt;
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}

XI_GENERIC_SERIALIZER_TEST(OptionalValue) {
  using namespace Xi::Serialization;
  MaybeStorage maybe{};
  maybe.value.emplace(-120);
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}

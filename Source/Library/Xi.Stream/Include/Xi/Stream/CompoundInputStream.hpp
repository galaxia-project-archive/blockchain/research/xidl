/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <initializer_list>
#include <deque>

#include <Xi/Global.hh>

#include "Xi/Stream/InputStream.hpp"

namespace Xi {
namespace Stream {

class CompoundInputStream final : public InputStream {
 public:
  explicit CompoundInputStream(std::vector<UniqueInputStream> sources);
  XI_DELETE_COPY(CompoundInputStream);
  XI_DEFAULT_MOVE(CompoundInputStream);
  ~CompoundInputStream() override = default;

  [[nodiscard]] Result<size_t> read(ByteSpan buffer) override;
  [[nodiscard]] Result<bool> isEndOfStream() const override;
  [[nodiscard]] Result<size_t> tell() const override;
  [[nodiscard]] Result<Byte> peek() const override;
  [[nodiscard]] Result<Byte> take() override;

 private:
  Result<void> popEndOfStreamSources();

 private:
  std::deque<UniqueInputStream> m_sources;
  size_t m_pos;
};

XI_DECLARE_SMART_POINTER(CompoundInputStream)

}  // namespace Stream
}  // namespace Xi

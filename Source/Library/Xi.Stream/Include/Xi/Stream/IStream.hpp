/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Ownership.hpp>

#include <istream>

#include "Xi/Stream/InputStream.hpp"

namespace Xi {
namespace Stream {
class IStream final : public InputStream {
 public:
  explicit IStream(Ownership<std::istream> source);
  XI_DELETE_COPY(IStream);
  IStream(IStream&&) = default;
  IStream& operator=(IStream&&) = delete;
  ~IStream() override = default;

  [[nodiscard]] Result<size_t> read(ByteSpan buffer) override;
  [[nodiscard]] Result<bool> isEndOfStream() const override;
  [[nodiscard]] Result<size_t> tell() const override;
  [[nodiscard]] Result<Byte> peek() const override;
  [[nodiscard]] Result<Byte> take() override;

 private:
  mutable Ownership<std::istream> m_source;
};

XI_DECLARE_SMART_POINTER(IStream)
}  // namespace Stream
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/InputStream.hpp"

#include "Xi/Stream/StreamError.hpp"
#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {

Result<void> InputStream::readStrict(ByteSpan buffer) {
  while (!buffer.empty()) {
    const auto nRead = read(buffer);
    XI_ERROR_PROPAGATE(nRead)
    XI_FAIL_IF(*nRead == 0ULL, StreamError::EndOfStream);
    buffer = buffer.slice(*nRead);
  }
  XI_FAIL_IF_NOT(buffer.empty(), StreamError::EndOfStream);
  XI_SUCCEED();
}

Result<void> InputStream::readAll(OutputStream &stream) {
  auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  while (!*isEos) {
    ByteArray<1024> buffer{};
    auto size = read(buffer);
    XI_ERROR_PROPAGATE(size)
    XI_ERROR_PROPAGATE_CATCH(stream.writeStrict(asConstByteSpan(buffer.data(), *size)));
    isEos = isEndOfStream();
    XI_ERROR_PROPAGATE(isEos)
  }
  XI_SUCCEED()
}

}  // namespace Stream
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace VersionInfo {

class Git {
 public:
  static const std::optional<Git>& project();

 public:
  Git() = default;
  XI_DEFAULT_COPY(Git);
  XI_DEFAULT_MOVE(Git);
  ~Git() = default;

  const std::string& commit() const;
  const std::string& author() const;
  const std::string& branch() const;
  const std::string& date() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class GitBuilder;

 private:
  std::string m_commit;
  std::string m_author;
  std::string m_branch;
  std::string m_date;
};

class GitBuilder {
 public:
  GitBuilder() = default;
  XI_DELETE_COPY(GitBuilder);
  XI_DELETE_MOVE(GitBuilder);
  ~GitBuilder() = default;

  GitBuilder& withCommit(const std::string& commit);
  GitBuilder& withAuthor(const std::string& author);
  GitBuilder& withBranch(const std::string& branch);
  GitBuilder& withDate(const std::string& date);

  Git build() const;

 private:
  Git m_info;
};

}  // namespace VersionInfo
}  // namespace Xi

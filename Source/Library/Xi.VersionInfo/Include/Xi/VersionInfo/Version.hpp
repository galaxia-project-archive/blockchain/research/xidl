/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <optional>
#include <string>

#if defined(major)
#undef major
#endif

#if defined(minor)
#undef minor
#endif

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/VersionInfo/Channel.hpp"

namespace Xi {
namespace VersionInfo {

// TODO: Semantic Versionsing
// https://semver.org/
class Version {
 public:
  using NumericIdentifier = std::uint64_t;

 public:
  static const Version& project();

 public:
  Version() = default;
  XI_DEFAULT_COPY(Version);
  XI_DEFAULT_MOVE(Version);
  ~Version() = default;

  NumericIdentifier major() const;
  NumericIdentifier minor() const;
  NumericIdentifier patch() const;
  std::optional<NumericIdentifier> build() const;
  std::optional<Channel> channel() const;

  std::string stringify() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class VersionBuilder;

 private:
  NumericIdentifier m_major;
  NumericIdentifier m_minor;
  NumericIdentifier m_patch;
  std::optional<NumericIdentifier> m_build;
  std::optional<Channel> m_channel;
};

class VersionBuilder {
 public:
  VersionBuilder() = default;
  XI_DELETE_COPY(VersionBuilder);
  XI_DELETE_MOVE(VersionBuilder);
  ~VersionBuilder() = default;

  VersionBuilder& withMajor(Version::NumericIdentifier major);
  VersionBuilder& withMinor(Version::NumericIdentifier minor);
  VersionBuilder& withPatch(Version::NumericIdentifier patch);
  VersionBuilder& withBuild(std::optional<Version::NumericIdentifier> build);
  VersionBuilder& withChannel(std::optional<Channel> channel);

  Version build() const;

 private:
  Version m_version;
};

}  // namespace VersionInfo
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/VersionInfo/Version.hpp"
#include "Xi/VersionInfo/Compiler.hpp"
#include "Xi/VersionInfo/Git.hpp"

namespace Xi {
namespace VersionInfo {

class VersionInfo {
 public:
  static const VersionInfo& project();

 public:
  VersionInfo() = default;
  XI_DEFAULT_COPY(VersionInfo);
  XI_DEFAULT_MOVE(VersionInfo);
  ~VersionInfo() = default;

  const Version& version() const;

  const Compiler& compiler() const;
  const std::optional<Git>& git() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class VersionInfoBuilder;

 private:
  Version m_version;
  Compiler m_compiler;
  std::optional<Git> m_git;
};

class VersionInfoBuilder {
 public:
  VersionInfoBuilder() = default;
  XI_DELETE_COPY(VersionInfoBuilder);
  XI_DELETE_MOVE(VersionInfoBuilder);
  ~VersionInfoBuilder() = default;

  VersionInfoBuilder& withVersion(const Version& version);
  VersionInfoBuilder& withCompiler(const Compiler& compiler);
  VersionInfoBuilder& withGit(const std::optional<Git> git);

  VersionInfo build() const;

 private:
  VersionInfo m_info;
};

}  // namespace VersionInfo
}  // namespace Xi

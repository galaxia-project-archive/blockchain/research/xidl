/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/VersionInfo/Channel.hpp"

#include <Xi/Exceptions.hpp>
#include <Xi/String/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::VersionInfo, Channel)
XI_ERROR_CODE_DESC(Unknown, "unknown channel name")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace VersionInfo {

std::string stringify(const Channel channel) {
  switch (channel) {
    case Channel::Stable:
      return "stable";
    case Channel::Beta:
      return "beta";
    case Channel::Edge:
      return "edge";
    case Channel::Clutter:
      return "clutter";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<Channel> fromString(const std::string &str) {
  const auto lowerStr = toLower(str);
  if (lowerStr == stringify(Channel::Stable)) {
    XI_SUCCEED(Channel::Stable)
  } else if (lowerStr == stringify(Channel::Beta)) {
    XI_SUCCEED(Channel::Beta)
  } else if (lowerStr == stringify(Channel::Edge)) {
    XI_SUCCEED(Channel::Edge)
  } else if (lowerStr == stringify(Channel::Clutter)) {
    XI_SUCCEED(Channel::Clutter)
  } else {
    XI_FAIL(ChannelError::Unknown)
  }
}

}  // namespace VersionInfo
}  // namespace Xi

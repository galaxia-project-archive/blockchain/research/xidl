/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/VersionInfo/Compiler.hpp"

#include <Version/Project/Version.hh>

namespace Xi {
namespace VersionInfo {

const Compiler &Compiler::project() {
  static const Compiler __singleton = CompilerBuilder{/* */}
                                          .withPlatform(Version::Project::Compiler::Platform)
                                          .withEndianess(Version::Project::Compiler::Endianess)
                                          .withCFlags(Version::Project::Compiler::CFlags)
                                          .withCIdentifier(Version::Project::Compiler::CIdentifier)
                                          .withCVersion(Version::Project::Compiler::CVersion)
                                          .withCxxFlags(Version::Project::Compiler::CXXFlags)
                                          .withCxxIdentifier(Version::Project::Compiler::CXXIdentifier)
                                          .withCxxVersion(Version::Project::Compiler::CXXVersion)
                                          .build();
  return __singleton;
}

const std::string &Compiler::platform() const {
  return m_platform;
}

const std::string &Compiler::endianess() const {
  return m_endianess;
}

const std::string &Compiler::cFlags() const {
  return m_cFlags;
}

const std::string &Compiler::cIdentifier() const {
  return m_cIdentifier;
}

const std::string &Compiler::cVersion() const {
  return m_cVersion;
}

const std::string &Compiler::cxxFlags() const {
  return m_cxxFlags;
}

const std::string &Compiler::cxxIdentifier() const {
  return m_cxxIdentifier;
}

const std::string &Compiler::cxxVersion() const {
  return m_cxxVersion;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Compiler)
XI_SERIALIZATION_MEMBER(m_platform, 0x0001, "platform")
XI_SERIALIZATION_MEMBER(m_endianess, 0x0002, "endianess")
XI_SERIALIZATION_MEMBER(m_cVersion, 0x0003, "c_version")
XI_SERIALIZATION_MEMBER(m_cIdentifier, 0x0004, "c_identifier")
XI_SERIALIZATION_MEMBER(m_cFlags, 0x0005, "c_flags")
XI_SERIALIZATION_MEMBER(m_cxxVersion, 0x0006, "cxx_version")
XI_SERIALIZATION_MEMBER(m_cxxIdentifier, 0x0007, "cxx_identifier")
XI_SERIALIZATION_MEMBER(m_cxxFlags, 0x0008, "cxx_flags")
XI_SERIALIZATION_COMPLEX_EXTERN_END

CompilerBuilder &CompilerBuilder::withPlatform(const std::string &platform) {
  m_info.m_platform = platform;
  return *this;
}

CompilerBuilder &CompilerBuilder::withEndianess(const std::string &endianess) {
  m_info.m_endianess = endianess;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCFlags(const std::string &flags) {
  m_info.m_cFlags = flags;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCIdentifier(const std::string &identifier) {
  m_info.m_cIdentifier = identifier;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCVersion(const std::string &version) {
  m_info.m_cVersion = version;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCxxFlags(const std::string &flags) {
  m_info.m_cxxFlags = flags;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCxxIdentifier(const std::string &identifier) {
  m_info.m_cxxIdentifier = identifier;
  return *this;
}

CompilerBuilder &CompilerBuilder::withCxxVersion(const std::string &version) {
  m_info.m_cxxVersion = version;
  return *this;
}

Compiler CompilerBuilder::build() const {
  return m_info;
}

}  // namespace VersionInfo
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/VersionInfo/Git.hpp"

#include <Version/Project/Version.hh>

namespace Xi {
namespace VersionInfo {

const std::optional<Git> &Git::project() {
  static const std::optional<Git> __singleton =
#if !defined(VERSION_PROJECT_GIT_ENABLED)
      std::nullopt
#else
      GitBuilder{/* */}
          .withBranch(Version::Project::Git::Branch)
          .withCommit(Version::Project::Git::CommitHash)
          .withAuthor(Version::Project::Git::CommiterName)
          .withDate(Version::Project::Git::CommiterDate)
          .build()
#endif
      ;
  return __singleton;
}

const std::string &Git::commit() const {
  return m_commit;
}

const std::string &Git::author() const {
  return m_author;
}

const std::string &Git::branch() const {
  return m_branch;
}

const std::string &Git::date() const {
  return m_date;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Git)
XI_SERIALIZATION_MEMBER(m_commit, 0x00001, "commit")
XI_SERIALIZATION_MEMBER(m_author, 0x00002, "author")
XI_SERIALIZATION_MEMBER(m_branch, 0x00003, "branch")
XI_SERIALIZATION_MEMBER(m_date, 0x00004, "date")
XI_SERIALIZATION_COMPLEX_EXTERN_END

GitBuilder &GitBuilder::withCommit(const std::string &commit) {
  m_info.m_commit = commit;
  return *this;
}

GitBuilder &GitBuilder::withAuthor(const std::string &author) {
  m_info.m_author = author;
  return *this;
}

GitBuilder &GitBuilder::withBranch(const std::string &branch) {
  m_info.m_branch = branch;
  return *this;
}

GitBuilder &GitBuilder::withDate(const std::string &date) {
  m_info.m_date = date;
  return *this;
}

Git GitBuilder::build() const {
  return m_info;
}

}  // namespace VersionInfo
}  // namespace Xi

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/VersionInfo/Version.hpp"

#include <sstream>

#include <Version/Project/Version.hh>
#include <Xi/String/ToString.hpp>

#include "Xi/VersionInfo/Config/Config.hh"

namespace Xi {
namespace VersionInfo {

const Version &Version::project() {
  static const Version __singletonVersion = VersionBuilder{/* */}
                                                .withMajor(
#if defined(VERSION_PROJECT_MAJOR)
                                                    VERSION_PROJECT_MAJOR
#else
                                                    0
#endif
                                                    )
                                                .withMinor(
#if defined(VERSION_PROJECT_MINOR)
                                                    VERSION_PROJECT_MINOR
#else
                                                    0
#endif
                                                    )
                                                .withPatch(
#if defined(VERSION_PROJECT_PATCH)
                                                    VERSION_PROJECT_PATCH
#else
                                                    0
#endif
                                                    )
                                                .withBuild(
#if defined(VERSION_PROJECT_BUILD)
                                                    VERSION_PROJECT_BUILD
#else
                                                    std::nullopt
#endif
                                                    )
                                                .withChannel(
#if defined(XI_RELEASE_CHANNEL_STABLE)
                                                    Channel::Stable
#elif defined(XI_RELEASE_CHANNEL_BETA)
                                                    Channel::Beta
#elif defined(XI_RELEASE_CHANNEL_EDGE)
                                                    Channel::Edge
#elif defined(XI_RELEASE_CHANNEL_CLUTTER)
                                                    Channel::Clutter
#else
                                                    std::nullopt
#endif
                                                    )
                                                .build();
  return __singletonVersion;
}

Version::NumericIdentifier Version::major() const {
  return m_major;
}

Version::NumericIdentifier Version::minor() const {
  return m_minor;
}

Version::NumericIdentifier Version::patch() const {
  return m_patch;
}

std::optional<Version::NumericIdentifier> Version::build() const {
  return m_build;
}

std::optional<Channel> Version::channel() const {
  return m_channel;
}

std::string Version::stringify() const {
  std::ostringstream builder{};
  builder << major() << ".";
  builder << minor() << ".";
  builder << patch();
  if (channel()) {
    builder << "-" << *channel();
  }
  if (build()) {
    builder << "+" << *build();
  }
  return builder.str();
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Version)
XI_SERIALIZATION_MEMBER(m_major, 0x0001, "major")
XI_SERIALIZATION_MEMBER(m_minor, 0x0002, "minor")
XI_SERIALIZATION_MEMBER(m_patch, 0x0003, "patch")
XI_SERIALIZATION_MEMBER(m_build, 0x0004, "build")
XI_SERIALIZATION_MEMBER(m_channel, 0x0005, "channel")
XI_SERIALIZATION_COMPLEX_EXTERN_END

VersionBuilder &VersionBuilder::withMajor(Version::NumericIdentifier major) {
  m_version.m_major = major;
  return *this;
}

VersionBuilder &VersionBuilder::withMinor(Version::NumericIdentifier minor) {
  m_version.m_minor = minor;
  return *this;
}

VersionBuilder &VersionBuilder::withPatch(Version::NumericIdentifier patch) {
  m_version.m_patch = patch;
  return *this;
}

VersionBuilder &VersionBuilder::withBuild(std::optional<Version::NumericIdentifier> build) {
  m_version.m_build = build;
  return *this;
}

VersionBuilder &VersionBuilder::withChannel(std::optional<Channel> channel) {
  m_version.m_channel = channel;
  return *this;
}

Version VersionBuilder::build() const {
  return m_version;
}

}  // namespace VersionInfo
}  // namespace Xi

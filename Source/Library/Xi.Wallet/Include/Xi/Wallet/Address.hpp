/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/Crypto/PublicKey.hpp>
#include <Xi/Config/NetworkType.h>

#include "Xi/Wallet/AddressPrefix.hpp"
#include "Xi/Wallet/AddressExtension.hpp"

namespace Xi {
namespace Wallet {

XI_ERROR_CODE_BEGIN(Address)
XI_ERROR_CODE_VALUE(InvalidPrefix, 0x0001)
XI_ERROR_CODE_VALUE(InvalidPublicKey, 0x0002)
XI_ERROR_CODE_VALUE(InvalidChecksum, 0x0003)
XI_ERROR_CODE_END(Address, "Wallet::AddressError")

class Address final {
 public:
  static Result<Address> parse(const std::string& str);

 public:
  explicit Address() = default;
  XI_DEFAULT_COPY(Address);
  XI_DEFAULT_MOVE(Address);
  ~Address() = default;

  const AddressPrefix& prefix() const;
  void setPrefix(const AddressPrefix& prefix_);

  Config::Network::Type network() const;
  void setNetwork(Config::Network::Type network_);

  const Crypto::PublicKey& publicKey() const;
  void setPublicKey(const Crypto::PublicKey& publicKey_);

  const AddressExtension& extension() const;
  AddressExtension& extension();
  void setExtension(AddressExtension& extension_);

  std::string stringify() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  AddressPrefix m_prefix{/* */};
  Config::Network::Type m_network{Config::Network::Type::LocalTestNet};
  Crypto::PublicKey m_publicKey = Crypto::PublicKey::Null;
  AddressExtension m_extension{/* */};
};

}  // namespace Wallet
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Wallet, Address)

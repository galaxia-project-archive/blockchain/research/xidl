/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <string>

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Result.hpp>
#include <Xi/ErrorCode.hpp>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace Wallet {

XI_ERROR_CODE_BEGIN(AddressPrefix)
XI_ERROR_CODE_VALUE(Empty, 0x0001)
XI_ERROR_CODE_VALUE(TooLong, 0x0002)
XI_ERROR_CODE_VALUE(InvalidTermination, 0x0003)
XI_ERROR_CODE_END(AddressPrefix, "Wallet::AddressPrefixError")

class AddressPrefix {
 public:
  using Binary = ByteVector;
  using Text = std::string;

 public:
  static Result<AddressPrefix> parse(const std::string& str);

 public:
  explicit AddressPrefix();
  explicit AddressPrefix(Binary binary);

  const Binary& binary() const;
  const Text& text() const;

  bool isValid() const;
  std::string stringify() const;

 private:
  Binary m_binary;
  Text m_text;
};

Result<void> serialize(AddressPrefix& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace Wallet
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Wallet, AddressPrefix)

/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Wallet/AddressExtension.hpp"

#include <Xi/Serialization/Map.hpp>

namespace Xi {
namespace Wallet {

AddressExtension::Feature AddressExtension::features() const {
  Feature reval = Feature::None;
  if (m_viewKey) {
    reval |= Feature::ViewKey;
  }
  if (m_paymentId) {
    reval |= Feature::PaymentId;
  }
  if (m_custom.size() > 0) {
    reval |= Feature::Custom;
  }
  return reval;
}

const std::optional<Crypto::PublicKey> &AddressExtension::viewKey() const {
  return m_viewKey;
}

void AddressExtension::setViewKey(const std::optional<Crypto::PublicKey> &viewKey_) {
  m_viewKey = viewKey_;
}

const std::optional<Crypto::PublicKey> &AddressExtension::paymentId() const {
  return m_paymentId;
}

void AddressExtension::setPaymentId(const std::optional<Crypto::PublicKey> &paymentId_) {
  m_paymentId = paymentId_;
}

const std::map<std::string, std::string> &AddressExtension::custom() const {
  return m_custom;
}

std::map<std::string, std::string> &AddressExtension::custom() {
  return m_custom;
}

Result<void> AddressExtension::serialize(Serialization::Serializer &serializer) {
  Feature thisFeatures = features();
  XI_ERROR_PROPAGATE_CATCH(serializer(thisFeatures, Serialization::Tag{0x0001, "features"}));
  if (serializer.isInputMode()) {
    m_custom.clear();
    if (hasFlag(thisFeatures, Feature::ViewKey)) {
      m_viewKey = Crypto::PublicKey::Null;
    } else {
      m_viewKey = std::nullopt;
    }
    if (hasFlag(thisFeatures, Feature::PaymentId)) {
      m_paymentId = Crypto::PublicKey::Null;
    } else {
      m_paymentId = std::nullopt;
    }
  }

  if (hasFlag(thisFeatures, Feature::ViewKey)) {
    XI_ERROR_PROPAGATE_CATCH(serializer(*m_viewKey, Serialization::Tag{0x0002, "view_key"}))
    XI_FAIL_IF_NOT(m_viewKey->isValid(), AddressExtensionError::InvalidViewKey)
  }

  if (hasFlag(thisFeatures, Feature::PaymentId)) {
    XI_ERROR_PROPAGATE_CATCH(serializer(*m_paymentId, Serialization::Tag{0x0002, "payment_id"}))
    XI_FAIL_IF_NOT(m_paymentId->isValid(), AddressExtensionError::InvalidPaymentId)
  }

  if (hasFlag(thisFeatures, Feature::Custom)) {
    XI_ERROR_PROPAGATE_CATCH(serializer(m_custom, Serialization::Tag{0x0005, "custom"}))
    XI_FAIL_IF(m_custom.empty(), AddressExtensionError::EmptyCustom)
  }

  XI_SUCCEED()
}

}  // namespace Wallet
}  // namespace Xi

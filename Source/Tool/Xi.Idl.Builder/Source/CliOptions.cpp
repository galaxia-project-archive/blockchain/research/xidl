/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CliOptions.hpp"

#include <Xi/Extern/Push.hh>
#include <cxxopts.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Idl/Generator/Cpp/Generator.hpp>

#include "CliOptionsError.hpp"

namespace XiIdlBuilder {

bool CliOptions::parse(int &argc, char **&argv) {
  using namespace cxxopts;

  Options options("xi-idl-builder", "");
  // clang-format off
  options.add_options("common")
    ("i,input", "xidl input files or directories", value(inputFiles))
    ("r,recurse", "recurses on input directories", value(recurse)->default_value("false")->implicit_value("true"))
    ("g,generator", "generator to use", value(generator))
    ("o,output", "output directory", value(output))
    ("c,config", "generator specific configuration file", value(configFile))
  ;
  // clang-format on

  [[maybe_unused]] const auto result = options.parse(argc, argv);

  try {
    XI_RETURN_SC(true);
  } catch (...) {
    XI_RETURN_EC(false);
  }
}

Xi::Result<std::unique_ptr<Generator> > CliOptions::makeGenerator() {
  std::unique_ptr<Generator> reval{};
  if (generator == "cpp") {
    auto gen = Cpp::makeGenerator(Cpp::Config{output});
    XI_ERROR_PROPAGATE(gen)
    reval = gen.take();
  } else {
    XI_FAIL(CliOptionsError::UnknownGenerator)
  }
  XI_SUCCEED(std::move(reval))
}

}  // namespace XiIdlBuilder

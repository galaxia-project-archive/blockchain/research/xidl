/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <string>
#include <iostream>
#include <fstream>

#include <Xi/Extern/Push.hh>
#include <cxxopts.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>
#include <Xi/Exceptions.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/String/String.hpp>
#include <Xi/Log/ConsoleLogger.hpp>
#include <Xi/FileSystem/FileSystem.hpp>
#include <Xi/Stream/IStream.hpp>
#include <Xi/Idl/Processor.hpp>
#include <Xi/Idl/Parser/Parser.hpp>
#include <Xi/Idl/Generator/Generator.hpp>
#include <Xi/Idl/Generator/Cpp/Generator.hpp>

#include "CliOptions.hpp"

XI_LOGGER("Idl/Builder")

using namespace XiIdlBuilder;
using namespace Xi;
using namespace Xi::Idl;

namespace {
Result<void> process(CliOptions& cliOptions, const FileSystem::Entry& entry) {
  XI_INFO("Processing '{}'", entry)
  auto entryKind = entry.kind();
  XI_ERROR_PROPAGATE(entryKind)
  if (*entryKind == FileSystem::Entry::Kind::File) {
    auto file = entry.asFile();
    XI_ERROR_PROPAGATE(file)
    auto gen = cliOptions.makeGenerator();
    XI_ERROR_PROPAGATE(gen);
    auto ec = Processor::process(toString(*file), **gen);
    XI_ERROR_PROPAGATE(ec)
  } else if (*entryKind == FileSystem::Entry::Kind::Directory) {
    if (cliOptions.recurse) {
      auto dir = entry.asDirectory();
      XI_ERROR_PROPAGATE(dir)
      auto dirEntries = dir->entries();
      XI_ERROR_PROPAGATE(dirEntries)

      for (const auto dirEntry : *dirEntries) {
        XI_ERROR_PROPAGATE_CATCH(process(cliOptions, dirEntry))
      }
    }
  } else {
    XI_EXCEPTIONAL(InvalidEnumValueError)
  }
  XI_SUCCEED()
}

Result<void> process(CliOptions& cliOptions, const std::string& file) {
  const auto entry = FileSystem::makeEntry(file);
  if (entry.isError()) {
    XI_FATAL("Failed resolving path '{}' with: {}", file, entry.error().message())
    XI_RETURN_EC(entry.error());
  }
  return process(cliOptions, *entry);
}
}  // namespace

int main(int argc, char** argv) {
  Log::Registry::put(Log::ConsoleLoggerBuilder{}.withColoring().build());

  try {
    CliOptions cliOptions{};
    XI_RETURN_EC_IF_NOT(cliOptions.parse(argc, argv), EXIT_FAILURE);

    for (const auto& file : cliOptions.inputFiles) {
      const auto ec = process(cliOptions, file);
      if (ec.isError()) {
        XI_FATAL("Processing failed: {}", ec.error());
        XI_RETURN_EC(EXIT_FAILURE);
      }
    }

    XI_RETURN_SC(EXIT_SUCCESS);
  } catch (std::exception& e) {
    XI_FATAL(e.what());
    XI_RETURN_EC(EXIT_FAILURE);
  }
}
